//
//  FloatingTextField.swift
//  AnimatedTextField
//
//  Created by Alexey Zhulikov on 22.10.2019.
//  Copyright © 2019 Alexey Zhulikov. All rights reserved.
//

import UIKit

private extension TimeInterval {
    static let animation250ms: TimeInterval = 0.25
}

private extension UIColor {
  //static let inactive: UIColor = Constants.Colors.colorlightgray ?? .lightGray
  static let inactiveText: UIColor = UIColor(red: 126.0/255.0, green: 126.0/255.0, blue: 127.0/255.0, alpha: 1.0)
  static let inactiveBorder: UIColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1.0)
  static let active: UIColor = Constants.Colors.colorPrimary ?? .blue
}

private enum Constant {
    static let offset: CGFloat = 8
    static let placeholderSize: CGFloat = 14
}

final class FloatingTextField: UITextField {

    // MARK: - Subviews

    private var border = UIView()
    private var label = UILabel()

    // MARK: - Private Properties

    private var scale: CGFloat {
        Constant.placeholderSize / fontSize
    }

    private var fontSize: CGFloat {
        font?.pointSize ?? 0
    }

    private var labelHeight: CGFloat {
        ceil(font?.withSize(Constant.placeholderSize).lineHeight ?? 0)
    }

    private var textHeight: CGFloat {
        ceil(font?.lineHeight ?? 0)
    }

    private var isEmpty: Bool {
        text?.isEmpty ?? true
    }

    private var textInsets: UIEdgeInsets {
        UIEdgeInsets(top: Constant.offset + labelHeight, left: 0, bottom: Constant.offset, right: 0)
    }

    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    // MARK: - UITextField

    override var intrinsicContentSize: CGSize {
        return CGSize(width: bounds.width, height: textInsets.top + textHeight + textInsets.bottom)
    }

    override var placeholder: String? {
        didSet {
            label.text = placeholder
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        border.frame = CGRect(x: 0, y: bounds.height - 1, width: bounds.width, height: 1)
        updateLabel(animated: false)
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textInsets)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textInsets)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return .zero
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        guard !isFirstResponder else {
            return
        }

        label.transform = .identity
        label.frame = bounds.inset(by: textInsets)
        self.layoutSubviews()
    }

    // MARK: - Private Methods

    private func setupUI() {
        borderStyle = .none

        border.backgroundColor = .inactiveBorder
        border.isUserInteractionEnabled = false
        addSubview(border)

        label.textColor = .inactiveText
        label.font = font
        label.text = placeholder
        label.isUserInteractionEnabled = false
        addSubview(label)

      addTarget(self, action: #selector(handleEditing), for: .allEditingEvents)
    }

    @objc private func handleEditing() {
      //self.label.textColor = .inactive
        updateLabel()
        updateBorder()
    }

    private func updateBorder() {
      let TextColor = isFirstResponder ? Constants.Colors.colorPrimary : .inactiveText
      let borderColor = isFirstResponder ? Constants.Colors.colorPrimary : .inactiveBorder
      UIView.animate(withDuration: .animation250ms) {
        self.border.backgroundColor = borderColor
        self.label.textColor = TextColor
        
      }
    }

    private func updateLabel(animated: Bool = true) {
        let isActive = isFirstResponder || !isEmpty

        let offsetX = -label.bounds.width * (1 - scale) / 2
        let offsetY = -label.bounds.height * (1 - scale) / 2

        let transform = CGAffineTransform(translationX: offsetX, y: offsetY - labelHeight - Constant.offset)
            .scaledBy(x: scale, y: scale)

        guard animated else {
          self.label.transform = isActive ? transform : .identity
          return
        }

        UIView.animate(withDuration: .animation250ms) {
            self.label.transform = isActive ? transform : .identity
            self.border.backgroundColor = isActive ? .active : .inactiveBorder
        }
      
        
    }
}



