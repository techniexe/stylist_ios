
import Foundation
import UIKit

struct Constants {
  static let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
  static let kMainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
  static let kTabBarStoryBoard = UIStoryboard(name: "TabBar", bundle: Bundle.main)
  static let kHomeStoryBoard = UIStoryboard(name: "HomeVC", bundle: Bundle.main)
  static let kProfileStoryBoard = UIStoryboard(name: "PofileVC", bundle: Bundle.main)
  static let kWishListStoryBoard = UIStoryboard(name: "WishListVC", bundle: Bundle.main)
  static let kCartStoryBoard = UIStoryboard(name: "CartVC", bundle: Bundle.main)
  static let kCustomAlertStoryBoard = UIStoryboard(name: "CustomAlertVC", bundle: Bundle.main)
  static let kSideMenuVC = UIStoryboard(name: "SideMenuVC", bundle: Bundle.main)
  static let kAcceptableCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  static let googleApiKey = "AIzaSyCK9LAEHHl6hfatHhmreDw9oaTAgLstNUk" //"AIzaSyAR1PTwldO7JESbqp81HVwJRj-2-3cb9qE"
  static let indicatorType = NVActivityIndicatorType.ballBeat 
  
  struct Application {
    static let name = "StylistApp"
  }
  
  struct APIConstants {
    static let accessToken = "ZWFjaHBhbmVsYWZ0ZSBhYWRkZA" //"aUkpUlhhp82nlKhlkh3lLHbkh9jhG76GHl" //IGVhY2ggcGFuZWwgYWZ0ZQ
    static let baseDevAPIURL = "http://192.168.1.154:3030/v1/"
    static let baseProdAPIURL = "http://192.168.1.154:3030/v1/"
   
    //static let baseDevAPIURL = RemoteConfig.remoteConfig().configValue(forKey: "spotter_api_dev").stringValue!
    //static let baseProdAPIURL = RemoteConfig.remoteConfig().configValue(forKey: "spotter_api_prod").stringValue!
  }
  
  struct APIPaths {
    static let sessions = "common/sessions"
    static let getAuthCode = "stylist/auth/mobiles/\(Placeholders.mobileNumber)"
    static let verifyAuthCode = "stylist/auth/mobiles/\(Placeholders.mobileNumber)/code/\(Placeholders.code)"
    static let dashBoardDetails = "stylist/dashboard"
    static let stylistAppointment = "stylist/appointment"
    static let stylistDetails = "stylist/user"
    static let stylistMedia = "stylist/user/media"
    static let stylistPortfolio = "stylist/user/portfolio"
    static let stylistFeedback = "stylist/user/feedback"
    static let stylistHoliday = "stylist/holiday"
    static let stylistLeave = "stylist/leave"
    static let stylistEditLeave = "stylist/leave/\(Placeholders.leaveId)"
    static let requestOTP = "stylist/user/requestOTP"
    static let getNotifications = "stylist/notification"
    static let getNotificationCount = "stylist/notification/count"
    static let notificationSeen = "stylist/notification/\(Placeholders.notificationID)"
  }
  
  struct Placeholders {
    static let mobileNumber = "{mobileNumber}"
    static let code = "{code}"
    static let leaveId = "{leaveId}"
    static let notificationID = "{notificationId}"
  }
  
  struct UserDefaults {
    
    static let customToken = "CustomToken"
    static let userLoginType = "UserLoginType"
    static let alreadyLogIn = "AlreadyLogIn"
    static let StylistDetails = "StylistDetails"
    static let CityDetail = "CityDetail"
    static let DeviceToken = "DeviceToken"
  }
  
  struct CountryCode {
    static var countryCode = "countrycode"
  }
  
  struct Colors {
    static let colorPrimary = UIColor(named: "ColorPrimary")
    static let colorPrimaryDark = UIColor(named: "ColorPrimaryDark")
    static let colorlightgray = UIColor(named: "ColorLightGray")
    static let colorBackground = UIColor(named: "ColorBackground")
    static let colorBox = UIColor(named: "ColorBox")
    static let colorOrange = UIColor(named: "ColorOrange")
  }

  struct Toast {
  
    static let permissionDenied = "Permission denied"
    static let cameraPermissionDenied = "Please allow \(Application.name) to use camera from Settings"
    static let photoLibraryPermissionDenied = "Please allow \(Application.name) to use camera from Settings"
    static let noCameraAvailable = "Sorry, this device has no camera"
    static let noContactAvailable = "Please allow \(Application.name) to access your contacts through the Settings"
    
    static let libraryAccessTitle =  "Please Allow Access to Your Photos"//"Enable Library Access"
    static let libraryAccessMessage = "This allows \(Application.name) to share photos from your library"//"Open Settings\nTap Photos\nSelect 'Read and Write'"
    
    static let cameraAccessTitle = "Take Photos With \(Application.name)"//"Enable Camera Access"
    static let cameraAccessMessage = "Allow access to your camera to start taking photos with the \(Application.name) app" //"Open Settings\nTurn On Camera"
    
    static let openSettings = "Open Settings"
    static let cancel = "Cancel"
    static let exit = "Exit"
    
    static let enableLibraryAccess = "Enable Library Access"
    static let enableCameraAccess = "Enable Camera Access"
    
    static let locationPermissionTitle = "Turn On Location Services"
    static let locationPermissionMessage = "Open Settings\nTap Location\nSelect 'While Using the App'"

      
  }
  
  struct TableViewCell {
    static let ReviewCell = "ReviewCell"
    static let NotificationListCell = "NotificationListCell"
    static let NoReviewCell = "NoReviewCell"
    static let AboutCellTableViewCell = "AboutCellTableViewCell"
    static let LeaveListCell = "LeaveListCell"
    static let NoLeaveCell = "NoLeaveCell"
    static let HolidayListCell = "HolidayListCell"
    static let NoHolidayCell = "NoHolidayCell"
    static let LeaveHeaderCell = "LeaveHeaderCell"
    static let AppointmentCell = "AppointmentCell"
    static let NoAppointmentCell = "NoAppointmentCell"
    static let NotificationCell = "NotificationCell"
  }
  
  struct TableViewCellIdentifier {
    static let ReviewCellID = "ReviewCellID"
    static let NoReviewCellID = "NoReviewCellID"
    static let NotificationListCellID = "NotificationListCellID"
    static let loadMoreCellID = "loadMoreCell"
    static let AboutCellTableViewCellID = "AboutCellTableViewCellID"
    static let LeaveListCellID = "LeaveListCellID"
    static let NoLeaveCellID = "NoLeaveCellID"
    static let HolidayListCellID = "HolidayListCellID"
    static let NoHolidayCellID = "NoHolidayCellID"
    static let LeaveHeaderCellID = "LeaveHeaderCellID"
    static let AppointmentCellID = "AppointmentCellID"
    static let NoAppointmentCellID = "NoAppointmentCellID"
    static let NotificationCellID = "NotificationCellID"
  }
  
  struct CollectionViewCell {
    static let TabBarCollectionViewCell = "TabBarCollectionViewCell"
    static let PortFolioCollectionCell = "PortFolioCollectionCell"
    static let AddPortFolioCollectionCell = "AddPortFolioCollectionCell"
    static let NoPortFolioCell = "NoPortFolioCell"
  }
  
  struct CollectionViewCellIdentifier {
    static let TabBarCollectionViewCellID = "TabBarCollectionViewCellID"
    static let PortFolioCollectionCellID = "PortFolioCollectionCellID"
    static let AddPortFolioCollectionCellID = "AddPortFolioCollectionCellID"
    static let NoPortFolioCellID = "NoPortFolioCellID"
  }
  
  struct NotificationName {
    static let loginOnAddToCart = "loginOnAddToCart"
  }
  
  struct  CommonAlert {
    static let logout = "Are you sure you want to logout?"
    static let deletePortFolio = "Are you sure you want to delete?"
    static let deleteLeave = "Are you sure you want to cancel(delete) your Leave?"
  }

  struct AppointmentStatus {
    static let UNAPPROVED = "UNAPPROVED"
    static let CANCELLED = "CANCELLED"
    static let MISSED = "MISSED"
    static let APPROVED = "APPROVED"
    static let COMPLETED = "COMPLETED"
    static let WAITINGPAYMENT = "WAITINGPAYMENT"
    static let ONGOING = "ONGOING"
    static let NOSHOW = "NO_SHOW"
  }
 
  struct AppointmentStatusText {
    static let UNAPPROVED = "UNAPPROVED"
    static let CANCELLED = "CANCELLED"
    static let MISSED = "EXPIRED"
    static let APPROVED = "APPROVED"
    static let COMPLETED = "COMPLETED"
    static let WAITINGPAYMENT = "WAITING FOR PAYMENT"
    static let ONGOING = "ONGOING"
    static let NOSHOW = "NO SHOW"
  }
 
  
}
