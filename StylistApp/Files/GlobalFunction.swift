
import UIKit



class GlobalFunction: NSObject {
  
  
  // MARK: - Set RootView Controller
  class func setRootViewController() {
   
    if Helper.isKeyPresentInUserDefaults(key: Constants.UserDefaults.alreadyLogIn) == true {
     
      if let alredyLogIn = UserDefaults.standard.bool(forKey:Constants.UserDefaults.alreadyLogIn)  as Bool?, alredyLogIn == true  {
        
        setTabBarViewController()

        
      }else {
      
        setLoginVC()
      }
    }else{
      
      setLoginVC()
    }
    
    
  }
  
  // MARK: - Set RootView Controller
  class func setTabBarViewController() {
   
    if let tabBarController = Constants.kTabBarStoryBoard.instantiateViewController(withIdentifier: "TabBarController") as? TabBarController {
      
      let tabNavVC = UINavigationController(rootViewController: tabBarController)
      
      let sideMenuController = PGSideMenu(animationType: .slideIn)
      sideMenuController.enableMenuPanGesture = false
      sideMenuController.addContentController(tabNavVC)
      
      if let sideMenuVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier: "SideMenuVC") as? SideMenuVC {
        
        let sideNavVC = UINavigationController(rootViewController: sideMenuVC)
        
        sideMenuController.addLeftMenuController(sideNavVC)
      }
      
      Constants.kAppDelegate.window?.setRootViewController(sideMenuController, options: UIWindow.TransitionOptions(direction: .toRight))
      
     // UIApplication.shared.keyWindow?.setRootViewController(sideMenuController, options: UIWindow.TransitionOptions(direction: .toRight))
      
    }
  }
  
  
  // MARK: - Remove Default Setting When User Logout
//  class func removeDefaultSettingWhenLogOut() {
//
//    Helper.clearDocumentDirectory()
//
//    UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.alreadyLogIn)
//    //UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.userLoginType)
//    UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.customToken)
//    //UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.CityDetail)
//
//
//   // controller.selectedIndex = 0
//
//   /* if let tabBarController = Constants.kTabBarStoryBoard.instantiateViewController(withIdentifier: "BubbleTabBarController") as? BubbleTabBarController {
//
//      Helper.clearDocumentDirectory()
//
//      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.alreadyLogIn)
//      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.userLoginType)
//      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.customToken)
//      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.CityDetail)
//
//      let navVC = UINavigationController(rootViewController: tabBarController)
//      UIApplication.shared.keyWindow?.setRootViewController(navVC, options: UIWindow.TransitionOptions(direction: .toLeft))
//
//    }*/
//
//
//
//    /*if let objLoginVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"LoginVC") as? LoginVC {
//
//      Helper.clearDocumentDirectory()
//
//      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.alreadyLogIn)
//      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.userLoginType)
//      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.customToken)
//      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.CityDetail)
//
//      let navVC = UINavigationController(rootViewController: objLoginVC)
//      UIApplication.shared.keyWindow?.setRootViewController(navVC, options: UIWindow.TransitionOptions(direction: .toLeft))
//    }
//*/
//    return
//
//  }
  
  // MARK: - Set Login VC
  class func setLoginVC() {
    
    if let objLoginVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"LoginVC") as? LoginVC {
    
      let navVC = UINavigationController(rootViewController: objLoginVC)
      Constants.kAppDelegate.window?.setRootViewController(navVC, options: UIWindow.TransitionOptions(direction: .toRight))
    }
  }
  
  // MARK: - Logout
  class func logOut() {
    
    if let objLoginVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"LoginVC") as? LoginVC {
    
      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.alreadyLogIn)
      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.customToken)
      UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.StylistDetails)
 
      let navVC = UINavigationController(rootViewController: objLoginVC)
      
      Constants.kAppDelegate.window?.setRootViewController(navVC, options: UIWindow.TransitionOptions(direction: .toLeft))
    }
  }
  
  
  
  // MARK: - Check USer Login Or Not
  class func checkLoggedInUser(_ completion: @escaping(Bool) -> Void) {
    
    if Helper.isKeyPresentInUserDefaults(key: Constants.UserDefaults.alreadyLogIn) == true {
      if let alredyLogIn = UserDefaults.standard.bool(forKey:Constants.UserDefaults.alreadyLogIn)  as Bool?, alredyLogIn == true  {
        completion(true)
      }else {
       completion(false)
      }
    }else{
      completion(false)
    }
    
    
    
  }
/*
  // MARK: - Remove Default Setting When User Logout
  class func openLoginScreen() {
    
    if let objLoginVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"LoginVC") as? LoginVC {
      let navVC = UINavigationController(rootViewController: objLoginVC)
      UIApplication.shared.keyWindow?.setRootViewController(navVC, options: UIWindow.TransitionOptions(direction: .toLeft))
    }
    
    
  }
  
  // MARK: - Add No Internet Connection Screen
  class func addNoInternetConnectionVC(_ parent: UIViewController) -> NoInternetConnectionVC {
   
    let noInternetVC = UIStoryboard(name: "NoInternetConnectionVC", bundle: nil).instantiateViewController(withIdentifier: "NoInternetConnectionVC") as! NoInternetConnectionVC
    
    parent.addChild(noInternetVC)
    noInternetVC.view.bounds = parent.view.bounds
    parent.view.addSubview(noInternetVC.view)
    noInternetVC.didMove(toParent: parent)
    return noInternetVC
    
  }
  
  // MARK: - Add Custom Alert Screen
  class func addCustomAlertVC(_ parent: UIViewController) -> CustomAlertVC {
   
    let customAlertVC = Constants.kCustomAlertStoryBoard.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
    
    parent.addChild(customAlertVC)
    customAlertVC.view.bounds = parent.view.bounds
    parent.view.addSubview(customAlertVC.view)
    customAlertVC.didMove(toParent: parent)
    return customAlertVC
    
  }*/
  

  
  // MARK: - Get Stylist Details
  class func getStylistDetails() -> StylistDetails? {
    let defaults = UserDefaults.standard
    if let stylist = defaults.object(forKey: Constants.UserDefaults.StylistDetails) as? Data {
      let decoder = JSONDecoder()
      if let stylistDetails = try? decoder.decode(StylistDetails.self, from: stylist) {
        return stylistDetails
      }else {
        return nil
      }
    }else {
      return nil
    }
  }
  
  // MARK: - Get Stylist Details
  class func addStylistDetails(_ stylistDetails: StylistDetails?) -> Bool {
    
    let encoder = JSONEncoder()
    if let encoded = try? encoder.encode(stylistDetails) {
      let defaults = UserDefaults.standard
      defaults.set(encoded, forKey: Constants.UserDefaults.StylistDetails)
      return true
    }else {
      return false
    }
  }
 
  
  class func openCustomAlert() -> CommonAlertVC {
    
    let alertVC = CommonAlertVC.init(nibName: "CommonAlertVC", bundle: nil)
    alertVC.modalTransitionStyle = .crossDissolve
    alertVC.modalPresentationStyle = .overCurrentContext
    return alertVC
    
  }
  
  class func getStringFromLatLong(_ lat: Double, long: Double) -> String {
    
    let latitude = String(format: "%.05f", lat)
    
    let longitude = String(format: "%.05f", long)
    
    return "\(latitude), \(longitude)"
    
  }
}
