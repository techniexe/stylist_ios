
import Foundation
import Photos
import UIKit
//import SVProgressHUD
//import Firebase
//import FirebaseAnalytics
//import FirebaseMessaging
//import FirebaseRemoteConfig
//import FBSDKCoreKit
import MapKit

class Helper {
  
  // MARK: - Print
  static func DLog(message: String, function: String = #function) {
    #if DEBUG
    print("\(function): \(message)")
    #endif
  }
  
  // MARK: - Check key is perent or not
  static func isKeyPresentInUserDefaults(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil
  }
  
  // MARK: - Check field is epmpty or not
  static func isMobileNumberEmpty(aStrMobileNumber:String) -> Bool {
    
    if aStrMobileNumber.count == 0 {
      
      return true
    }
    return false
  }
  
  // MARK: - Check Valid Email Address
  static func isValidEmail(_ email: String) -> Bool {
      let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

      let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
      return emailPred.evaluate(with: email)
  }
  
  
  // MARK: - Check Validate Pan Card
  
  static func isValidPAN(_ pannumber: String) -> Bool {
   
    let pancardregEx = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
    
    let pancardPred = NSPredicate(format:"SELF MATCHES %@", pancardregEx)
    
    return pancardPred.evaluate(with: pannumber)
  }
  
  // MARK: - Check Validate GST Card
  
  static func isValidGST(_ gstnumber: String) -> Bool {
    
    let gstcardregEx = "^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$"
    
    let gstcardPred = NSPredicate(format:"SELF MATCHES %@", gstcardregEx)
    
    return gstcardPred.evaluate(with: gstnumber)
  }
  
  // MARK: - Show Toast Message
  static func showToast(_strMessage: String?) {
    
    //ToastCenter.default.isSupportAccessibility = true
    let toast = Toast(text: _strMessage)
    toast.show()
    //let toast = Toast(text: _strMessage, delay: Delay.short, duration: Delay.long)
    //toast.show()
  }
  
  // MARK: - Get Document Directory
  static func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
  }
  
  // MARK: - Clear Document Directory
  static func clearDocumentDirectory() {
    
    let dirPath = getDocumentsDirectory()
    
    do {
      let filePaths = try FileManager.default.contentsOfDirectory(atPath: dirPath.path)
      for filePath in filePaths {
        let path = dirPath.appendingPathComponent(filePath).path
        try FileManager.default.removeItem(atPath: path as String)
      }
    } catch {
      Helper.DLog(message: "Could not clear documents directory: \(error.localizedDescription)")
    }
    
  }
  
  // MARK: - Clear Directory
  static func clearDirectory() {
    
    let dirPath = URL.appDirectory()
    
    do {
      let filePaths = try FileManager.default.contentsOfDirectory(atPath: dirPath.path)
      for filePath in filePaths {
        let path = dirPath.appendingPathComponent(filePath).path
        try FileManager.default.removeItem(atPath: path as String)
      }
    } catch {
      Helper.DLog(message: "Could not clear folder: \(error.localizedDescription)")
    }
    
  }
  
  // MARK : - PhotoLibraryAuthorizationStatus
  static func photoLibraryAuthorizationStatus(_ status: @escaping (Bool) -> Void) {
    
    switch PHPhotoLibrary.authorizationStatus() {
    case .denied:
      status(false)
    case .authorized:
      status(true)
    case .restricted:
      status(false)
    case .notDetermined:
      PHPhotoLibrary.requestAuthorization { granted in
        if granted == .authorized {
          status(true)
        } else {
          status(false)
        }
      }
    case .limited:
      status(false)
    @unknown default:
      fatalError()
    }
    
  }
  
  // MARK: - CameraAuthorizationStatus
  static func cameraAuthorizationStatus(_ status: @escaping (Bool) -> Void) {
    switch AVCaptureDevice.authorizationStatus(for:.video) {
    case .denied:
      status(false)
    case .authorized:
      status(true)
    case .restricted:
      status(false)
    case .notDetermined:
      AVCaptureDevice.requestAccess(for:.video) { (granted) in
        status(granted)
      }
    @unknown default:
      fatalError()
    }
  }
  
  // Helper function inserted by Swift 4.2 migrator.
  static func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
  }
  
  // Helper function inserted by Swift 4.2 migrator.
  static func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
  }
  
  // Add Shadow
  static func addShadow(_ view: UIView, color1: UIColor = UIColor(hexString: "#E8E8E8"), color2: UIColor = UIColor(hexString: "#FFFFFF") ) {
    //gradient layer
    let gradientLayer = CAGradientLayer()
    
    //define colors
    gradientLayer.colors = [color1.cgColor , color2.cgColor]
    
    //define locations of colors as NSNumbers in range from 0.0 to 1.0
    //if locations not provided the colors will spread evenly
    //gradientLayer.locations = [0.0, 0.6, 0.8]
    
    //define frame
    gradientLayer.frame = view.bounds
    
    //insert the gradient layer to the view layer
    view.layer.insertSublayer(gradientLayer, at: 0)
  }
  
  static func createIndicator(_ view: UIView) -> NVActivityIndicatorView {
    
    var y: CGFloat = 0.0
    
    if #available(iOS 11.0, *) {
           let window = UIApplication.shared.keyWindow
           let topPadding = window?.safeAreaInsets.top
           if topPadding == 0.0 {
              y = 20
           }else{
             y = topPadding ?? 0
           }
          
    }else {
      
    }
    
    let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: view.center.x-15, y: y+64, width: 30.0, height: 30.0), type: .ballBeat, color: Constants.Colors.colorPrimary , padding: 0)
    Constants.kAppDelegate.window?.addSubview(activityIndicator)
    return activityIndicator
  }
  
  // MARK: - Show SVProgressHUD
  static func showLoading(_ activityIndicator: NVActivityIndicatorView? = nil) {
    //SVProgressHUD.show()
    //SVProgressHUD.setDefaultStyle(.dark)
    if let activity = activityIndicator {
      activity.startAnimating()
    }
    
  }
  
  // MARK: - Hide SVProgressHUD
  static func hideLoading(_ activityIndicator: NVActivityIndicatorView? = nil) {
    //SVProgressHUD.dismiss()
    if let activity = activityIndicator {
      activity.stopAnimating()
    }
  }
 
  static func getStringFromDoubleWithNumberFormatter(_ number: Double) -> String {
    
    /*let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .currency
    //currencyFormatter.locale = Locale.current
    currencyFormatter.locale = Locale(identifier: "en_IN")
    
    let isInteger = number.truncatingRemainder(dividingBy: 1.0)
    
    if isInteger == 0.0 {
      
      let strTemp = String(format: "%.0f", number)
      
      currencyFormatter.maximumFractionDigits = 0
      
      if let strPrice = currencyFormatter.string(from: NSNumber(value: Int(strTemp) ?? 0)) {
        
        return strPrice.removingWhitespaces()
      }
      
    }else {
      
      if let strPrice = currencyFormatter.string(from: NSNumber(value: number)) {
        
        return strPrice.removingWhitespaces()
      }
    }
    
    return ""
    */
  
    let price = NSNumber(value: number)
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale.current
    //formatter.locale = Locale(identifier: "en_IN")
    if let strPrice = currencyFormatter.string(from: price) {
      return strPrice.removingWhitespaces()
    }
    
    return ""
    
  }
  
  
  static func getStringFromNumberFormatter(_ price: NSNumber) -> String {
    
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale.current
     //formatter.locale = Locale(identifier: "en_IN")
    if let strPrice = currencyFormatter.string(from: price) {
      return strPrice
    }
    
    return ""
  }
  
  
  static func getStringFromDouble(_ qunatity: NSNumber) -> String {
    
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .none
    currencyFormatter.locale = Locale.current
    if let strQunatity = currencyFormatter.string(from: qunatity) {
      return strQunatity
    }
    
    return ""
  }
  
  static func convertDateFormate(date : Date) -> String{
    
    var fullDate: String = ""
    
    // Day
    //let calendar = Calendar.current
    //let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date)
    
    //EEEE h:mm a
    
    // Formate
    let dateFormate = DateFormatter()
    /*dateFormate.dateFormat = "EEE"
    fullDate.append(dateFormate.string(from: date))
    fullDate.append(", ")*/
    
    
    
    /*var day  = "\(anchorComponents.day!)"
    switch (day) {
    case "1" , "21" , "31":
      day.append("st")
    case "2" , "22":
      day.append("nd")
    case "3" ,"23":
      day.append("rd")
    default:
      day.append("th")
    }
    fullDate.append(day)
    fullDate.append(" ")*/
    //dd MMM, yyyy hh:mm a
    dateFormate.dateFormat = "dd" //"MMM"
    fullDate.append(dateFormate.string(from: date))
    fullDate.append(" ")
    //fullDate.append(" '")
    
    dateFormate.dateFormat = "MMM" //"yy"
    fullDate.append(dateFormate.string(from: date))
    fullDate.append(", ")
    //fullDate.append(" - ")
    
    dateFormate.dateFormat = "yyyy"
    fullDate.append(dateFormate.string(from: date))
    fullDate.append(" ")
    
    dateFormate.dateFormat = "hh:mm a"
    fullDate.append(dateFormate.string(from: date).lowercased())
    
    return fullDate
    //return newDay + ", " + day + " " + newDate + " '" + newDate1 + " - " + time.lowercased()
  }
  
  static func numberOfLines(textView: UITextView) -> Int {
      let layoutManager = textView.layoutManager
      let numberOfGlyphs = layoutManager.numberOfGlyphs
      var lineRange: NSRange = NSMakeRange(0, 1)
      var index = 0
      var numberOfLines = 0
      
      while index < numberOfGlyphs {
          layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
          index = NSMaxRange(lineRange)
          numberOfLines += 1
      }
      return numberOfLines
  }
  
  static func getUserLoginType() -> String {
    
    if let strLoginType = UserDefaults.standard.value(forKey: Constants.UserDefaults.userLoginType) as? String {
      return strLoginType
    }
    
    return ""
  }
  
  
  static func setFirebaseCrashlytics(_ value: String, function: String = #function) {
    #if DEBUG
    //FirebaseCrashlytics.Crashlytics.crashlytics().setCustomValue(value, forKey: function)
    #else
    FirebaseCrashlytics.Crashlytics.crashlytics().setCustomValue(value, forKey: function)
    #endif
  }
  
  static func setFirebaseAnalytics(_ screenName: String) {
    #if DEBUG
    /*Analytics.logEvent(AnalyticsEventScreenView,
                       parameters: [AnalyticsParameterScreenName: screenName,
                                    AnalyticsParameterScreenClass: screenName])*/
    #else
    
    Analytics.logEvent(AnalyticsEventScreenView,
                       parameters: [AnalyticsParameterScreenName: screenName,
                                    AnalyticsParameterScreenClass: screenName])
    
    #endif
  }
  
  static func setFacebookAnalytics(_ eventName: String) {
    #if DEBUG
    //AppEvents.logEvent(AppEvents.Name(rawValue: eventName))
    #else
    AppEvents.logEvent(AppEvents.Name(rawValue: eventName))
    #endif
  }
  
  // MARK: - Set Route In Map
  static func setRouteInMap(_ pickup: [Double], deliver: [Double], sourceName: String = "", destName: String = "") {

    
    if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) == true { //comgooglemapsurl
      
      if #available(iOS 10.0, *) {
         UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=\(pickup[1]),\(pickup[0])&daddr=\(deliver[1]),\(deliver[0])&directionsmode=driving&zoom=14&views=traffic")!)
        
      } else {
        //UIApplication.shared.openURL(url)
      }
      
     /* let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: pickup[1], longitude: pickup[0])))
      source.name = sourceName
      let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: deliver[1], longitude: deliver[0])))
      destination.name = destName
      
      MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])*/
      
    } else {
      let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: pickup[1], longitude: pickup[0])))
      source.name = sourceName
      let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: deliver[1], longitude: deliver[0])))
      destination.name = destName
      
      MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
      
    }
    
  }
  
  //MARK:- Set Mobile OTP Text
  static func setMobileOTPText(_ strMobile: String) -> String {
    
    var strFinal: String = ""
    
    strFinal = "Enter the OTP sent on your registered mobile no. \(strMobile.prefix(2))XXXXXX\(strMobile.suffix(2))"
    
    return strFinal
    
  }
 
  //MARK:- Seconds To Hours Minutes Seconds
  static func secondsToHoursMinutesSeconds(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {

    completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)

  }
 
  //MARK:- Get String From Hours Minutes Seconds
  static func getStringFrom(seconds: Int) -> String {

      return seconds < 10 ? "0\(seconds)" : "\(seconds)"
  }
  
  static func tapOnMobileNumber(_ phoneNumber: String) {
    
    guard let number = URL(string: "tel://" + phoneNumber) else { return }
    UIApplication.shared.open(number)
  }
  
  //MARK:- Get Price
  static func getPrice(_ price: Double) -> String {
    
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale(identifier: "en_IN")
    currencyFormatter.minimumFractionDigits = 2
    currencyFormatter.maximumFractionDigits = 2
    
    let number = NSNumber(value: price)
    if let strPrice = currencyFormatter.string(from: number) {
      return strPrice//.removingWhitespaces()
    }
    
    return ""
  }

}
