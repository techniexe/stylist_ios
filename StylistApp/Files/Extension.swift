
import Foundation
import UIKit
import SDWebImage

// MARK: - UIViewController - 
extension UIViewController {
  
  @objc func dismissViewController() {
    self.dismiss(animated:true, completion:nil)
  }
  
  func multiply(lhs: Int, rhs: Double) -> Double {
      return Double(lhs) * rhs
  }
  
  func getProgressCircle() -> UIView {
      
      let parentView = UIStackView()
      parentView.axis = .vertical
      parentView.spacing = 0
      
      let p = RPCircularProgress()
      p.thicknessRatio = 0.1
      p.progressTintColor = Constants.Colors.colorPrimary!
      p.enableIndeterminate()
      parentView.addArrangedSubview(p)
      
      /*let label = UILabel()
      label.text = "Loading, please wait"
      label.textAlignment = .center
      label.textColor = .gray
      label.font = UIFont.systemFont(ofSize: 12)
      parentView.addArrangedSubview(label)*/
      
      return parentView
  }
  
  func enableUserInteraction() {
    self.view.isUserInteractionEnabled = true
  }
  
  func disableUserInteraction() {
    self.view.isUserInteractionEnabled = false
  }
}

// MARK: - UIView -
extension UIView{
  
  @IBInspectable var Corneredius:CGFloat{
    
    get{
      return layer.cornerRadius
    }
    set{
      self.layer.cornerRadius = newValue
      self.layer.masksToBounds = newValue > 0
    }
  }
  @IBInspectable var TWBorderWidth:CGFloat{
    
    get{
      return layer.borderWidth
    }
    set{
      self.layer.borderWidth = newValue
      self.layer.masksToBounds = newValue > 0
    }
  }
  
  @IBInspectable var TWBorderColor:UIColor{
    
    get{
      return self.TWBorderColor
    }
    set{
      self.layer.borderColor = newValue.cgColor
      
    }
  }
  @IBInspectable var TWRoundDynamic:Bool{
    
    get{
      return false
    }
    set{
      if newValue == true {
        
        self.perform(#selector(UIView.afterDelay), with: nil, afterDelay: 0.1)
      }
      
    }
    
  }
  @objc func afterDelay(){
    
    let  Height =  self.frame.size.height
    self.layer.cornerRadius = Height/2;
    self.layer.masksToBounds = true;
    
    
  }
  @IBInspectable var TWRound:Bool{
    get{
      return false
    }
    set{
      if newValue == true {
        self.layer.cornerRadius = self.frame.size.height/2;
        self.layer.masksToBounds = true;
      }
      
    }
  }
  @IBInspectable var TWShadow:Bool{
    get{
      return false
    }
    set{
      if newValue == true {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowOpacity = 0.2;
        
      }
      
    }
    
  }
  @IBInspectable var TWclipsToBounds:Bool{
    get{
      return false
    }
    set{
      if newValue == true {
        
        self.clipsToBounds = true;
      }else{
        self.clipsToBounds = false
      }
      
    }
    
  }
  
  
  func shadowWith(alph:Float){
    self.layer.masksToBounds = false
    self.layer.shadowColor = UIColor.black.cgColor
    self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    self.layer.shadowOpacity = alph
  }
  
  
  func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
    UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
      self.alpha = 1.0
    }, completion: completion)  }
  
  func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 1.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
    UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
      self.alpha = 0.3
    }, completion: completion)
  }
  
  func createDottedLine(x: CGFloat, width: CGFloat, color: CGColor) {
    let caShapeLayer = CAShapeLayer()
    caShapeLayer.strokeColor = color
    caShapeLayer.lineWidth = width
    caShapeLayer.lineDashPattern = [2,3]
    let cgPath = CGMutablePath()
    let cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: x , y: 0)]
    cgPath.addLines(between: cgPoint)
    caShapeLayer.path = cgPath
    layer.addSublayer(caShapeLayer)
  }
  
  func roundCorners(corners: UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    layer.mask = mask
    layer.masksToBounds = false
  }
  
  func lock() {
    if let _ = viewWithTag(10) {
      //View is already locked
    }
    else {
      let lockView = UIView(frame: bounds)
      lockView.backgroundColor = UIColor(white: 0.0, alpha: 0.75)
      lockView.tag = 10
      lockView.alpha = 0.0
      let activity = UIActivityIndicatorView(style: .white)
      activity.color = Constants.Colors.colorPrimary
      activity.hidesWhenStopped = true
      activity.center = lockView.center
      lockView.addSubview(activity)
      activity.startAnimating()
      addSubview(lockView)
      
      UIView.animate(withDuration: 0.2, animations: {
        lockView.alpha = 1.0
      })
    }
  }
  
  func unlock() {
    if let lockView = viewWithTag(10) {
      UIView.animate(withDuration: 0.2, animations: {
        lockView.alpha = 0.0
      }, completion: { finished in
        lockView.removeFromSuperview()
      })
    }
  }
  
  func fadeOut(_ duration: TimeInterval) {
    UIView.animate(withDuration: duration, animations: {
      self.alpha = 0.0
    })
  }
  
  func fadeIn(_ duration: TimeInterval) {
    UIView.animate(withDuration: duration, animations: {
      self.alpha = 1.0
    })
  }
  
  class func viewFromNibName(_ name: String) -> UIView? {
    let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
    return views?.first as? UIView
  }
  
  
  // MARK: - Config
  /// The default duration for fading-animations, measured in seconds.
  public static let defaultFadingAnimationDuration: TimeInterval = 1.0
  
  // MARK: - Public methods
  /// Updates the view visiblity.
  ///
  /// - Parameters:
  ///   - isHidden: The new view visibility.
  ///   - duration: The duration of the animation, measured in seconds.
  ///   - completion: Closure to be executed when the animation sequence ends. This block has no return value and takes a single Boolean
  ///                 argument that indicates whether or not the animations actually finished before the completion handler was called.
  ///
  /// - SeeAlso: https://developer.apple.com/documentation/uikit/uiview/1622515-animatewithduration
  public func animate(isHidden: Bool, duration: TimeInterval = UIView.defaultFadingAnimationDuration, completion: ((Bool) -> Void)? = nil) {
    if isHidden {
      fadeOut(duration: duration,
              completion: completion)
    } else {
      fadeIn(duration: duration,
             completion: completion)
    }
  }
  
  /// Fade out the current view by animating the `alpha` to zero and update the `isHidden` flag accordingly.
  ///
  /// - Parameters:
  ///   - duration: The duration of the animation, measured in seconds.
  ///   - completion: Closure to be executed when the animation sequence ends. This block has no return value and takes a single Boolean
  ///                 argument that indicates whether or not the animations actually finished before the completion handler was called.
  ///
  /// - SeeAlso: https://developer.apple.com/documentation/uikit/uiview/1622515-animatewithduration
  public func fadeOut(duration: TimeInterval = UIView.defaultFadingAnimationDuration, completion: ((Bool) -> Void)? = nil) {
    UIView.animate(withDuration: duration,
                   animations: {
                    self.alpha = 0.0
                   },
                   completion: { isFinished in
                    // Update `isHidden` flag accordingly:
                    //  - set to `true` in case animation was completely finished.
                    //  - set to `false` in case animation was interrupted, e.g. due to starting of another animation.
                    self.isHidden = isFinished
                    
                    completion?(isFinished)
                   })
  }
  
  /// Fade in the current view by setting the `isHidden` flag to `false` and animating the `alpha` to one.
  ///
  /// - Parameters:
  ///   - duration: The duration of the animation, measured in seconds.
  ///   - completion: Closure to be executed when the animation sequence ends. This block has no return value and takes a single Boolean
  ///                 argument that indicates whether or not the animations actually finished before the completion handler was called.
  ///
  /// - SeeAlso: https://developer.apple.com/documentation/uikit/uiview/1622515-animatewithduration
  public func fadeIn(duration: TimeInterval = UIView.defaultFadingAnimationDuration, completion: ((Bool) -> Void)? = nil) {
    if isHidden {
      // Make sure our animation is visible.
      isHidden = false
    }
    
    UIView.animate(withDuration: duration,
                   animations: {
                    self.alpha = 1.0
                   },
                   completion: completion)
  }
  
  
  func addCornerRadiusWithShadow(color: UIColor, borderColor: UIColor, cornerRadius: CGFloat) {
    self.layer.shadowColor = color.cgColor
    self.layer.shadowOffset = CGSize(width: 0, height: 3)
    self.layer.shadowOpacity = 1.0
    self.layer.shadowRadius = 6.0
    self.layer.cornerRadius = cornerRadius
    self.layer.borderColor = borderColor.cgColor
    self.layer.borderWidth = 1.0
    self.layer.masksToBounds = true
  }
  
  func setCornerRadiusWith(radius: Float, borderWidth: Float, borderColor: UIColor) {
    self.layer.cornerRadius = CGFloat(radius)
    self.layer.borderWidth = CGFloat(borderWidth)
    self.layer.borderColor = borderColor.cgColor
    self.layer.masksToBounds = true
  }
  
  func anchor (top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat, enableInsets: Bool) {
      var topInset = CGFloat(0)
      var bottomInset = CGFloat(0)
      
      if #available(iOS 11, *), enableInsets {
          let insets = self.safeAreaInsets
          topInset = insets.top
          bottomInset = insets.bottom
      }
      
      translatesAutoresizingMaskIntoConstraints = false
      
      if let top = top {
          self.topAnchor.constraint(equalTo: top, constant: paddingTop+topInset).isActive = true
      }
      if let left = left {
          self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
      }
      if let right = right {
          rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
      }
      if let bottom = bottom {
          bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom-bottomInset).isActive = true
      }
      if height != 0 {
          heightAnchor.constraint(equalToConstant: height).isActive = true
      }
      if width != 0 {
          widthAnchor.constraint(equalToConstant: width).isActive = true
      }
      
  }
  
}

extension URL {
    
    static func appDirectory() -> URL {
        
      let path = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]).appendingPathComponent(Constants.Application.name)
        
        return path
    }
}

enum ImageStyle: Int {
    case squared,rounded
}

extension UIImageView {
    func setImage(url: String,
                  style: ImageStyle = .squared,
                  completion: ((_ result:Bool,_ error:Error?)->Void)?=nil) {
        
        image = nil
        
        if url.count < 1 {
            return
        }
        
        if(style == .rounded) {
            layer.cornerRadius = frame.height/2
          backgroundColor = Constants.Colors.colorlightgray
            //backgroundColor = UIColor.rgb(from: 0xEDF0F1)
        }else if(style == .squared){
            layer.cornerRadius = 0.0
            backgroundColor =  Constants.Colors.colorlightgray
            //backgroundColor = UIColor.clear
        }
        
        sd_imageIndicator = SDWebImageActivityIndicator.gray
    
        self.sd_setImage(with: URL.init(string: url), placeholderImage:nil, options: [.avoidAutoSetImage,.highPriority,.retryFailed,.delayPlaceholder,.continueInBackground], completed: { (image, error, cacheType, url) in
            if error == nil {
                DispatchQueue.main.async {
                    /*self.backgroundColor = .clear*/
                    //self.alpha = 0;
                   
//                    if  (image?.size.width)! > (image?.size.height)! {
//                        self.contentMode = UIView.ContentMode.scaleAspectFit
//                    }else {
//                         self.contentMode = UIView.ContentMode.scaleAspectFill
//                    }
                   
                    self.image = image
                    self.clipsToBounds = true
                    UIView.animate(withDuration: 0.5, animations: {
                        self.alpha = 1
                    }, completion: { (done) in
                        if let completion = completion {
                            completion(true,error)
                        }
                    })
                }
            }else {
                if let completion = completion {
                    completion(false,error)
                }
            }
        })
    }
}

extension UIImage {

  //Function for store file/Image into local directory. If image is already on the directory then first remove it and replace new image/File on that location
  func storedFileIntoLocal(strImageName:String, quality: CGFloat = 1.0) -> String{
      var strPath = ""
      
      let dirPath = URL.appDirectory()
      
      let isExist = FileManager.default.fileExists(atPath: dirPath.path)
      if(isExist == false)
      {
          do {
              try FileManager.default.createDirectory(atPath: dirPath.path, withIntermediateDirectories: true, attributes: nil)
          } catch {
              print(error)
          }
      }
      
      let imageName:String = strImageName + ".jpg"
      let imagePath = dirPath.appendingPathComponent(imageName).path
      strPath = imagePath
      let fileManager = FileManager.default
      let isFileExist = fileManager.fileExists(atPath: String.init(imagePath))
      if(isFileExist == true)
      {
          do {
              try fileManager.removeItem(atPath: imagePath as String)//removing file if exist
              // print("Remove success")
          } catch {
              print(error)
          }
      }
      let imageData:Data = self.jpegData(compressionQuality: quality)!
      
      do {
          try imageData.write(to: URL(fileURLWithPath: imagePath as String), options: .atomic)
      } catch {
          print(error)
          strPath = "Failed to cache image data to disk"
          return strPath
      }
      
      return strPath
  }
}


// MARK: - UIColor -
extension UIColor {
    
    // MARK: - Initialization
    
    convenience init(hexString: String) {
        
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
            
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to String
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
    class func rgb(from hex: Int,alpha :CGFloat=1.0) -> UIColor {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 0xFF
        let green = CGFloat((hex & 0x00FF00) >> 8) / 0xFF
        let blue = CGFloat(hex & 0x0000FF) / 0xFF
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}



extension Formatter {
  
  static let iso8601: DateFormatter = {
    let formatter = DateFormatter()
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
    return formatter
  }()
  
  static let monthFull: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "MMMM"
    return formatter
  }()
  
  static let monthMedium: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "MMM"
    return formatter
  }()
  
  static let yearMedium: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "YYYY"
    return formatter
  }()
}


extension Date {

  var monthFull: String  { return Formatter.monthFull.string(from: self) }
  var monthMedium: String  { return Formatter.monthMedium.string(from: self) }
  var yearMedium: String  { return Formatter.yearMedium.string(from: self) }
  
  //convert string to date
  static func convertStringToDate(strDate:String, dateFormate strFormate:String) -> Date{
    let dateFormate = DateFormatter()
    dateFormate.dateFormat = strFormate
    dateFormate.timeZone = TimeZone.init(abbreviation: "UTC")
    
    if let dateResult:Date = dateFormate.date(from: strDate) {
      return dateResult
    }
    
    dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    return dateFormate.date(from: strDate)!
  }
  
  //Function for old date format to new format from UTC to local
  static func convertDateUTCToLocal(strDate:String, oldFormate strOldFormate:String, newFormate strNewFormate:String) -> String{
    let dateFormatterUTC:DateFormatter = DateFormatter()
    dateFormatterUTC.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?//set UTC timeZone
    dateFormatterUTC.dateFormat = strOldFormate //set old Format
    if let oldDate:Date = dateFormatterUTC.date(from: strDate)  as Date?//convert date from input string
    {
      dateFormatterUTC.timeZone = NSTimeZone.local//set localtimeZone
      dateFormatterUTC.dateFormat = strNewFormate //make new dateformatter for output format
      dateFormatterUTC.amSymbol = "AM"
      dateFormatterUTC.pmSymbol = "PM"
      if let strNewDate:String = dateFormatterUTC.string(from: oldDate as Date) as String?//convert dateInUTC into string and set into output
      {
        return strNewDate
      }
      return strDate
    }
    return strDate
  }
  
  //Convert without UTC to local
  static func convertDateToLocal(strDate:String, oldFormate strOldFormate:String, newFormate strNewFormate:String) -> String{
    let dateFormatterUTC:DateFormatter = DateFormatter()
    //set local timeZone
    dateFormatterUTC.dateFormat = strOldFormate //set old Format
    if let oldDate:Date = dateFormatterUTC.date(from: strDate) as Date?//convert date from input string
    {
      dateFormatterUTC.timeZone = NSTimeZone.local
      dateFormatterUTC.dateFormat = strNewFormate //make new dateformatter for output format
      dateFormatterUTC.amSymbol = "AM"
      dateFormatterUTC.pmSymbol = "PM"
      if let strNewDate = dateFormatterUTC.string(from: oldDate as Date) as String?//convert dateInUTC into string and set into output
      {
        return strNewDate
      }
      return strDate
    }
    return strDate
  }
  
  //Convert Date to String
  func convertDateToString(strDateFormate:String) -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = strDateFormate
    let strDate = dateFormatter.string(from: self)
    //      dateFormatter = nil
    return strDate
  }
  
  
  //Convert local to utc
  static func convertLocalToUTC(strDate:String, oldFormate strOldFormate:String, newFormate strNewFormate:String) -> String{
    let dateFormatterUTC:DateFormatter = DateFormatter()
    dateFormatterUTC.timeZone = NSTimeZone.local as TimeZone?//set UTC timeZone
    dateFormatterUTC.dateFormat = strOldFormate //set old Format
    if let oldDate:Date = dateFormatterUTC.date(from: strDate)  as Date?//convert date from input string
    {
      dateFormatterUTC.timeZone = NSTimeZone.init(abbreviation: "UTC")! as TimeZone//set localtimeZone
      dateFormatterUTC.dateFormat = strNewFormate //make new dateformatter for output format
      dateFormatterUTC.amSymbol = "AM"
      dateFormatterUTC.pmSymbol = "PM"
      if let strNewDate:String = dateFormatterUTC.string(from: oldDate as Date) as String?//convert dateInUTC into string and set into output
      {
        return strNewDate
      }
      return strDate
    }
    return strDate
  }
  
  //Comparison two date
  static func compare(date:Date, compareDate:Date) -> String{
    var strDateMessage:String = ""
    let result:ComparisonResult = date.compare(compareDate)
    switch result {
    case .orderedAscending:
      strDateMessage = "Future Date"
      break
    case .orderedDescending:
      strDateMessage = "Past Date"
      break
    case .orderedSame:
      strDateMessage = "Same Date"
      break
      
    }
    return strDateMessage
  }
 
  
  static func getElapsedInterval(_ aSourceDate: Date) -> String {
    
    let year = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date())
    
    if year.year! > 0 {
      return year.year! == 1 ? "\(year.year!)" + " " + "year ago" :
        "\(year.year!)" + " " + "years ago"
    }
    
    let aMonth = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date()).month
    if aMonth! > 0 {
      return aMonth == 1 ? "\(aMonth!)" + " " + "month ago" :
        "\(aMonth!)" + " " + "months ago"
    }
    
    let aDay = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date()).day
    if aDay! > 0 {
     
      return aDay! == 1 ? "Yesterday" :
            "\(aDay!)" + " " + "days ago"
    }
    
    let aHour = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date()).hour
    if (aHour != nil) {
      
      if aHour! > 0 {
        return aHour == 1 ? "\(aHour!)" + " " + "hour ago" :
          "\(aHour!)" + " " + "hours ago"
      }
    }
    
    let aMinute = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date()).minute
    
    if (aMinute != nil){
      if aMinute! > 0 {
        return aMinute == 1 ? "\(aMinute!)" + " " + "minute ago" :
          "\(aMinute!)" + " " + "minutes ago"
      }
    }
    
    let aSecond = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute,.second], from: aSourceDate, to: Date()).second
    
    if (aSecond != nil){
      
      if aSecond! > 0 {
        
        return aSecond == 1 ? "\(aSecond!)" + " " + "second ago" :
          "\(aSecond!)" + " " + "seconds ago"
        
      } else {
        
        return "a moment ago"
      }
      
    }
    
    return "a moment ago"
  }
  
  static func getCurrentTimeStamp() -> Float {
    return Float(NSTimeIntervalSince1970 * 1000)
  }
  
  var iso8601: String {
    return Formatter.iso8601.string(from: self)
  }
  
  func dayOfWeek() -> String? {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "EEEE"
    return dateFormatter.string(from: self).capitalized

  }
  
  func startOfMonth() -> Date {
    
    var calendar = Calendar.current
    calendar.timeZone =  NSTimeZone(abbreviation: "UTC")! as TimeZone
    return calendar.date(from: calendar.dateComponents([.year, .month], from: calendar.startOfDay(for: self)))!
  }
   
  func endOfMonth() -> Date {
    var calendar = Calendar.current
    calendar.timeZone =  NSTimeZone(abbreviation: "UTC")! as TimeZone
    
    var components = DateComponents()
    components.day = -1
    components.month = 1
    components.hour = 23
    components.minute = 59
    components.second = 0
    
    return calendar.date(byAdding: components, to: self.startOfMonth())!
    //return calendar.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
  }

}


extension TimeInterval {
  
    private var milliseconds: Int {
        return Int((truncatingRemainder(dividingBy: 1)) * 1000)
    }
    
    private var seconds: Int {
        return Int(self) % 60
    }
    
    private var minutes: Int {
        return (Int(self) / 60 ) % 60
    }
    
    private var hours: Int {
        return Int(self) / 3600
    }
    
    var stringTime: String {
        if hours != 0 {
          
            return String(format:"%02i:%02i:%02i",hours,minutes,seconds)
        } else if minutes != 0 {
          
            return String(format:"%02i:%02i",minutes,seconds)
        } else if milliseconds != 0 {
         
            return String(format:"0:%02i",seconds)
        } else {
          
            return String(format:"0:%02i",seconds)
        }
    }
}

@IBDesignable
class CustomImageView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
}

@IBDesignable
class CustomView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
}

//@IBDesignable
//class CustomButton: UIButton {
//
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0.0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    @IBInspectable var borderColor: UIColor? {
//        didSet {
//            layer.borderColor = borderColor?.cgColor
//        }
//    }
//}


//@IBDesignable
//class CustomLabel: UILabel {
//
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0.0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    @IBInspectable var borderColor: UIColor? {
//        didSet {
//            layer.borderColor = borderColor?.cgColor
//        }
//    }
//}

@IBDesignable
class CustomShadowView: UIView {
  
  @IBInspectable var cornerRadius: CGFloat {
    get{
      return layer.cornerRadius
    }
    set{
      self.layer.cornerRadius = newValue
      self.layer.masksToBounds = newValue > 0
    }
  }
  
  @IBInspectable var shadow:Bool{
    get{
      return false
    }
    set{
      if newValue == true {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero//CGSize(width:1.0, height: 5.0)
        self.layer.shadowOpacity = 0.2 //0.1
        self.layer.maskedCorners = CACornerMask(arrayLiteral: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner])
      }
      
    }
    
  }
  
}

@IBDesignable
class CustomButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
}

extension Int {
    func toFloat() -> CGFloat {
        return CGFloat(self)
    }
}

extension Array {
    func sortedArrayByPosition() -> [Element] {
        return sorted(by: { (obj1 : Element, obj2 : Element) -> Bool in
            
            let view1 = obj1 as! UIView
            let view2 = obj2 as! UIView
            
            let x1 = view1.frame.minX
            let y1 = view1.frame.minY
            let x2 = view2.frame.minX
            let y2 = view2.frame.minY
            
            if y1 != y2 {
                return y1 < y2
            } else {
                return x1 < x2
            }
        })
    }
    
    /// - parameter indexes: Get elements from these indexes
    /// - returns: New array with elements from the indexes specified.
    func at(indexes: Int...) -> [Element] {
        return Dollar.at(self, indexes: indexes)
    }
    
    /// Cycles through the array n times passing each element into the callback function
    ///
    /// - parameter times: Number of times to cycle through the array
    /// - parameter callback: function to call with the element
    func cycle<U>(times: Int, callback: (Element) -> U) {
        Dollar.cycle(self, times, callback: callback)
    }
    
    /// Cycles through the array indefinetly passing each element into the callback function
    ///
    /// - parameter callback: function to call with the element
    func cycle<U>(callback: (Element) -> U) {
        Dollar.cycle(self, callback: callback)
    }
    
    /// For each item in the array invoke the callback by passing the elem
    ///
    /// - parameter callback: The callback function to invoke that take an element
    /// - returns: array itself
    @discardableResult func eachWithIndex(callback: (Int, Element) -> ()) -> [Element] {
        for (index, elem) in self.enumerated() {
            callback(index, elem)
        }
        return self
    }
    
    /// For each item in the array invoke the callback by passing the elem along with the index
    ///
    /// - parameter callback: The callback function to invoke
    /// - returns: The array itself
    @discardableResult func each(callback: (Element) -> ()) -> [Element] {
        self.eachWithIndex { (index, elem) -> () in
            callback(elem)
        }
        return self
    }
    
    /// For each item in the array that meets the when conditon, invoke the callback by passing the elem
    ///
    /// - parameter when: The condition to check each element against
    /// - parameter callback: The callback function to invoke
    /// - returns: The array itself
    @discardableResult func each(when: (Element) -> Bool, callback: (Element) -> ()) -> [Element] {
        return Dollar.each(self, when: when, callback: callback)
    }
    
    /// Checks if the given callback returns true value for all items in the array.
    ///
    /// - parameter callback: Check whether element value is true or false.
    /// - returns: First element from the array.
    func every(callback: (Element) -> Bool) -> Bool {
        return Dollar.every(self, callback: callback)
    }
    
    /// Get element from an array at the given index which can be negative
    /// to find elements from the end of the array
    ///
    /// - parameter index: Can be positive or negative to find from end of the array
    /// - parameter orElse: Default value to use if index is out of bounds
    /// - returns: Element fetched from the array or the default value passed in orElse
    func fetch(index: Int, orElse: Element? = .none) -> Element! {
        return Dollar.fetch(self, index, orElse: orElse)
    }
    
    /// This method is like find except that it returns the index of the first element
    /// that passes the callback check.
    ///
    /// - parameter callback: Function used to figure out whether element is the same.
    /// - returns: First element's index from the array found using the callback.
    func findIndex(callback: (Element) -> Bool) -> Int? {
        return Dollar.findIndex(self, callback: callback)
    }
    
    /// This method is like findIndex except that it iterates over elements of the array
    /// from right to left.
    ///
    /// - parameter callback: Function used to figure out whether element is the same.
    /// - returns: Last element's index from the array found using the callback.
    func findLastIndex(callback: (Element) -> Bool) -> Int? {
        return Dollar.findLastIndex(self, callback: callback)
    }
    
    /// Gets the first element in the array.
    ///
    /// - returns: First element from the array.
    func first() -> Element? {
        return Dollar.first(self)
    }
    
    /// Flattens the array
    ///
    /// - returns: Flatten array of elements
    func flatten() -> [Element] {
        return Dollar.flatten(self)
    }
    
    /// Get element at index
    ///
    /// - parameter index: The index in the array
    /// - returns: Element at that index
    func get(index: Int) -> Element! {
        return self.fetch(index: index)
    }
    
    /// Gets all but the last element or last n elements of an array.
    ///
    /// - parameter numElements: The number of elements to ignore in the end.
    /// - returns: Array of initial values.
    func initial(numElements: Int? = 1) -> [Element] {
        return Dollar.initial(self, numElements: numElements!)
    }
    
    /// Gets the last element from the array.
    ///
    /// - returns: Last element from the array.
    func last() -> Element? {
        return Dollar.last(self)
    }
    
    /// The opposite of initial this method gets all but the first element or first n elements of an array.
    ///
    /// - parameter numElements: The number of elements to exclude from the beginning.
    /// - returns: The rest of the elements.
    func rest(numElements: Int? = 1) -> [Element] {
        return Dollar.rest(self, numElements: numElements!)
    }
    
    /// Retrieves the minimum value in an array.
    ///
    /// - returns: Minimum value from array.
    func min<T: Comparable>() -> T? {
        return Dollar.min(map { $0 as! T })
    }
    
    /// Retrieves the maximum value in an array.
    ///
    /// - returns: Maximum element in array.
    func max<T: Comparable>() -> T? {
        return Dollar.max(map { $0 as! T })
    }
    
    /// Gets the index at which the first occurrence of value is found.
    ///
    /// - parameter value: Value whose index needs to be found.
    /// - returns: Index of the element otherwise returns nil if not found.
    func indexOf<T: Equatable>(value: T) -> Int? {
        return Dollar.indexOf(map { $0 as! T }, value: value)
    }
    
    /// Remove element from array
    ///
    /// - parameter value: Value that is to be removed from array
    /// - returns: Element at that index
    mutating func remove<T: Equatable>(value: T) -> T? {
        if let index = Dollar.indexOf(map { $0 as! T }, value: value) {
            return (remove(at: index) as? T)
        } else {
            return .none
        }
    }
    
    /// Checks if a given value is present in the array.
    ///
    /// - parameter value: The value to check.
    /// - returns: Whether value is in the array.
    func contains<T: Equatable>(value: T) -> Bool {
        return Dollar.contains(map { $0 as! T }, value: value)
    }
    
    /// Return the result of repeatedly calling `combine` with an accumulated value initialized
    /// to `initial` on each element of `self`, in turn with a corresponding index.
    ///
    /// - parameter initial: the value to be accumulated
    /// - parameter combine: the combiner with the result, index, and current element
    /// - throws: Rethrows the error generated from combine
    /// - returns: combined result
    func reduceWithIndex<T>(initial: T, combine: (T, Int, Array.Iterator.Element) throws -> T) rethrows -> T {
        var result = initial
        for (index, element) in self.enumerated() {
            result = try combine(result, index, element)
        }
        return result
    }
    
    /// Checks if the array has one or more elements.
    ///
    /// - returns: true if the array is not empty, or false if it is empty.
    public var isNotEmpty: Bool {
        get {
            return !self.isEmpty
        }
    }
    
    /// Move item in array from old index to new index
    ///
    /// - parameter oldIndex: index of item to be moved
    /// - parameter newIndex: index to move item to
    mutating func moveElement(at oldIndex: Int, to newIndex: Int) {
        let element = self[newIndex]
        self[newIndex] = self[oldIndex]
        self[oldIndex] = element
    }
}

extension Array where Element: Hashable {
    
    /// Creates an object composed from arrays of keys and values.
    ///
    /// - parameter values: The array of values.
    /// - returns: Dictionary based on the keys and values passed in order.
    public func zipObject<T>(values: [T]) -> [Element:T] {
        return Dollar.zipObject(self, values: values)
    }
}

/// Overloaded operator to appends another array to an array
///
/// - parameter left: array to insert elements into
/// - parameter right: array to source elements from
/// - returns: array with the element appended in the end
public func<<<T>(left: inout [T], right: [T]) -> [T] {
    left += right
    return left
}

/// Overloaded operator to append element to an array
///
/// - parameter array: array to insert element into
/// - parameter elem: element to insert
/// - returns: array with the element appended in the end
public func<<<T>( array: inout [T], elem: T) -> [T] {
    array.append(elem)
    return array
}

/// Overloaded operator to remove elements from first array
///
/// - parameter left: array from which elements will be removed
/// - parameter right: array containing elements to remove
/// - returns: array with the elements from second array removed
public func -<T: Hashable>(left: [T], right: [T]) -> [T] {
    return Dollar.difference(left, right)}


extension UIButton {
    
    func startAnimatingPressActions() {
        addTarget(self, action: #selector(animateDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(animateUp), for: [.touchDragExit, .touchCancel, .touchUpInside, .touchUpOutside])
    }
    
    @objc private func animateDown(sender: UIButton) {
        animate(sender, transform: CGAffineTransform.identity.scaledBy(x: 0.89, y: 0.89))
    }
    
    @objc private func animateUp(sender: UIButton) {
        animate(sender, transform: .identity)
    }
    
    private func animate(_ button: UIButton, transform: CGAffineTransform) {
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 3,
                       options: [.curveEaseInOut],
                       animations: {
                        button.transform = transform
        }, completion: nil)
    }
    
}

extension Double {
    func truncate(places: Int) -> Double {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}

extension UITextField {
  
//  func setPlaceholderText(_ strText: String) {
//
//    let attributedPlaceholder = NSAttributedString(string: strText, attributes: [NSAttributedString.Key.foregroundColor: Constants.Colors.PlaceholderColor!, NSAttributedString.Key.font: MainFont.regular.with(size: 16.0)])
//    self.attributedPlaceholder = attributedPlaceholder
//    
//  }
  
}

extension String {
 
  func removingWhitespaces() -> String {
    return components(separatedBy: .whitespaces).joined()
  }
  
  var dateFromISO8601: Date? {
      return Formatter.iso8601.date(from: self)
  }
  
  func trim()->String{
      return self.trimmingCharacters(in: .whitespacesAndNewlines)
  }
  
//  func trimCount()->Int {
//    return self.trimmingCharacters(in: .whitespacesAndNewlines).count
//  }
}


extension UILabel {
  
  func setText(_ name: String) {
    self.text = name
  }
  
  func setOrderID(_ strDisplayId: String) {
    self.text = String(format: "Order ID : #%@",strDisplayId)
  }
  
  func setOrderDate(_ strDate: String) {
    self.text = String(format: "Order Date : %@", strDate)
  }
  
  func setPhoneNumber(_ strPhone: String) {
    self.text = String(format: "Phone No : %@", strPhone)
  }
  

  
//
//  func setSourceName(_ strSourceName: String, fontsize: CGFloat = 12.0) {
//
//    let strTitle = String(format: "Source : %@", strSourceName) as NSString
//
//    let strMutableAttributed =  NSMutableAttributedString(string: strTitle as String)
//
//    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "Source : "))
//
//    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize),NSAttributedString.Key.foregroundColor: Constants.Colors.orangeColor!], range: strTitle.range(of: "\(strSourceName)"))
//
//    self.attributedText = strMutableAttributed
//
//  }
  
  
  
  
  func setRating(_ ratingSum: Double = 0.0, ratingCount: Double = 0.0, rating: Double) {
    
    
    let objRating = Int(rating)
    
    if objRating == 0 {
      self.text = "0"
    }else {
      self.text =  String(format: "%.1f", rating)
    }
    
    
    /*guard ratingSum == 0.0 && ratingCount == 0.0 else {
     
      let ratings = ratingSum / ratingCount
      
      let ourRatings = Double(round(10 * ratings)/10)
      
//      let currencyFormatter = NumberFormatter()
//      currencyFormatter.numberStyle = .none
//      currencyFormatter.locale = Locale.current
//
//      if let strRating = currencyFormatter.string(from: NSNumber(value: ourRatings)) {
//        self.text = strRating
//      }else {
//        self.text = ""
//      }
      
      if ourRatings == 0.0 {
        self.text = "0"
      }else {
        self.text =  String(format: "%.1f", ourRatings)
      }
      
      
      
      return
    }
    
    self.text =  "0" */
    
  }
  
  
  func setDistance(_ calculatedDistance: NSNumber) {
    
   /* let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .decimal
    currencyFormatter.locale = Locale.current*/
    
    let double = Double(exactly: calculatedDistance)
    
    self.text = String(format: "%.2f km", double!)
    
   /* if let strDistance = currencyFormatter.string(from: calculatedDistance) {
      self.text = strDistance + " km"
    }else {
      self.text = ""
    }*/
    
    //let distance = round(Double(truncating: calculatedDistance))
    //self.text = String(format: "%.02f km", distance)
  }
  
  
  func setAvailability(_ strQty: String, strUnit: String, fontsize: CGFloat = 12.0) {
  
    var strStock: String = ""
    var color: UIColor!
    var strTitle: NSString!
   
    if strQty == "0" {
      strStock = "Out of stock"
      color = UIColor.lightGray
      strTitle = "Availability : \(strStock) " as NSString
    }else {
      strStock = "In stock"
      color = UIColor.init(hexString: "#259221")
      strTitle = "Availability : \(strStock) (\(strQty) \(strUnit))" as NSString
    }
    
    
    let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "Availability : "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.bold.with(size: fontsize),NSAttributedString.Key.foregroundColor: color!], range: strTitle.range(of: "\(strStock) "))
    
    if strStock == "In stock"{
      strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "(\(strQty) \(strUnit))"))
    }
    
    self.attributedText = strMutableAttributed
    
  }
  
  
  func setTotalPrice(_ quantity: Int, unitPrice: Double) {
    
    var strTotalPrice: NSString = ""
    
    var totalPrice: Double = 0.0
    
    totalPrice = Double(quantity) * unitPrice
    
    var items: String = ""
    
    if quantity == 1 {
      
      items = "Item"
      strTotalPrice = "\(quantity) \(items)\nTotal Price : \(Helper.getStringFromDoubleWithNumberFormatter(totalPrice))\nInc.of all taxes" as NSString
      
    }else {
      items = "Items"
      strTotalPrice = "\(quantity) \(items)\nTotal Price : \(Helper.getStringFromDoubleWithNumberFormatter(totalPrice))\nInc.of all taxes" as NSString
    }
  
    let strMutableAttributed =  NSMutableAttributedString(string:strTotalPrice as String)

    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.medium.with(size: 10.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTotalPrice.range(of: "\(quantity) \(items)\n"))

    strMutableAttributed.addAttributes([NSAttributedString.Key.font:MainFont.medium.with(size: 15.0),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTotalPrice.range(of: "Total Price : \(Helper.getStringFromDoubleWithNumberFormatter(totalPrice))\n"))

    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: 10.0),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTotalPrice.range(of: "Inc.of all taxes"))

    self.attributedText = strMutableAttributed
    
  }
  
  func setCount(_ count: NSNumber) {
    
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .none
    currencyFormatter.locale = Locale.current
    if let strCount = currencyFormatter.string(from: count) {
      self.text = strCount
    }else {
      self.text = ""
    }
  }
}



extension UIButton {
  
  func setWishlisted(_ iswishlisted: Bool?) {
    
    if iswishlisted == true{
       self.isSelected = true
     }else {
       self.isSelected = false
     }
    
  }
  
}

extension UIScreen {

  var width: CGFloat {
    return UIScreen.main.bounds.size.width
  }
  var height: CGFloat {
    return UIScreen.main.bounds.size.height
  }
}

extension UIAlertController {
 
  override open func viewDidLoad() {
    super.viewDidLoad()
    pruneNegativeWidthConstraints()
  }
  
  func pruneNegativeWidthConstraints() {
    for subView in self.view.subviews {
      for constraint in subView.constraints where constraint.debugDescription.contains("width == - 16") {
        subView.removeConstraint(constraint)
      }
    }
  }
  
  //Set background color of UIAlertController
  func setBackgroundColor(color: UIColor) {
      if let bgView = self.view.subviews.first, let groupView = bgView.subviews.first, let contentView = groupView.subviews.first {
          contentView.backgroundColor = color
      }
  }
  
  //Set title font and title color
  func setTitle(font: UIFont?, color: UIColor?) {
      guard let title = self.title else { return }
      let attributeString = NSMutableAttributedString(string: title)//1
      if let titleFont = font {
          attributeString.addAttributes([NSAttributedString.Key.font : titleFont],//2
                                        range: NSMakeRange(0, title.utf8.count))
      }
      
      if let titleColor = color {
          attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor],//3
                                        range: NSMakeRange(0, title.utf8.count))
      }
      self.setValue(attributeString, forKey: "attributedTitle")//4
  }
  
  //Set message font and message color
  func setMessage(font: UIFont?, color: UIColor?) {
      guard let message = self.message else { return }
      let attributeString = NSMutableAttributedString(string: message)
      if let messageFont = font {
          attributeString.addAttributes([NSAttributedString.Key.font : messageFont],
                                        range: NSMakeRange(0, message.utf8.count))
      }
      
      if let messageColorColor = color {
          attributeString.addAttributes([NSAttributedString.Key.foregroundColor : messageColorColor],
                                        range: NSMakeRange(0, message.utf8.count))
      }
      self.setValue(attributeString, forKey: "attributedMessage")
  }
  
  //Set tint color of UIAlertController
  func setTint(color: UIColor) {
      self.view.tintColor = color
  }
}





public extension UIWindow {
  
  /// Transition Options
   struct TransitionOptions {
    
    /// Curve of animation
    ///
    /// - linear: linear
    /// - easeIn: ease in
    /// - easeOut: ease out
    /// - easeInOut: ease in - ease out
    public enum Curve {
      case linear
      case easeIn
      case easeOut
      case easeInOut
      
      /// Return the media timing function associated with curve
      internal var function: CAMediaTimingFunction {
        let key: String!
        switch self {
        case .linear:    key = CAMediaTimingFunctionName.linear.rawValue
        case .easeIn:    key = CAMediaTimingFunctionName.easeIn.rawValue
        case .easeOut:    key = CAMediaTimingFunctionName.easeOut.rawValue
        case .easeInOut:  key = CAMediaTimingFunctionName.easeInEaseOut.rawValue
        }
        return CAMediaTimingFunction(name: CAMediaTimingFunctionName(rawValue: key))
      }
    }
    
    /// Direction of the animation
    ///
    /// - fade: fade to new controller
    /// - toTop: slide from bottom to top
    /// - toBottom: slide from top to bottom
    /// - toLeft: pop to left
    /// - toRight: push to right
    public enum Direction {
      case fade
      case toTop
      case toBottom
      case toLeft
      case toRight
      
      /// Return the associated transition
      ///
      /// - Returns: transition
      internal func transition() -> CATransition {
        let transition = CATransition()
        transition.type = CATransitionType.push
        switch self {
        case .fade:
          transition.type = CATransitionType.fade
          transition.subtype = nil
        case .toLeft:
          transition.subtype = CATransitionSubtype.fromLeft
        case .toRight:
          transition.subtype = CATransitionSubtype.fromRight
        case .toTop:
          transition.subtype = CATransitionSubtype.fromTop
        case .toBottom:
          transition.subtype = CATransitionSubtype.fromBottom
        }
        return transition
      }
    }
    
    /// Background of the transition
    ///
    /// - solidColor: solid color
    /// - customView: custom view
    public enum Background {
      case solidColor(_: UIColor)
      case customView(_: UIView)
    }
    
    /// Duration of the animation (default is 0.20s)
    public var duration: TimeInterval = 0.20
    
    /// Direction of the transition (default is `toRight`)
    public var direction: TransitionOptions.Direction = .toRight
    
    /// Style of the transition (default is `linear`)
    public var style: TransitionOptions.Curve = .linear
    
    /// Background of the transition (default is `nil`)
    public var background: TransitionOptions.Background? = nil
    
    /// Initialize a new options object with given direction and curve
    ///
    /// - Parameters:
    ///   - direction: direction
    ///   - style: style
    public init(direction: TransitionOptions.Direction = .toRight, style: TransitionOptions.Curve = .linear) {
      self.direction = direction
      self.style = style
    }
    
    public init() { }
    
    /// Return the animation to perform for given options object
    internal var animation: CATransition {
      let transition = self.direction.transition()
      transition.duration = self.duration
      transition.timingFunction = self.style.function
      return transition
    }
  }
  
  
  /// Change the root view controller of the window
  ///
  /// - Parameters:
  ///   - controller: controller to set
  ///   - options: options of the transition
  func setRootViewController(_ controller: UIViewController, options: TransitionOptions = TransitionOptions()) {
    
    var transitionWnd: UIWindow? = nil
    
    if let background = options.background {
      transitionWnd = UIWindow(frame: UIScreen.main.bounds)
      switch background {
      case .customView(let view):
        transitionWnd?.rootViewController = UIViewController.newController(withView: view, frame: transitionWnd!.bounds)
      case .solidColor(let color):
        transitionWnd?.backgroundColor = color
      }
      transitionWnd?.makeKeyAndVisible()
    }
    
    // Make animation
    self.layer.add(options.animation, forKey: kCATransition)
    self.rootViewController = controller
    self.makeKeyAndVisible()
    
    if let wnd = transitionWnd {
      DispatchQueue.main.asyncAfter(deadline: (.now() + 1 + options.duration), execute: {
        wnd.removeFromSuperview()
      })
    }
  }
}

internal extension UIViewController {
  
  /// Create a new empty controller instance with given view
  ///
  /// - Parameters:
  ///   - view: view
  ///   - frame: frame
  /// - Returns: instance
  static func newController(withView view: UIView, frame: CGRect) -> UIViewController {
    view.frame = frame
    let controller = UIViewController()
    controller.view = view
    return controller
  }
  
}
