
import Foundation
import UIKit
//Enum type for ListView
//enum ListType {
//
//    case country
//    case user
//    case spotterPlace
//    case googlePlace
//    case trailCompanions
//    case trailBuddies
//    case trailFollowers
//
//    func list() -> Array<Any> {
//        switch self {
//        case .country:
//            return CountryInfo.shared.countries
//        default: break
//        }
//        return Array()
//    }
//}

enum MediaType: Int {
  case `default` = 0
  case image
  case video
  case audio
  case note
  case feeling
  case gif

  func name() -> String {
    switch self {
    case .image:
      return "image"
    case .video:
      return "video"
    case .audio:
      return "audio"
    case .note:
      return "note"
    case .feeling:
      return "feelings"
    case .gif:
        return "gif"
    default:
      return "none"
    }
  }
}

//enum SignupType: Int {
//  case `default` = 0
//  case mobile
//  case email
//  
//  func name() -> String {
//    switch self {
//    case .mobile:
//      return "Mobile"
//    case .email:
//      return "Email"
//    default:
//      return "none"
//    }
//  }
//}

enum FromScreen {
  case isFromDetail
  case isFromTab
}

enum CommonAlert {
  case logout
  case deletePortFlio
}

enum LogInType: Int {
  case `default` = 0
  case mobile
  case email
  case facebook
  case google
  
  func name() -> String {
    switch self {
    case .mobile:
      return "Mobile"
    case .email:
      return "Email"
    case .facebook:
      return "Facebook"
    case .google:
      return "Google"
    default:
      return "none"
    }
  }
}

typealias MainFont = Font.Roboto

enum Font {
  
  enum Roboto: String {
    case regular = "Regular"
    case medium = "Medium"
    case bold = "Bold"
    
    func with(size: CGFloat) -> UIFont {
      return UIFont(name: "Roboto-\(rawValue)", size: size)!
    }
  }
}

enum DateFormats:String {

  case iso = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
  case birth = "dd MMM, yyyy"
  case display = "dd MMM yyyy"
  case orderDateTime = "dd MMM, yyyy hh:mm a"
  case fetched = "yyyy-MM-dd"
  case generic = "yyyy-MM-dd hh:mm:ss Z"
  case created = "yyyy-MM-dd'T'HH:mm:ss'Z'"
  case createdTime = "HH:mm:ss"
  case birthdate = "dd-MM-yyyy"
  case displayNewTrailCardDateFormate = "dd-MMM-yyyy"
  case lastSeenUser = "EEEE h:mm a"
  case createMeetup = "yyyy-MM-dd'T'HH:mm:ss"
  case flipkart = "EEEE, ddth MMM'yy" //Fri, 17th Oct'14
  case startTime = "hh:mm a"
  case appointment = "yyyy-MM-dd'T'HH:mm:ss.SSS"
  
  
  func format() -> String {
    return rawValue
  }
}

enum Title: String {
  case aboutus = "About Us"
  case payments = "Payments"
  case cancel = "Return & Cancellations"
  case warranty = "Warranty & Gurantee"
  case shipping = "Delivery Policy"
  case privacy = "Privacy Policy"
  case terms = "Terms of Use"
  case help = "Help & FAQs"
}

enum Profile {
  
  enum Placeholder: String {
   
    case personname = "e.g. Jhondoe"
    case mobile = "e.g. +919898989898"
    case email = "e.g. jhon@example.com"
    case password = "e.g. 123456"
    case pan = "e.g. AAABB0000C"
    case gst = "e.g. 00AABBC0000A1BB"
    
  }
}

enum BuyerSite {
  
  enum Placeholder: String {
   
    case sitename = "e.g. Techniexe Infolabs"
    case personname = "e.g. Techniexe"
    case mobile = "e.g. +919888888888"
    case email = "e.g. test@gmail.com"
    case addline1 = "e.g. 104, sumel-2"
    case addline2 = "e.g. s.g.highway"
    case pincode = "e.g. 380054"
    case coordinates = "e.g. 77.38066792488098, 28.6517752463408"
  }
}


enum ErrorMessage: String {

  case pleaselogin = "Login required"
  case invalidcred = "Please enter the valid credentials"
  case name = "Please enter your name"
  case nameminlength = "Name must be of minimum 3 characters length"
  case mobile = "Please enter mobile number"
  //case invalidmobile = "Invalid mobile number."
  case invalidmobileregioncode = "Invalid mobile number" //"Invalid mobile number/region code."
  case mobileemail = "Please enter your registered mobile number/email to log-in"
  case requiredmobileemail = "Mobile Number/Email is required"
  case requiredmobile = "Mobile Number is required to create an account"
  case password = "Please enter your password"
  case passwordlength = "Password must be of minimum 6 characters length"
  case confirmpassword = "Please enter your confirm password"
  case passnotmatch = "Password and confirm password does not match"
  case email = "Please enter email address"
  case invalidemail = "Please enter valid email address"
  case invalidemailmobile = "Please enter valid mobile number or email address"
  case pan = "Please enter PAN number"
  case panminlength = "Pan number must be of 10 characters length"
  case invalidPAN = "Invalid PAN number"
  case gst = "Please enter GST number"
  case gstminlength = "GST number must be of 15 characters length"
  case invalidGST = "Invalid GST number"
  case invalidcode = "Code you have entered is invalid"
  case noNetwork = "It seems you are not connected to Internet. Please try after connecting to Internet"
  case networkLost = "It seems your Internet connection is lost. Please try after connecting to Internet"
  case timeOut = "It is taking too long. Please try again later"
  case noConnection = "There is some problem when connecting to host. Please try again later"
  case sessionexpire = "Your session has been expired, Please login again"
  case loginFailed = "Login Failed, Please Try again!!"
  case signupFailed = "Signup Failed, Please Try again!!"
  case companyName = "Please enter company name"
  case siteName = "Please enter site name"
  case contactPersonName = "Please enter contact person name"
  case addressLine1 = "Please enter address line1"
  case addressLine2 = "Please enter address line2"
  case invaidcountry = "Please select country"
  case invaidsate = "Please select state"
  case invaidcity = "Please select city"
  case pincode = "Please enter pincode"
  case pincodelength = "Pincode must be of 6 characters length"
  case location = "Please select location"
  case newpassword = "Please enter password"
  case confirmnewpassword = "Please enter confirm password"
  //case passwordmismatch = "Password and confirm password does not match."
  case deliveryaddDetails = "Please select delivery address" //Please add delivery address detail."
  case addreview = "Please add review"
  case addrating = "Please add rating"
  case quantity = "Please enter quantity"
  case complain = "Please enter your complain"
  case enterLeaveReason = "Please enter your leave reason"
  case enterStartDate = "Please enter start date"
  case enterEndDate = "Please enter end date"
  case greaterEndDate = "End date must greater than the start date"
  case medialimit = "Media can not greater than 5"
  case addMedia = "Please add media"
  
  func message() -> String {
    return rawValue
  }
}


enum SuccessMessage: String {

  case loginsuccess = "Login successfully"
  case signupsucess = "Signup successfully"
  case codesuccess = "OTP sent successfully"
  case passchangesuccess = "Password has been changed successfully"
  case passresetsuccess = "Password has been reset successfully"
  case itemaddwishlist = "Item added to wishlist successfully"
  case itemremovewishlist = "Item remove from wishlist successfully"
  case itemaddcart = "Item added to cart successfully"
  case quantityupdate = "Quantity updated successfully"
  case itemremovecart = "Item remove from cart successfully"
  case productreviewaddsuccess = "Thank You for your review. Your review sent for approval" //"Review added sucessfully."
  case productrevieweditsuccess = "Your review updated successfully"
  case siteaddsuccess = "Delivery address added successfully"
  case sitedeletesuccess = "Delivery address deleted successfully"
  case siteupdatesuccess = "Delivery address updated successfully"
  case profileupdatesuccess = "Profile updated successfully"
  case ordercancel = "Order cancelled successfully"
  case orderItemCancel = "Order Item cancelled successfully"
  case complaintaddsuccess = "Complaint added successfully"
  case orderreceive = "Thank You! We have received your order"
  case pricechange = "Price may have got changed. Kindly check"
  case leaveAppliedSuccess = "Leave applied successfully"
  case leaveEditedSuccess = "Leave edited successfully"
  case portflioDeletedSuccess = "Stylist portfolio deleted successfully"
  case profilePicSuccess = "Profile picture uploaded successfully"
  case portfolioPicSuccess = "Portfolio media uploaded successfully"
  case leaveCancelSuccess = "Leave cancelled successfully"
  
  func message() -> String {
    return rawValue
  }
}


//enum OrderStatus: String {
//  case PLACED = "PLACED"
//  case PROCESSING = "PROCESSING"
//  case CONFIRMED = "CONFIRMED"
//  case DELIVERED = "DELIVERED"
//  case CANCELLED = "CANCELLED"
//  case PROCESSED = "PROCESSED"
//  case DISPACHED = "DISPATCHED"
//  case REJECT = "REJECT"
//  case REFUNDED = "REFUNDED"
//
//}

//enum EventStatus: String {
//  case PROCESSING = "PROCESSING"
//  //case PROCESSED = "PROCESSED"
//  case TRUCKASSIGNED = "TRUCK_ASSIGNED_FOR_PICKUP"
//  case PICKUP = "PICKUP"
//  case DELAYED = "DELAYED"
//  case DELIVERED = "DELIVERED"
//  
//  
//  func position() -> Int {
//    switch self {
//    case .PROCESSING:
//      return 0
//    /*case .PROCESSED:
//      return 1*/
//    case .TRUCKASSIGNED:
//      return 1
//    case .PICKUP:
//      return 2
//    case .DELAYED:
//      return 3
//    case .DELIVERED:
//      return 4
//    default:
//      return 0
//    }
//  }
//}

enum CardViewState {
  case expanded
  case normal
}

enum MinLength: Int {
  
  case personname = 3
  case password = 6
}

enum MaxLength: Int {
  
  case personname = 64
  case mobile = 13
  case password = 20
  case pan = 10
  case gst = 15
  case leaveReason = 250
  case complain = 249
  case pincode = 6
  case address = 500
  
  func getMaxLength() -> Int {
    return rawValue
  }
}

enum DragDirection {
    
    case Up
    case Down
}

enum StylistLeaveOperation {
  
  case Add
  case Edit
}

enum StylistNotificationType: Int {
  case appointmentReschedule = 1
  case appointmentSchedule
  case cancelAppointmentSchedule
  case appointmentComplete
  case appointmentOngoing
  case addReview
}
