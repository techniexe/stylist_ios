
import Foundation
import UIKit

@objc public protocol CustomDelegate {
   
  @objc optional func canTryAgain()
  @objc optional func getLocation()
  @objc optional func collectionView(_ indexPath: IndexPath)
  @objc optional func pressRFQ()
  @objc optional func pressSeeAll()
  @objc optional func wishlist(_ tag: Int)
  @objc optional func distance(_ tag: Int)
  @objc optional func cartItemDelete(_ tag: Int)
  @objc optional func cartItemMinus(_ tag: Int)
  @objc optional func cartItemPlus(_ tag: Int)
  @objc optional func cartItemUpdateQty(_ tag: Int)
  @objc optional func cartItemQtyChange(_ tag: Int)
  @objc optional func actionCustomAlertDialog(_ aButton: UIButton)
  @objc optional func selectAddress(_ aButton: UIButton)
  @objc optional func deleteEditAddress(_ aButton: UIButton, isEdit: Bool)
  @objc optional func addNewAddressSite()
  @objc optional func updateProfile()
  @objc optional func updateSupplierList(_ isWishlist: Bool)
  @objc optional func redirectToHomeTab()
  @objc optional func applyFilter(_ strSourceID: String?, strSourceName: String?, strRegionName: String?)
  @objc optional func applySorting(_ strText: String, strOrderBy: String, selectedIndex: Int)
  @objc optional func complaint()
  @objc optional func addReview(_ isUpdate: Bool)
  @objc optional func selectState(_ name: String, selectedIndex: Int)
  @objc optional func selectCity(_ name: String, selectedIndex: Int)
  @objc optional func checkout()
  @objc optional func close()
  @objc optional func applyDate(_ month: String, year: Int)
}

protocol AppCodable: Codable {}

public struct AppData<T: Codable>: Codable {
  public var data: T
}

public struct AppSessionData: AppCodable {
  
  #if DEBUG
  var accessToken = Constants.APIConstants.accessToken
  var registrationToken = UUID().uuidString
  #else
  var accessToken = Constants.APIConstants.accessToken
  var registrationToken = UUID().uuidString
  #endif
}


public struct SessionToken: Codable {
  
  public var token : String?

  public init(token: String?) {
      self.token = token
  }
}


public struct CustomToken: Codable {
  
  public var customToken : String?

  public init(token: String?) {
      self.customToken = token
  }
}


public struct DashBoardDetails: Codable {
  
  public var totalAppointment: Double?
  public var completedAppointment: Double?
  public var cancelledAppointment: Double?
  public var totalLeaves: Double?
  
  public init(total: Double?, completed: Double?, cancelled: Double?, leave: Double?) {
    self.totalAppointment = total
    self.completedAppointment = completed
    self.cancelledAppointment = cancelled
    self.totalLeaves = leave
  }
  
  public enum CodingKeys: String, CodingKey {
    
    case totalAppointment
    case completedAppointment
    case cancelledAppointment
    case totalLeaves
  }

}

public struct ReviewDetails: Codable {
  
  public var _id: String?
  public var userId: String?
  public var rating: Double?
  public var text: String?
  public var createdAt: String?
  public var fullname: String?
  public var profilePic: String?
  public var thumbnail: String?
  public var thumbnaillow: String?
  
  public init(_id: String?, userId: String?, rating: Double?, text: String?, createdAt: String?, fullname: String?, profilePic: String?, thumbnail: String?, thumbnaillow: String?) {
    self._id = _id
    self.userId = userId
    self.rating = rating
    self.text = text
    self.createdAt = createdAt
    self.fullname = fullname
    self.profilePic = profilePic
    self.thumbnail = thumbnail
    self.thumbnaillow = thumbnaillow
  }
  
  public enum CodingKeys: String, CodingKey {
    
    case _id
    case userId = "user_id"
    case rating
    case text
    case createdAt = "created_at"
    case fullname = "user_full_name"
    case profilePic = "user_profile_pic"
    case thumbnail = "user_thumbnail_url"
    case thumbnaillow = "user_thumbnail_low_url"
    
  }

}



public struct AppointmentDetails: Codable {

    public var _id: String?
    public var appointmentStatus: String?
    public var duration: Double?
    public var date: String?
    public var startTime: String?
    public var endTime: String?
    public var bookedOn: String?
    public var bookedByType: String?
    public var bookedById: String?
    public var subAmount: Double?
    public var marginRate: Double?
    public var marginAmount: Double?
    public var baseAmount: Double?
    public var igstRate: Double?
    public var igstAmount: Double?
    public var totalAmount: Double?
    public var branch: BranchDataForAppointment?
    public var service: ServiceDataForAppointment?
    public var client: ClientDataForAppointment?
    public var vendor: VendorDataForAppointment?

    public init(_id: String?, appointmentStatus: String?, duration: Double?, date: String?, startTime: String?, endTime: String?, bookedOn: String?, bookedByType: String?, bookedById: String?, subAmount: Double?, marginRate: Double?, marginAmount: Double?, baseAmount: Double?, igstRate: Double?, igstAmount: Double?, totalAmount: Double?, branch: BranchDataForAppointment?, service: ServiceDataForAppointment?, client: ClientDataForAppointment?, vendor: VendorDataForAppointment?) {
        self._id = _id
        self.appointmentStatus = appointmentStatus
        self.duration = duration
        self.date = date
        self.startTime = startTime
        self.endTime = endTime
        self.bookedOn = bookedOn
        self.bookedByType = bookedByType
        self.bookedById = bookedById
        self.subAmount = subAmount
        self.marginRate = marginRate
        self.marginAmount = marginAmount
        self.baseAmount = baseAmount
        self.igstRate = igstRate
        self.igstAmount = igstAmount
        self.totalAmount = totalAmount
        self.branch = branch
        self.service = service
        self.client = client
        self.vendor = vendor
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case appointmentStatus = "appointment_status"
        case duration
        case date
        case startTime = "start_time"
        case endTime = "end_time"
        case bookedOn = "booked_on"
        case bookedByType = "booked_by_type"
        case bookedById = "booked_by_id"
        case subAmount = "sub_amount"
        case marginRate = "margin_rate"
        case marginAmount = "margin_amount"
        case baseAmount = "base_amount"
        case igstRate = "igst_rate"
        case igstAmount = "igst_amount"
        case totalAmount = "total_amount"
        case branch
        case service
        case client
        case vendor
    }


}

public struct BranchDataForAppointment: Codable {

    public var _id: String?
    public var location: Location?
    public var name: String?
    public var contactNumber: String?
    public var contactPersonName: String?
    public var coverPic: String?
    public var placeId: String?
    public var addressLine1: String?
    public var cityId: String?
    public var stateId: String?
    public var pincode: Double?
    public var stylistGender: String?
    public var serveGender: String?
    public var about: String?
    public var cityName: String?
    public var stateName: String?
    public var countryId: Double?
    public var countryCode: String?
    public var countryName: String?

    public init(_id: String?, location: Location?, name: String?, contactNumber: String?, contactPersonName: String?, coverPic: String?, placeId: String?, addressLine1: String?, cityId: String?, stateId: String?, pincode: Double?, stylistGender: String?, serveGender: String?, about: String?, cityName: String?, stateName: String?, countryId: Double?, countryCode: String?, countryName: String?) {
        self._id = _id
        self.location = location
        self.name = name
        self.contactNumber = contactNumber
        self.contactPersonName = contactPersonName
        self.coverPic = coverPic
        self.placeId = placeId
        self.addressLine1 = addressLine1
        self.cityId = cityId
        self.stateId = stateId
        self.pincode = pincode
        self.stylistGender = stylistGender
        self.serveGender = serveGender
        self.about = about
        self.cityName = cityName
        self.stateName = stateName
        self.countryId = countryId
        self.countryCode = countryCode
        self.countryName = countryName
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case location
        case name
        case contactNumber = "contact_number"
        case contactPersonName = "contact_person_name"
        case coverPic = "cover_pic"
        case placeId = "place_id"
        case addressLine1 = "address_line1"
        case cityId = "city_id"
        case stateId = "state_id"
        case pincode
        case stylistGender = "stylist_gender"
        case serveGender = "serve_gender"
        case about
        case cityName = "city_name"
        case stateName = "state_name"
        case countryId = "country_id"
        case countryCode = "country_code"
        case countryName = "country_name"
    }


}

public struct ServiceDataForAppointment: Codable {

    public var _id: String?
    public var name: String?
    public var duration: Double?

    public init(_id: String?, name: String?, duration: Double?) {
        self._id = _id
        self.name = name
        self.duration = duration
    }


}


public struct ClientDataForAppointment: Codable {

    public var _id: String?
    public var mobileNumber: String?
    public var email: String?
    public var fullName: String?
    /** YYYY-MM-DD format */
    public var birthdate: String?
    public var gender: String?

    public init(_id: String?, mobileNumber: String?, email: String?, fullName: String?, birthdate: String?, gender: String?) {
        self._id = _id
        self.mobileNumber = mobileNumber
        self.email = email
        self.fullName = fullName
        self.birthdate = birthdate
        self.gender = gender
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case mobileNumber = "mobile_number"
        case email
        case fullName = "full_name"
        case birthdate
        case gender
    }


}

public struct VendorDataForAppointment: Codable {

    public var _id: String?
    public var businessName: String?
    public var mobileNumber: String?
    public var email: String?

    public init(_id: String?, businessName: String?, mobileNumber: String?, email: String?) {
        self._id = _id
        self.businessName = businessName
        self.mobileNumber = mobileNumber
        self.email = email
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case businessName = "business_name"
        case mobileNumber = "mobile_number"
        case email
    }


}


public struct Stylist: Codable {

    public var stylist: StylistDetails?

    public init(stylist: StylistDetails?) {
        self.stylist = stylist
    }


}


public struct StylistDetails: Codable {

    public var _id: String?
    public var availability: WorkingHours?
    public var skill: [String]?
    public var fullName: String?
    public var info: String?
    public var profilePic: String?
    public var thumbnailUrl: String?
    public var thumbnailLowUrl: String?
    public var mobileNumber: String?
    public var email: String?
    public var gender: String?
    /** YYYY-MM-DD format */
    public var birthdate: String?
    public var totalExperienceYear: Double?
    public var totalExperienceMonth: Double?
    public var designation: String?
    public var vendorId: String?
    public var ratingCount: Double?
    public var ratingSum: Double?
    public var rating: Double?
    public var branch: BranchData?
    public var vendor: Vendor?
  

  public init(_id: String?, availability: WorkingHours?, skill: [String]?, fullName: String?, info: String?, profilePic: String?, thumbnailUrl: String?, thumbnailLowUrl: String?, mobileNumber: String?, email: String?, gender: String?, birthdate: String?, totalExperienceYear: Double?, totalExperienceMonth: Double?, designation: String?, vendorId: String?, ratingCount: Double?, ratingSum: Double?, rating: Double?, branch: BranchData?, vendor: Vendor?) {
        self._id = _id
        self.availability = availability
        self.skill = skill
        self.fullName = fullName
        self.info = info
        self.profilePic = profilePic
        self.thumbnailUrl = thumbnailUrl
        self.thumbnailLowUrl = thumbnailLowUrl
        self.mobileNumber = mobileNumber
        self.email = email
        self.gender = gender
        self.birthdate = birthdate
        self.totalExperienceYear = totalExperienceYear
        self.totalExperienceMonth = totalExperienceMonth
        self.designation = designation
        self.vendorId = vendorId
        self.ratingCount = ratingCount
        self.ratingSum = ratingSum
        self.rating = rating
        self.branch = branch
        self.vendor = vendor
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case availability
        case skill
        case fullName = "full_name"
        case info
        case profilePic = "profile_pic"
        case thumbnailUrl = "thumbnail_url"
        case thumbnailLowUrl = "thumbnail_low_url"
        case mobileNumber = "mobile_number"
        case email
        case gender
        case birthdate
        case totalExperienceYear = "total_experience_year"
        case totalExperienceMonth = "total_experience_month"
        case designation
        case vendorId = "vendor_id"
        case ratingCount = "rating_count"
        case ratingSum = "rating_sum"
        case rating
        case branch
        case vendor
    }


}


public struct WorkingHours: Codable {

    public var mon: WorkingHoursMon?
    public var tue: WorkingHoursMon?
    public var wed: WorkingHoursMon?
    public var thu: WorkingHoursMon?
    public var fri: WorkingHoursMon?
    public var sat: WorkingHoursMon?
    public var sun: WorkingHoursMon?

    public init(mon: WorkingHoursMon?, tue: WorkingHoursMon?, wed: WorkingHoursMon?, thu: WorkingHoursMon?, fri: WorkingHoursMon?, sat: WorkingHoursMon?, sun: WorkingHoursMon?) {
        self.mon = mon
        self.tue = tue
        self.wed = wed
        self.thu = thu
        self.fri = fri
        self.sat = sat
        self.sun = sun
    }

}

public struct WorkingHoursMon: Codable {

    public var startTime: String?
    public var endTime: String?
    public var _open: Bool?

    public init(startTime: String?, endTime: String?, _open: Bool?) {
        self.startTime = startTime
        self.endTime = endTime
        self._open = _open
    }

    public enum CodingKeys: String, CodingKey {
        case startTime = "start_time"
        case endTime = "end_time"
        case _open = "open"
    }


}


public struct BranchData: Codable {

    public var _id: String?
    public var location: Location?
    public var name: String?
    public var contactNumber: String?
    public var contactPersonName: String?
    public var placeId: String?
    public var addressLine1: String?
    public var cityId: String?
    public var stateId: String?
    public var pincode: Double?
    public var complimentary: [String]?
    public var links: Links?
    public var stylistGender: String?
    public var serveGender: String?
    public var about: String?
    public var cityName: String?
    public var stateName: String?
    public var countryId: Double?
    public var countryCode: String?
    public var countryName: String?

    public init(_id: String?, location: Location?, name: String?, contactNumber: String?, contactPersonName: String?, placeId: String?, addressLine1: String?, cityId: String?, stateId: String?, pincode: Double?, complimentary: [String]?, links: Links?, stylistGender: String?, serveGender: String?, about: String?, cityName: String?, stateName: String?, countryId: Double?, countryCode: String?, countryName: String?) {
        self._id = _id
        self.location = location
        self.name = name
        self.contactNumber = contactNumber
        self.contactPersonName = contactPersonName
        self.placeId = placeId
        self.addressLine1 = addressLine1
        self.cityId = cityId
        self.stateId = stateId
        self.pincode = pincode
        self.complimentary = complimentary
        self.links = links
        self.stylistGender = stylistGender
        self.serveGender = serveGender
        self.about = about
        self.cityName = cityName
        self.stateName = stateName
        self.countryId = countryId
        self.countryCode = countryCode
        self.countryName = countryName
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case location
        case name
        case contactNumber = "contact_number"
        case contactPersonName = "contact_person_name"
        case placeId = "place_id"
        case addressLine1 = "address_line1"
        case cityId = "city_id"
        case stateId = "state_id"
        case pincode
        case complimentary
        case links
        case stylistGender = "stylist_gender"
        case serveGender = "serve_gender"
        case about
        case cityName = "city_name"
        case stateName = "state_name"
        case countryId = "country_id"
        case countryCode = "country_code"
        case countryName = "country_name"
    }


}

public struct Vendor: Codable {
  
  public var email: String?
  
  public init(email: String?) {
    self.email = email
  }
}

public struct Links: Codable {

    public var website: String?
    public var instagram: String?
    public var twitter: String?
    public var youtube: String?
    public var facebook: String?
    public var medium: String?
    public var linkedin: String?

    public init(website: String?, instagram: String?, twitter: String?, youtube: String?, facebook: String?, medium: String?, linkedin: String?) {
        self.website = website
        self.instagram = instagram
        self.twitter = twitter
        self.youtube = youtube
        self.facebook = facebook
        self.medium = medium
        self.linkedin = linkedin
    }


}

public struct Portfolio: Codable {

    public var portfolio: [PortfolioInfo]?

    public init(portfolio: [PortfolioInfo]?) {
        self.portfolio = portfolio
    }


}


public struct PortfolioInfo: Codable {

    public var _id: String?
    public var url: String?
    public var thumbnailUrl: String?
    public var thumbnailLowUrl: String?
    public var type: String?
    public var sequence: Double?
    public var createdAt: String?

    public init(_id: String?, url: String?, thumbnailUrl: String?, thumbnailLowUrl: String?, type: String?, sequence: Double?, createdAt: String?) {
        self._id = _id
        self.url = url
        self.thumbnailUrl = thumbnailUrl
        self.thumbnailLowUrl = thumbnailLowUrl
        self.type = type
        self.sequence = sequence
        self.createdAt = createdAt
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case url
        case thumbnailUrl = "thumbnail_url"
        case thumbnailLowUrl = "thumbnail_low_url"
        case type
        case sequence
        case createdAt = "created_at"
    }


}


public struct HolidayList: Codable {

    public var _id: String?
    public var holidayName: String?
    public var branch: HolidayListBranch?
    public var vendorId: String?
    public var date: String?
    public var createdAt: String?
    public var updatedAt: String?

    public init(_id: String?, holidayName: String?, branch: HolidayListBranch?, vendorId: String?, date: String?, createdAt: String?, updatedAt: String?) {
        self._id = _id
        self.holidayName = holidayName
        self.branch = branch
        self.vendorId = vendorId
        self.date = date
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case holidayName = "holiday_name"
        case branch
        case vendorId = "vendor_id"
        case date
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }


}


public struct HolidayListBranch: Codable {

    public var _id: String?
    public var name: String?
    public var media: [PortfolioInfo]?
  
    public init(_id: String?, name: String?, media: [PortfolioInfo]?) {
        self._id = _id
        self.name = name
        self.media = media
    }


}



public struct LeaveList: Codable {

    public var _id: String?
    /** valid values are \&quot;UNAPPROVED\&quot;, \&quot;APPROVED\&quot;, \&quot;CANCELLED\&quot;, \&quot;REJECTED\&quot;. */
    public var leaveStatus: String?
    public var reason: String?
    public var startTime: String?
    public var endTime: String?
    public var createdAt: String?
    public var updatedAt: String?
    public var branch: LeaveListBranch?

    public init(_id: String?, leaveStatus: String?, reason: String?, startTime: String?, endTime: String?, createdAt: String?, updatedAt: String?, branch: LeaveListBranch?) {
        self._id = _id
        self.leaveStatus = leaveStatus
        self.reason = reason
        self.startTime = startTime
        self.endTime = endTime
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.branch = branch
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case leaveStatus = "leave_status"
        case reason
        case startTime = "start_time"
        case endTime = "end_time"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case branch
    }


}


public struct LeaveListBranch: Codable {

    public var _id: String?
    public var name: String?
    public var location: Location?

    public init(_id: String?, name: String?, location: Location?) {
        self._id = _id
        self.name = name
        self.location = location
    }


}


public struct Location: Codable {

    public var type: String?
    public var coordinates: [Double]?

    public init(type: String?, coordinates: [Double]?) {
        self.type = type
        self.coordinates = coordinates
    }


}

public struct AddEditLeave: Codable {

  var data: LeaveList?
  var branchName: String?
  
  public init(data: LeaveList?, branchName: String?) {
    
    self.data = data
    self.branchName = branchName
  }
  
  public enum CodingKeys: String, CodingKey {
    
    case data
    case branchName = "branch_name"
  }
}

struct NotificationDataInformation: Codable {
  var notifications: [NotificationData]?
}

struct NotificationData: Codable {
  
  var _id: String?
  var notification_type: Int?
  var created_at: String?
  var to_user: NotificationFromuser?
  var appointment: NotificationAppointment?
  var order_id: String?
  var total_amount:Double?
  var seen_on: String?
  //var order: OrderDetails
}

struct NotificationFromuser: Codable {

  var _id: String?
  var full_name: String?
  var is_following: Bool?
}

struct NotificationAppointment: Codable {

  var _id: String?
  var appointment_id: String?
}

struct NotificationCount: Codable {
    var notification_count: Int?
}

//struct OrderDetails: Codable {
//
//  var _id: String?
//  var items: [OrderItem]?
//  var total_amount: Double?
//  var payment_attempt: Int?
//  var payment_method: String?
//  var payment_status: String?
//  var order_status: String?
//  var gstin: String?
//  var created_at: String?
//  var created_by_id: String?
//  var created_by_type: String?
//  var fee: Double?
//  var tax: Double?
//  var gateway_transaction_id: String?
//}


public struct Code: Codable {
  
  public var code: String?
  
  public init(code: String?) {
    self.code = code
  }
}



struct UploadDocument {
  var title: String?
  var img: UIImage?
  var data: Data?
  var name: String?
  var fileName: String?
  
}

extension AppCodable {
  func getJSONData() -> Any? {
    
    guard let bodyData = try? JSONEncoder().encode(self) else {
      return nil
    }
    
    return try? JSONSerialization.jsonObject(with: bodyData, options: [.allowFragments])
  }
}
