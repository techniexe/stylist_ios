//
//  LeaveHeaderCell.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 24/02/21.
//

import UIKit

class LeaveHeaderCell: UITableViewCell {
  
  // MARK:- @IBOutlet
  @IBOutlet weak var txtDate: UITextField!
  @IBOutlet weak var btnDateChage: UIButton!
  @IBOutlet weak var btnClearAll: UIButton!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}
