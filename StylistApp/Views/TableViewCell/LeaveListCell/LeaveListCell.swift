//
//  LeaveListCell.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 16/02/21.
//

import UIKit

class LeaveListCell: UITableViewCell {
  
  // MARK:- @IBOutlets
  @IBOutlet weak var imgView: UIImageView!
  @IBOutlet weak var lblSalonName: UILabel!
  @IBOutlet weak var lblApplyOn: UILabel!
  @IBOutlet weak var btnStatus: CustomButton!
  @IBOutlet weak var lblDate: UILabel!
  @IBOutlet weak var lblDescription: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK:- Configure Cell
  func configureCell(_ dicLeaveInfo: LeaveList) {
    
    if let strLeaveStatus = dicLeaveInfo.leaveStatus, strLeaveStatus != "" {
      
      if strLeaveStatus == "APPROVED" {
        self.btnStatus.backgroundColor = UIColor.init(hexString: "#00A65A")
      }else if strLeaveStatus == "UNAPPROVED" {
        self.btnStatus.backgroundColor = Constants.Colors.colorPrimary!
      }else {
        self.btnStatus.backgroundColor = UIColor.init(hexString: "#FA4248")
      }
      
      self.btnStatus.setTitle(strLeaveStatus, for: .normal)
      self.btnStatus.isHidden = false
      
    }else {
      self.btnStatus.isHidden = true
    }
    
//    if let branch = dicLeaveInfo.branch {
//
//      if let name = branch.name, name != "" {
//        self.lblSalonName.text = name
//      }else {
//        self.lblSalonName.text = ""
//      }
//
//    }else {
//      self.lblSalonName.text = ""
//    }
    
    if let createdAt = dicLeaveInfo.createdAt, createdAt != "" {
      
      self.lblApplyOn.text = "Apply On : \(Date.convertDateUTCToLocal(strDate: createdAt , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
    }else {
      self.lblApplyOn.text = ""
    }
    
    var strStartTime: String = ""
    var strEndTime: String = ""
   
    if let startTime = dicLeaveInfo.startTime, startTime != "" {
      strStartTime = "\(Date.convertDateUTCToLocal(strDate: startTime , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
    }
    
    if let endTime = dicLeaveInfo.endTime, endTime != "" {
      strEndTime = "\(Date.convertDateUTCToLocal(strDate: endTime , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
    }
    
    var strFinal: NSString!
   
    strFinal = "Start Date : \(strStartTime)  |  End Date : \(strEndTime)" as NSString
    
    let strMutableAttributed =  NSMutableAttributedString(string:strFinal as String)
    //Start Date : 10th Jan, 2021 | End Date : 12th Jan, 2021
    
    let font = MainFont.regular.with(size: 11.0)
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "#C0C0C0")], range: strFinal.range(of: "Start Date : "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black], range: strFinal.range(of: "\(strStartTime)"))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: Constants.Colors.colorPrimary!], range: strFinal.range(of: "  |  "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "#C0C0C0")], range: strFinal.range(of: "End Date : "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black], range: strFinal.range(of: "\(strEndTime)"))
    
    
    lblDate.attributedText = strMutableAttributed
    
    if let reason = dicLeaveInfo.reason, reason != "" {
      self.lblDescription.text = reason
      //self.lblDescription.textAlignment = .justified
    }else {
      self.lblDescription.text = ""
    }
    
  }
  
}
