//
//  NotificationCell.swift
//  Saloon
//
//  Created by Paras Modi on 27/02/19.
//  Copyright © 2019 Techniexe Infolabs. All rights reserved.
//

import UIKit
import FRHyperLabel

protocol NotificationCellDelegate: AnyObject {
    func didPressOnUserName(_ strUserName: String?, tag: Int)
}

class NotificationCell: UITableViewCell {
  
  // MARK: - @IBOutlet
  @IBOutlet var imgProfilePic: UIImageView!
  @IBOutlet var lblStatus: UILabel!
  @IBOutlet var lblMessage: FRHyperLabel!
  @IBOutlet var lblCreatedTime: UILabel!
  
  // MARK: - @vars
  weak var delegate: NotificationCellDelegate?
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func configureCell(_ dicNotification: NotificationData) {
    
    //self.imgProfilePic.setImage(url: "https://s3.ap-south-1.amazonaws.com/tec-salon/d/profile/5bd2a05795a4fa45d1163edf.jpg?i=1550134681337", style: .rounded, completion: nil)
    
    /*let aStrMainString = "Risk - free Cancellable cash voucher at upto 70% OFF!" as NSString
    
    let aStrAttributed =    NSMutableAttributedString(string:aStrMainString as String)
    aStrAttributed.addAttributes([NSAttributedString.Key.foregroundColor: Constants.Colors.blackColor], range: aStrMainString.range(of: "Risk"))
    self.lblMessage.attributedText = aStrAttributed
    
    let font = UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.medium)
    self.lblMessage.linkAttributeDefault = [NSAttributedString.Key.font : font]
    self.lblMessage.linkAttributeHighlight = [NSAttributedString.Key.font : font]
    
    let strName: String!
    strName = "Risk"
    self.lblMessage.setLinksForSubstrings([strName!], withLinkHandler: { (label, substring) in
      self.delegate?.didPressOnUserName(substring!, tag:  self.lblMessage.tag)
    })
    
    let createAtDate = Date.convertStringToDate(strDate: "2020-04-01T18:30:00.000Z", dateFormate: Constants.DateFormats.ISO)
    self.lblCreatedTime.text = Date.getElapsedInterval(createAtDate)*/
    
    var message: NSString = ""
    var strFullName: String = ""
    var strAppointmentID: String = ""
    //var strTotalAmount: String = ""
    var strOrderID: String = ""
    
    if let notificationType = dicNotification.notification_type {
      
      switch notificationType {
      case StylistNotificationType.appointmentReschedule.rawValue:
        //self.lblStatus.text = "BOOK APPOINTMENT"
        
        strFullName = getFullName(dicNotification)
        strAppointmentID = getAppointmentID(dicNotification)
        message = "Dear \(strFullName), One appointment with id \(strAppointmentID) is rescheduled." as NSString
        
      
      case StylistNotificationType.appointmentSchedule.rawValue:
        //self.lblStatus.text = "PAYMENT CONFIRMATION"
        
        strFullName = getFullName(dicNotification)
        //let strTotalAmount = getTotalAmount(dicNotification)
        strOrderID = getOrderID(dicNotification)
        message = "Dear \(strFullName), One appointment with id \(strAppointmentID) is scheduled." as NSString
        
      case StylistNotificationType.cancelAppointmentSchedule.rawValue:
        //self.lblStatus.text = "APPOINTMENT APPROVED"
        strFullName = getFullName(dicNotification)
        strAppointmentID = getAppointmentID(dicNotification)
        message = "Dear \(strFullName), One scheduled appointment with id \(strAppointmentID) is cancel." as NSString

        
     
      case StylistNotificationType.appointmentComplete.rawValue:
        //self.lblStatus.text = "CANCEL ORDER"
        strFullName = getFullName(dicNotification)
        strOrderID = getOrderID(dicNotification)
        //message = "Dear \(strFullName), Your order with id \(strOrderID) is cancel." as NSString
        
        message = "Dear \(strFullName), One appointment with id \(strAppointmentID) is completed." as NSString
    
        
      case StylistNotificationType.appointmentOngoing.rawValue:
        //self.lblStatus.text = "REMINDER BEFORE SEVEN DAY"
        strFullName = getFullName(dicNotification)
        message = "Dear \(strFullName), One appointment with id \(strAppointmentID) is ongoing"  as NSString
        
      case StylistNotificationType.addReview.rawValue:
        //self.lblStatus.text = "REMINDER BEFORE ONE DAY"
        strFullName = getFullName(dicNotification)
        message = "Dear \(strFullName), One review added."  as NSString
      default:
        break
      }
      
    }
    
    
    let aStrAttributed =    NSMutableAttributedString(string:message as String)
    aStrAttributed.addAttributes([NSAttributedString.Key.foregroundColor: Constants.Colors.colorPrimary!], range: message.range(of: "\(strFullName)"))
    self.lblMessage.attributedText = aStrAttributed
    
    let font = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.medium)
    self.lblMessage.linkAttributeDefault = [NSAttributedString.Key.font : font]
    self.lblMessage.linkAttributeHighlight = [NSAttributedString.Key.font : font]
    
    let strName: String!
    strName = "\(strFullName)"
    self.lblMessage.setLinksForSubstrings([strName!], withLinkHandler: { (label, substring) in
      self.delegate?.didPressOnUserName(substring!, tag:  self.lblMessage.tag)
    })
    
    if let strCreatedDate = dicNotification.created_at {
      let objDate = Date.convertStringToDate(strDate: strCreatedDate, dateFormate: DateFormats.iso.rawValue)
      self.lblCreatedTime.text = Date.getElapsedInterval(objDate)
    }else {
      self.lblCreatedTime.text = ""
    }
    
  }
  
  //MARK:- GetFullName
  func getFullName(_ dicNotification: NotificationData) -> String {
    
    var strFullName: String = ""
    
    if let touser = dicNotification.to_user {
      
      if let fullName = touser.full_name {
        strFullName = fullName
      }
      
    }
    
    return strFullName
    
  }
  
  //MARK:- Get AppointmentID
  func getAppointmentID(_ dicNotification: NotificationData) -> String {
    
    var strAppointmentID: String = ""
    
    if let appointment = dicNotification.appointment {
      
      if let appointmentID = appointment.appointment_id {
        strAppointmentID = "#\(appointmentID)"
      }
      
    }
    
    
    return strAppointmentID
    
  }
  
  //MARK:- Get Total Amount
  func getTotalAmount(_ dicNotification: NotificationData) -> String {
    
    var strTotalAmount: String = ""
    
    if let totalAmount = dicNotification.total_amount {
      strTotalAmount = Helper.getPrice(totalAmount)
    }
    
    return strTotalAmount
    
  }
  
  //MARK:- Get Total Amount
  func getOrderID(_ dicNotification: NotificationData) -> String {
    
    var strOrderID: String = ""
    
    if let orderID = dicNotification.order_id {
      strOrderID = "#\(orderID)"
    }
    
    return strOrderID
    
  }
  
}
