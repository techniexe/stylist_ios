//
//  NotificationListCell.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 09/02/21.
//

import UIKit

class NotificationListCell: UITableViewCell {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var lblTime: UILabel!
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var lblDescription: UILabel!
  @IBOutlet weak var customimgView: CustomImageView!
  @IBOutlet weak var imgView: UIImageView!
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
