//
//  HolidayListCell.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 16/02/21.
//

import UIKit

class HolidayListCell: UITableViewCell {
  
  // MARK:- @IBOutlets
  @IBOutlet weak var imgView: UIImageView!
  @IBOutlet weak var lblSalonName: UILabel!
  @IBOutlet weak var lblDate: UILabel!
  @IBOutlet weak var lblDescription: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
 
  // MARK:- Configure Cell
  func configureCell(_ dicHolidayInfo: HolidayList) {
    
    if let branch = dicHolidayInfo.branch {
      
      if let name = branch.name, name != "" {
        self.lblSalonName.text = name
      }else {
        self.lblSalonName.text = ""
      }
      
      if let arrMedia = branch.media {
        
        if arrMedia.count > 0 {
          
          if let thumbnail = arrMedia[0].url {
            self.imgView.setImage(url: thumbnail, style: .squared, completion: nil)
          }
        }
      }
      
    }else {
      self.lblSalonName.text = ""
    }
    
    if let date = dicHolidayInfo.date, date != "" {
    
      let strDate = "\(Date.convertDateUTCToLocal(strDate: date , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
      
      var strFinal: NSString!
     
      strFinal = "Date : \(strDate)" as NSString
      
      let strMutableAttributed =  NSMutableAttributedString(string:strFinal as String)
      
      strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: 12.0), NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "#C0C0C0")], range: strFinal.range(of: "Date : "))
      
      strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: 12.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strFinal.range(of: "\(strDate)"))
      
      lblDate.attributedText = strMutableAttributed
    }else {
      lblDate.text = ""
    }
    
    
    if let holidayName = dicHolidayInfo.holidayName, holidayName != "" {
    
      var strFinal: NSString!
     
      strFinal = "Holiday Name : \(holidayName)" as NSString
      
      let strMutableAttributed =  NSMutableAttributedString(string:strFinal as String)
      
      strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: 12.0), NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "#C0C0C0")], range: strFinal.range(of: "Holiday Name : "))
      
      strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: 12.0), NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "#FA4248")], range: strFinal.range(of: "\(holidayName)"))
      
      lblDescription.attributedText = strMutableAttributed
      
    }else {
      lblDescription.text = ""
    }
    
  }
}
