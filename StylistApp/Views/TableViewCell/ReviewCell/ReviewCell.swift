//
//  ProductReviewCell.swift
//  ConmateBuyerApp
//
//  Created by Techniexe Infolabs on 27/08/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var imgView: CustomImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblReviewText: UILabel!
  @IBOutlet weak var lblCreatedDate: UILabel!
  @IBOutlet weak var ratingView: CosmosView!
  @IBOutlet weak var bottomLine: UIView!
  @IBOutlet var layoutconstraint: [NSLayoutConstraint]!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK: - configureCell
  func configureCell(_ reviewDetails: ReviewDetails) {

    if let fullname = reviewDetails.fullname {
      self.lblName.setText(fullname)
    }else {
      self.lblName.text = ""
    }
    
    if let reviewText = reviewDetails.text {
       self.lblReviewText.setText(reviewText)
    }else {
      self.lblReviewText.setText("")
    }
    
    if let createdAt = reviewDetails.createdAt {
      let createAtDate = Date.convertStringToDate(strDate: createdAt, dateFormate: DateFormats.iso.format())
      self.lblCreatedDate.setText(Date.getElapsedInterval(createAtDate))
    }else {
      self.lblCreatedDate.setText("")
    }
    
    if let rating = reviewDetails.rating {
      
      if rating == 0.0 {
        //self.ratingView.isHidden = true
        ratingView.rating = 0
      }else {
        self.ratingView.isHidden = false
        ratingView.rating = rating
      }
      
    }
    
    if let thumbnail = reviewDetails.profilePic {
      self.imgView.setImage(url: thumbnail, style: .rounded, completion: nil)
    }
    
  }
}
