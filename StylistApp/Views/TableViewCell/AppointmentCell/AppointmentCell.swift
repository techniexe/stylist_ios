//
//  AppointmentCell.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 25/02/21.
//

import UIKit

class AppointmentCell: UITableViewCell {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblClientName: UILabel!
  @IBOutlet weak var lblDate: UILabel!
  @IBOutlet weak var btnTime: UIButton!
  @IBOutlet weak var bottomLine: UIView!
  @IBOutlet weak var lineHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var lblAppntStatus: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK:- Configure Cell
  func configureCell(_ dicAppointment: AppointmentDetails) {
    
    if let service = dicAppointment.service {
      
      if let serviceName = service.name,  serviceName != ""  {
        self.lblName.text = serviceName
      }else {
        self.lblName.text = ""
      }
    }else {
      self.lblName.text = ""
    }
    
  
    if let client = dicAppointment.client {
      
      if let name = client.fullName, name != "" {
        self.lblClientName.text = name
      }else {
        self.lblClientName.text = ""
      }
      
    }else {
      self.lblClientName.text = ""
    }
    
   
    if let time = dicAppointment.startTime {
      
      //self.lblDate.text = "\(Date.convertLocalToUTC(strDate: time, oldFormate: DateFormats.iso.rawValue, newFormate: DateFormats.birth.rawValue))"
      
      self.lblDate.text = "\(Date.convertDateUTCToLocal(strDate: time , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
      let convertedTime = "\(Date.convertDateUTCToLocal(strDate: time , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.startTime.format()))"
      self.btnTime.isHidden = false
      self.btnTime.setTitle(convertedTime, for: .normal)
      
    }else {
      self.lblDate.text = ""
      self.btnTime.isHidden = true
    }
    
   
    if let status = dicAppointment.appointmentStatus {
     
      switch status {
      
      case Constants.AppointmentStatus.UNAPPROVED:
        self.lblAppntStatus.text = Constants.AppointmentStatusText.UNAPPROVED
        self.lblAppntStatus.textColor = UIColor.init(hexString: "FF8C00")
       
      case Constants.AppointmentStatus.CANCELLED:
        
        self.lblAppntStatus.text = Constants.AppointmentStatusText.CANCELLED
        self.lblAppntStatus.textColor = UIColor.init(hexString: "FF2828")
       
      case Constants.AppointmentStatus.MISSED:
        self.lblAppntStatus.text = Constants.AppointmentStatusText.MISSED
        self.lblAppntStatus.textColor = UIColor.init(hexString: "FF2828")
        
      case Constants.AppointmentStatus.APPROVED:
        self.lblAppntStatus.text = Constants.AppointmentStatusText.APPROVED
        self.lblAppntStatus.textColor = UIColor.init(hexString: "05C270")
        
      case Constants.AppointmentStatus.COMPLETED:
        self.lblAppntStatus.text = Constants.AppointmentStatusText.COMPLETED
        self.lblAppntStatus.textColor = UIColor.init(hexString: "05C270")
       
      case Constants.AppointmentStatus.WAITINGPAYMENT:
        self.lblAppntStatus.text = Constants.AppointmentStatusText.WAITINGPAYMENT
        self.lblAppntStatus.textColor = UIColor.init(hexString: "FF2828")
       
      case Constants.AppointmentStatus.ONGOING:
        self.lblAppntStatus.text = Constants.AppointmentStatusText.ONGOING
        self.lblAppntStatus.textColor = UIColor.init(hexString: "FF8C00")
       
      case Constants.AppointmentStatus.NOSHOW:
        self.lblAppntStatus.text = Constants.AppointmentStatusText.NOSHOW
        self.lblAppntStatus.textColor = UIColor.init(hexString: "FF2828")
      
      default:
        self.lblAppntStatus.isHidden = true
      }
    }
    
  }

}
