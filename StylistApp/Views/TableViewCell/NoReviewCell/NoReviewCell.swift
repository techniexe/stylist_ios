//
//  NoReviewCell.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 10/02/21.
//

import UIKit

class NoReviewCell: UITableViewCell {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var mainView: UIView!
  @IBOutlet weak var imgView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}
