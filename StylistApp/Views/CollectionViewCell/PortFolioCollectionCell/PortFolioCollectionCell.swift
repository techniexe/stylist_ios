//
//  PortFolioCollectionCell.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 15/02/21.
//

import UIKit

class PortFolioCollectionCell: UICollectionViewCell {
  
  // MARK: - @@IBOutlets
  @IBOutlet weak var imgView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  // MARK: - configureCell
  func configureCell(_ infoDetails: PortfolioInfo) {
    
    if let url = infoDetails.url {
      self.imgView.setImage(url: url, style: .squared, completion: nil)
    }
    
  }
}
