//
//  AddPortFolioCollectionCell.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 23/02/21.
//

import UIKit

class AddPortFolioCollectionCell: UICollectionViewCell {
  
  // MARK:- @IBOutlets
  @IBOutlet weak var btnRemove: UIButton!
  @IBOutlet weak var imgView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
}
