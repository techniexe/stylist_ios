//
//  CustomTableFooterView.swift
//  Trail
//
//  Created by Techniexe Infolabs on 07/06/19.
//  Copyright © 2019 Paras Modi. All rights reserved.
//

import UIKit

class CustomTableFooterView: UIView {

  @IBOutlet weak var indicatorView: UIActivityIndicatorView!
  var isAnimatingFinal:Bool = false
  var currentTransform:CGAffineTransform?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.prepareInitialAnimation()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  func setTransform(inTransform:CGAffineTransform, scaleFactor:CGFloat) {
    if isAnimatingFinal {
      return
    }
    self.currentTransform = inTransform
    self.indicatorView.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
    // self.activityIndicator.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
  }
  
  //reset the animation
  func prepareInitialAnimation() {
    self.isAnimatingFinal = false
    /*self.activityIndicator.type = NVActivityIndicatorType.ballClipRotate
     self.activityIndicator.color = Constants.Colors.themeColor
     self.activityIndicator.stopAnimating()
     self.activityIndicator.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)*/
    self.indicatorView.startAnimating()
    self.indicatorView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
  }
  
  func startAnimate() {
    self.isAnimatingFinal = true
    self.indicatorView.startAnimating()
    // self.activityIndicator.startAnimating()
  }
  
  func stopAnimate() {
    self.isAnimatingFinal = false
    //self.activityIndicator.stopAnimating()
    self.indicatorView.stopAnimating()
  }
  
  //final animation to display loading
  func animateFinal() {
    if isAnimatingFinal {
      return
    }
    self.isAnimatingFinal = true
    /*UIView.animate(withDuration: 0.0) {
     self.activityIndicator.transform = CGAffineTransform.identity
     }*/
    UIView.animate(withDuration: 0.0) {
      self.indicatorView.transform = CGAffineTransform.identity
    }
  }
  
  

}
