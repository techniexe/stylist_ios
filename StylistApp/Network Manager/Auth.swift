

import Foundation
import Alamofire
//import FirebaseMessaging

extension APIHandler {
  
  /*********************** Stylist Authentication ***********************/
  
  // MARK: - Login With Mobile
  func loginWithMobile(_ strPath: String, loginType: LogInType, bodyParams: [String: Any]?, queryParams: [String: Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.getAuthCode
      
      let pathData = [Constants.Placeholders.mobileNumber : strPath]
      
      let url = getURL(path: path, pathVariables: pathData, queryParams: queryParams)
      
      getStatusCode(url: url, method: .get, bodyData: bodyParams, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - Veify Mobile Number to Get Registration Token
  func verifyMobileForGetToken(_ strMobile: String, strCode: String, completion: @escaping(Int?, AppData<CustomToken>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.verifyAuthCode
      
      let pathData = [Constants.Placeholders.mobileNumber : strMobile, Constants.Placeholders.code: strCode]
      
      let url = getURL(path: path, pathVariables: pathData, queryParams: nil)
      
      getResponse(method: .post, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  /*********************** DashBoard Details ***********************/
  
  // MARK: - Get DashBoard Details
  func getDashBoardDetails(completion: @escaping(Int?, AppData<DashBoardDetails>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.dashBoardDetails
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
  
  /*********************** Review List ***********************/
  
  // MARK: - Get Review List
  func getReviewList(queryParams: [String: Any]?, completion: @escaping(Int?, AppData<[ReviewDetails]>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistFeedback
      
      let url = getURL(path: path, pathVariables: nil, queryParams: queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
  
  /*********************** Stylist Appointment ***********************/
  
  // MARK: - Get Appointment List
  func getAppointmentList(queryParams: [String: Any]?, completion: @escaping(Int?, AppData<[AppointmentDetails]>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistAppointment
      
      let url = getURL(path: path, pathVariables: nil, queryParams: queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
  /*********************** Stylist Details ***********************/
  
  // MARK: - Get Stylist Details
  func getStylistDetails(completion: @escaping(Int?, AppData<Stylist>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistDetails
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
  // MARK: - Update Stylist Details
  func updateStylistDetails(bodyParams: [String:Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistDetails
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      getStatusCode(url: url, method: .patch, bodyData: bodyParams, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - Add Profile Picture
  func addPortFolio(_ arrUploadDoc: [UploadDocument], completion: @escaping(Int?, String?) -> Void) {
   
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistMedia
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      uploadImageAsMultipartformData(url: url, method: .patch, bodyData: nil, arrUploadDoc: arrUploadDoc, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  
  // MARK: - Add Profile Picture
  func addProfilePic(_ arrUploadDoc: [UploadDocument], completion: @escaping(Int?, String?) -> Void) {
   
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistMedia
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      uploadProfilePicture(url: url, method: .patch, arrUploadDoc: arrUploadDoc, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - Delete Stylist PortFolio
  func deleteStylistPortFolio(bodyParams: [String : Any]?, completion: @escaping(Int?, String?) -> Void) {
   
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistMedia
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      getStatusCode(url: url, method: .delete, bodyData: bodyParams, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - Get Stylist PortFolio
  func getStylistPortfolio(queryParams: [String: Any]?, completion: @escaping(Int?, Portfolio?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistPortfolio
      
      let url = getURL(path: path, pathVariables: nil, queryParams: queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  /*********************** Stylist Holiday ***********************/
  
  // MARK: - Holiday List
  func getHolidayList(queryParams: [String: Any]?, completion: @escaping(Int?, AppData<[HolidayList]>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistHoliday
      
      let url = getURL(path: path, pathVariables: nil, queryParams: queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
  
  /*********************** Stylist Leave ***********************/
  
  // MARK: - Add Leave
  func addLeave(bodyParams: [String: Any]?, completion: @escaping(Int?, AddEditLeave?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistLeave
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      //getStatusCode(url: url, method: .post, bodyData: bodyParams, completion: completion)
      getResponse(method: .post, url: url, bodyData: bodyParams, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  
  // MARK: - Get Stylist Leave List
  func getStylistLeaveList(queryParams: [String: Any]?, completion: @escaping(Int?, AppData<[LeaveList]>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistLeave
      
      let url = getURL(path: path, pathVariables: nil, queryParams: queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
  // MARK: - Edit Stylist Leave
  func editLeave(strLeaveID: String, bodyParams: [String: Any]?, completion: @escaping(Int?, AddEditLeave?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistEditLeave
      
      let pathData: [String: Any]? = [Constants.Placeholders.leaveId : strLeaveID]
      
      let url = getURL(path: path, pathVariables: pathData, queryParams: nil)
      
      getResponse(method: .patch, url: url, bodyData: bodyParams, completion: completion)
      //getStatusCode(url: url, method: .patch, bodyData: bodyParams, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - Cancel Leave By Stylist
  func cancelLeave(strLeaveID: String, bodyParams: [String: Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.stylistEditLeave
      
      let pathData: [String: Any]? = [Constants.Placeholders.leaveId : strLeaveID]
      
      let url = getURL(path: path, pathVariables: pathData, queryParams: nil)
      
      getStatusCode(url: url, method: .delete, bodyData: bodyParams, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - Request OTP
  func requestOTP(completion: @escaping(Int?, Code?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
     
      let path = Constants.APIPaths.requestOTP
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
 
  
  /*********************** Stylist Notification ***********************/
  
  // MARK: - Get Stylist Notification
  func getStylistNotifications(queryParams: [String:Any]?, completion: @escaping(Int?, AppData<NotificationDataInformation>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.getNotifications
      
      let url = getURL(path: path, pathVariables: nil, queryParams: queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
 
  // MARK: - Get Stylist Notification Count
  func getStylistNotificationCount(completion: @escaping(Int?, AppData<NotificationCount>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.getNotificationCount
      
      let url = getURL(path: path, pathVariables: nil, queryParams: nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
}


