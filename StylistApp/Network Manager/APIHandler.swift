
import UIKit
import Foundation
import Alamofire

final class APIHandler {
  
  var sessionToken: String?
  
  static let shared = APIHandler()
  
  // MARK: - Get BaseURL
  func getBaseURL() -> String {
    
    #if DEBUG
    return Constants.APIConstants.baseDevAPIURL
    #else
    return Constants.APIConstants.baseProdAPIURL
    #endif
    
  }
  
  // MARK: - Get URL of Any Data Request
  func getURL(path: String, pathVariables: [String:Any]?, queryParams: [String:Any]?) -> URL {
    
    var resultingPath = self.getBaseURL() + path
    
    if let pathVariables = pathVariables {
      for (key, value) in pathVariables {
        resultingPath = resultingPath.replacingOccurrences(of: key, with: "\(value)")
      }
    }
    
    let encodedString = resultingPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
    
    var urlComponents = URLComponents(string: encodedString!)
    
    if let queryParams = queryParams {
      urlComponents?.queryItems = []
      for (key, value) in queryParams {
        urlComponents?.queryItems?.append(URLQueryItem(name: key, value: "\(value)"))
      }
    }
    Helper.DLog(message: "URL: \((urlComponents?.url)!)")
    return urlComponents!.url!
  }
  
  // MARK: - Refresh Session Token When it get expired
  func refreshSessionToken(completion: @escaping (String?, Error?) -> Void) {
    
    let url = getURL(path:Constants.APIPaths.sessions, pathVariables: nil, queryParams: nil)
    
    AF.request(url, method: .post, parameters: AppSessionData().getJSONData() as? Parameters, encoding: JSONEncoding.default, headers: nil)
      .validate()
      .response() { response in
        guard
          let data = response.data,
          let tokenReponse = try? JSONDecoder().decode(AppData<SessionToken>.self, from: data) else {
            completion(nil, response.error)
            return
        }
        
        self.sessionToken = tokenReponse.data.token
        completion(tokenReponse.data.token, nil)
    }
  }
  
  
  // MARK: - Get Token
  func getToken(_ withAuthHeader : Bool, completion: @escaping (String?, Error?) -> Void) {
    
    guard withAuthHeader == true else {
      if Helper.isKeyPresentInUserDefaults(key: Constants.UserDefaults.customToken) == true {
        completion((UserDefaults.standard.value(forKey:Constants.UserDefaults.customToken) as! String), nil)
      }else {
        refreshSessionToken(completion: completion)
      }
      
      return
    }
    
    refreshSessionToken(completion: completion)
  }
  
  
  // MARK: - Data Request which require token as Header
  func getDataRequest(url: URL, method: HTTPMethod, bodyData: [String:Any]?, completion: @escaping(DataRequest?, Error?) -> Void) {
    
    getToken(url.absoluteString.contains("auth")) { token, error in
      guard let token = token, error == nil else {
        completion(nil, error)
        return
      }
      
      Helper.DLog(message: "token : \(token)")
    
      let headers: HTTPHeaders =  ["Authorization": "Bearer \(token)"]
      
      let dataRequest = AF.request(url, method: method, parameters: bodyData, encoding: JSONEncoding.default, headers: headers)
      
      dataRequest.response() { response in
        
        if response.response != nil {
          
          if response.response?.statusCode != nil {
            
            if (response.response?.statusCode)! == 401 {
              GlobalFunction.logOut()
              Helper.showToast(_strMessage: ErrorMessage.sessionexpire.rawValue)
              return
            }else {
              completion(dataRequest, nil)
            }
          }else {
            completion(dataRequest,response.error)
          }
        }else {
          completion(dataRequest,response.error)
        }
        
      }
      
    }
  }
  
  // MARK: - Get Response of Any Data Request
  func getResponse <T: Codable> (method: HTTPMethod, url: URL, bodyData: [String:Any]?, completion: @escaping (Int?, T?, String?) -> Void) {
    
    if let params = bodyData {
      Helper.DLog(message: "bodyParmas: \(params)")
    }
    
    Helper.DLog(message: "mehod: \(method)\nurl: \(url)")
    
    getDataRequest(url: url, method: method, bodyData: bodyData) { dataRequest, error in
      guard let dataRequest = dataRequest, error == nil else {
        completion(nil, nil, error?.localizedDescription)
        return
      }
      
      dataRequest
        .response() { response in
          guard let data = response.data, response.error == nil else {
            completion(response.response?.statusCode ,nil, response.error?.localizedDescription)
            return
          }
          do {
            //let jsonResponse = try JSONSerialization.jsonObject(with:data, options: [])
            Helper.DLog(message: "\nSTATUS CODE::: \(String(describing: response.response?.statusCode))\nURL::: \(String(describing: response.response?.url))\nJSON RESPONSE: \(String(data: data, encoding: .utf8) ?? "")")
            let decodedData = try JSONDecoder().decode(T.self, from: data)
            completion(response.response?.statusCode, decodedData, nil)
            
          } catch {
            
            Helper.DLog(message: "\nSTATUS CODE::: \(String(describing: response.response?.statusCode))\nURL::: \(String(describing: response.response?.url))\nJSON FAILED: \(String(data: data, encoding: .utf8) ?? "")\nerror: \(error.localizedDescription)")
            completion(response.response?.statusCode, nil,self.convertDataToDictionary(jsonData:data))
          }
          
      }
    }
  }
  
  
  // MARK: - Convert Data to Dictionary of Any Data Request
  func convertDataToDictionary(jsonData: Data)-> String {
    do {
      let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String:Any]
      guard let dictError = json!["error"]  as? NSDictionary else {
        return ""
      }
      guard let dictDetail = dictError.value(forKey:"details")  as?  [Any] else {
        
        guard let errorMessage = dictError.value(forKey:"message")  as? String else {
          
          return ""
        }
        return errorMessage
      }
      
      if let json = dictDetail as? [[String:AnyObject]] {
        
        for object in json {
          
          guard let errorMessage = object["message"]  as? String else {
            
            guard let errorMessage = object["description"]  as? String else {
              
              return ""
            }
            return errorMessage
          }
          return errorMessage
        }
        
      }
      
      return ""
      
    } catch {
      
    }
    
    if let aStrError = String.init(data: jsonData, encoding:.utf8) {
      
      return aStrError
    }
    return ""
  }
  
  
  
  // MARK: - Get Status Code of Any Data Request
  func getStatusCode(url: URL, method: HTTPMethod, bodyData: [String:Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if let params = bodyData {
      Helper.DLog(message: "bodyParmas: \(params)")
    }
    
    Helper.DLog(message: "mehod: \(method)\nurl: \(url)")
    
    getDataRequest(url: url, method: method, bodyData: bodyData) { dataRequest, error in
      guard let dataRequest = dataRequest, error == nil else {
        completion(nil, error?.localizedDescription)
        return
      }
      
      dataRequest
        .response() { response in
          guard let statusCode = response.response?.statusCode, response.error == nil else {
            Helper.DLog(message: "error: \((response.error?.localizedDescription)!)")
            completion(nil, response.error?.localizedDescription)
            return
          }
          Helper.DLog(message: "URL::: \(String(describing: response.response?.url))\nSTATUS CODE::: \(statusCode)")
          if let data = response.data {
            completion(statusCode, self.convertDataToDictionary(jsonData:data))
          }else {
            completion(statusCode, "")
          }
      }
    }
  }
  
  // MARK: Upload multipart formaData Image
  func uploadImageAsMultipartformData(url: URL, method: HTTPMethod, bodyData: [String:Any]?, arrUploadDoc: [UploadDocument], completion: @escaping(Int?, String?) -> Void) {
    
    getToken(url.absoluteString.contains("auth")) { token, error in
      guard let token = token, error == nil else {
        completion(nil, error?.localizedDescription)
        return
      }
      
      let headers: HTTPHeaders =  ["Authorization": "Bearer \(token)"]
      
      AF.upload(multipartFormData: { multipartFormData in
        
        for i in 0..<arrUploadDoc.count {
          
          if let data = arrUploadDoc[i].data, let name = arrUploadDoc[i].name, let fileName = arrUploadDoc[i].fileName {
            multipartFormData.append(data, withName: "portfolio[\(i)]", fileName: fileName, mimeType: "image/jpeg")
          }
          
        }
        
      }, to: url, method: method , headers: headers).response { response in
        
        guard let statusCode = response.response?.statusCode, response.error == nil else {
          Helper.DLog(message: "error:::\(response.error?.localizedDescription ?? "")")
          completion(nil, response.error?.localizedDescription)
          return
        }
        Helper.DLog(message: "Upload statusCode:::\(response.response?.statusCode ?? 0)")
        if let responseData = response.data {
          completion(statusCode, self.convertDataToDictionary(jsonData:responseData))
        }else {
          completion(statusCode, "")
        }
        
      }.uploadProgress { (progress) in
        //completion(Int(progress.fractionCompleted),"Progress")
        Helper.DLog(message: "Progress::\(progress.fractionCompleted)")
      }
      
    }
    
  }
  
  
  
  // MARK: Upload multipart formaData Image
  func uploadProfilePicture(url: URL, method: HTTPMethod, arrUploadDoc: [UploadDocument], completion: @escaping(Int?, String?) -> Void) {
    
    getToken(url.absoluteString.contains("auth")) { token, error in
      guard let token = token, error == nil else {
        completion(nil, error?.localizedDescription)
        return
      }
      
      let headers: HTTPHeaders =  ["Authorization": "Bearer \(token)"]
      
      AF.upload(multipartFormData: { multipartFormData in
        
//        for (key, value) in (bodyData)! {
//          if let temp = value as? String {
//            multipartFormData.append(temp.data(using: .utf8)!, withName: key)
//          }
//        }
//
        for i in 0..<arrUploadDoc.count {
          
          if let data = arrUploadDoc[i].data, let name = arrUploadDoc[i].name, let fileName = arrUploadDoc[i].fileName {
            multipartFormData.append(data, withName: name, fileName: fileName, mimeType: "image/jpeg")
          }
          
        }
        
        
        
      }, to: url, method: method , headers: headers).response { response in
        
        guard let statusCode = response.response?.statusCode, response.error == nil else {
          Helper.DLog(message: "error:::\(response.error?.localizedDescription ?? "")")
          completion(nil, response.error?.localizedDescription)
          
          return
        }
        
        Helper.DLog(message: "Upload statusCode:::\(response.response?.statusCode ?? 0)")
        if let responseData = response.data {
          completion(statusCode, self.convertDataToDictionary(jsonData:responseData))
        }else {
          completion(statusCode, "")
        }
        
      }.uploadProgress { (progress) in
        //completion(Int(progress.fractionCompleted),"Progress")
        Helper.DLog(message: "Progress::\(progress.fractionCompleted)")
      }
      
    }
    
  }
  
}
