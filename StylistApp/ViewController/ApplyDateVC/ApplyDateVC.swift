//
//  ApplyDateVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 24/02/21.
//

import UIKit

class ApplyDateVC: UIViewController {
  
  // MARK:- @IBOutlets
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var pickerView: MonthYearPickerView!
  
  
  // MARK:- @vars
  var delegate: CustomDelegate? = nil
  var selectedDate: Date!
  var selectedMonth: String!
  var selectedYear: Int!
  
  lazy var MDate : MDatePickerView = {
    let mdate = MDatePickerView()
    mdate.delegate = self
    //mdate.Color = UIColor(red: 0/255, green: 178/255, blue: 113/255, alpha: 1)
    mdate.Color = Constants.Colors.colorPrimary!
    mdate.cornerRadius = 14
    mdate.translatesAutoresizingMaskIntoConstraints = false
    mdate.from = 2021
    mdate.to = 2050
    //mdate.selectDate = selectedDate
    return mdate
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureDatePickerView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - configure PickerView
  func configureDatePickerView() {
    
    /*topView.addSubview(MDate)
    NSLayoutConstraint.activate([
      //MDate.centerYAnchor.constraint(equalTo: topView.centerYAnchor, constant: 0),
      //MDate.centerXAnchor.constraint(equalTo: topView.centerXAnchor, constant: 0),
      MDate.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 0.0),
      MDate.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: 0.0),
      MDate.topAnchor.constraint(equalTo: topView.topAnchor, constant: 0.0),
      MDate.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0.0)
    ])*/
    
    pickerView.onDateSelected = { (month: String, year: Int) in
      //let string = String(format: "%02d/%d", month, year)
      //NSLog(string) // should show something like 05/2015
      self.selectedMonth = month
      self.selectedYear = year
    }

  }
  
  @IBAction func actionCancel(_ sender: Any) {
    if let popupVC = self.parent as? SBCardPopupViewController {
      popupVC.close()
    }
  }
 
  @IBAction func actionDone(_ sender: Any) {
    if let popupVC = self.parent as? SBCardPopupViewController {
      popupVC.close()
      self.delegate?.applyDate?(self.selectedMonth, year: self.selectedYear)
    }
  }
  
}

extension ApplyDateVC : MDatePickerViewDelegate {
    func mdatePickerView(selectDate: Date) {
        
      selectedDate = selectDate
    }
}


class MonthYearPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var months: [String]!
    var years: [Int]!
    
    var month = Calendar.current.component(.month, from: Date()) {
        didSet {
            selectRow(month-1, inComponent: 0, animated: false)
        }
    }
    
    var year = Calendar.current.component(.year, from: Date()) {
        didSet {
          
            selectRow(years.index(of: year)!, inComponent: 1, animated: true)
        }
    }
    
    var onDateSelected: ((_ month: String, _ year: Int) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonSetup()
    }
    
    func commonSetup() {
        // population years
        var years: [Int] = []
        if years.count == 0 {
            var year = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
            for _ in 1...15 {
                years.append(year)
                year += 1
            }
        }
        self.years = years
        
        // population months with localized names
        var months: [String] = []
        var month = 0
        for _ in 1...12 {
            months.append(DateFormatter().monthSymbols[month].capitalized)
            month += 1
        }
        self.months = months
        
        self.delegate = self
        self.dataSource = self
        
        let currentMonth = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.month, from: NSDate() as Date)
        self.selectRow(currentMonth - 1, inComponent: 0, animated: false)
      
        self.setValue(UIColor.black, forKey: "textColor")
        self.setValue(UIColor.white, forKey: "backgroundColor")
      
     
    }
    
    // Mark: UIPicker Delegate / Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return months[row]
        case 1:
            return "\(years[row])"
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return months.count
        case 1:
            return years.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let month = months[self.selectedRow(inComponent: 0)]
        let mmonth =  self.selectedRow(inComponent: 0)+1
        let year = years[self.selectedRow(inComponent: 1)]
        if let block = onDateSelected {
            block(month, year)
        }
        
      self.month = mmonth
      self.year = year
    }
    
}
