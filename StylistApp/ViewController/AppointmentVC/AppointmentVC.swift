//
//  AppointmentVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 25/01/21.
//

import UIKit

class AppointmentVC: UIViewController {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var detailView: UIView!
  @IBOutlet weak var tblAppointment: UITableView!
  @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var tblViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var calendar: FSCalendar!
  @IBOutlet weak var lblDate: UILabel!
  @IBOutlet weak var btnSelectionDate: UIButton!
  @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var calendarSuperViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet var calendarTopViewHeightConstraint: [NSLayoutConstraint]!
  @IBOutlet weak var topHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
  
  // MARK: - @vars
  var isCalendarOpen: Bool = true
  var arrDetailsList: [AppointmentDetails] = []
  var arrAppointmentList: [AppointmentDetails] = []
  fileprivate lazy var dateFormatter: DateFormatter = {
      let formatter = DateFormatter()
    formatter.dateFormat = DateFormats.iso.format() //"yyyy/MM/dd"
      return formatter
  }()
  
  fileprivate lazy var dateFormatter2: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = DateFormats.fetched.format()
      return formatter
  }()
  
  fileprivate lazy var dateFormatter3: DateFormatter = {
      let formatter = DateFormatter()
      formatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
      formatter.dateFormat = DateFormats.appointment.format()
      return formatter
  }()
  
  fileprivate lazy var dateFormatter4: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = DateFormats.appointment.format()
      return formatter
  }()
  
  var datesWithEvent: [String] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    initializeOnce()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }

  
  // MARK: - initializeOnce
  func initializeOnce() {
    
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    self.detailView.isHidden = true
    cofigureTableView()
    configureCalendar()
    Helper.showLoading(self.activityIndicator)
    callWebServices()
  }
  
  // MARK:- Configure Tableview
  func cofigureTableView() {
    self.tblAppointment.backgroundColor = UIColor.white
    self.tblAppointment.register(UINib(nibName: Constants.TableViewCell.AppointmentCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.AppointmentCellID)
    self.tblAppointment.register(UINib(nibName: Constants.TableViewCell.NoAppointmentCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.NoAppointmentCellID)
  }
  
  // MARK: - Configure Calendar
  func configureCalendar() {
    
    self.calendar.delegate = self
    self.calendar.dataSource = self
    self.calendar.select(Date())
    let strDate = Date().convertDateToString(strDateFormate: DateFormats.birth.format())
    self.lblDate.text = strDate
    self.calendar.scope = .month
    // For UITest
    self.calendar.accessibilityIdentifier = "calendar"
    self.btnSelectionDate.transform = CGAffineTransform(rotationAngle: -(.pi))
  }
  
  // MARK: - Call WebServices
  func callWebServices() {
    let startDate = self.dateFormatter3.string(from: Date().startOfMonth())
    let endDate = self.dateFormatter3.string(from: Date().endOfMonth())
    callGetAppointmentListAPI(startDate, endDate: endDate)
  }
  
  // MARK: - GET APPOINTMNET LIST OF STYLIST
  func callGetAppointmentListAPI(_ startDate: String, endDate: String, onDateSelect: Bool = false) {
    
    if Reachability.isConnectedToNetwork() {
      
      let queryParmas: [String:Any]? = ["start_date": startDate, "end_date": endDate]
      
      APIHandler.shared.getAppointmentList(queryParams: queryParmas) { (statuscode, dataResponse, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        guard error == nil else {
          self.updateUI()
          return
        }
        
        self.arrDetailsList = []
        self.arrAppointmentList = []
        
        if onDateSelect == false {
          self.datesWithEvent  = []
        }
        
        
        
        if let dataArray = dataResponse?.data {
          
          for element in dataArray {
            self.arrDetailsList.append(element)
          }
          
          for element in self.arrDetailsList {
            self.arrAppointmentList.append(element)
          }
         
          if onDateSelect == false {
           
            let arrStartTime =  self.arrAppointmentList.map({ (details) -> String in
              
              return details.startTime!
              
            })
            
            let arrDates = arrStartTime.map({$0.dateFromISO8601}) //arrStartTime.map({self.dateFormatter2.date(from: $0)})
            
            let arrString = arrDates.map({self.dateFormatter2.string(from: $0!)})
            
            self.datesWithEvent = arrString.removingDuplicates()
            
            self.calendar.reloadData()
            
          }
          
          
        }
        
        
        
        self.updateUI()
        
        
      }
      
    }else {
      Helper.hideLoading(self.activityIndicator)
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  
  // MARK: - Update UI
  func updateUI() {
  
    
    guard self.arrAppointmentList.count > 0 else {
      self.detailView.isHidden = false
      self.tblAppointment.delegate = self
      self.tblAppointment.dataSource = self
      self.tblAppointment.isHidden = false
      calculateTableViewHeight()
      return
    }
    
    self.tblAppointment.delegate = self
    self.tblAppointment.dataSource = self
    self.tblAppointment.isHidden = false
    self.detailView.isHidden = false
    calculateTableViewHeight()

  }
  
  // MARK: - Calculate Height Of Tableview
  func calculateTableViewHeight() {
    
    if self.arrAppointmentList.count == 0 {
      
      self.tblAppointment.reloadData()
      self.tblAppointment.layoutIfNeeded()
      self.tblViewHeightConstraint.constant = self.tblAppointment.contentSize.height
      self.view.layoutIfNeeded()
      
    }else {
      UIView.animate(withDuration: 0.5) {
        self.tblViewHeightConstraint.constant = CGFloat.greatestFiniteMagnitude
        self.tblAppointment.reloadData()
        self.tblAppointment.layoutIfNeeded()
        self.tblViewHeightConstraint.constant = self.tblAppointment.contentSize.height
        self.view.layoutIfNeeded()
        
      }
      
    }
    
  }
  
  // MARK: - @IBActions
  @IBAction func actionLeftMenu(_ sender: Any) {
    if let sideMenuController = self.parent?.parent?.parent?.parent as? PGSideMenu {
      if let sidemneuVC = sideMenuController.leftMenuController?.children[0] as? SideMenuVC {
        sidemneuVC.updateUI()
      }
      sideMenuController.toggleLeftMenu()
    }
  }
  
  @IBAction func actionChooseDate(_ sender: Any) {
  
    animateCalendarTopBottom()
  }
  
  // MARK:- Animate Calendar
  func animateCalendarTopBottom() {
    
    if isCalendarOpen == true {
      
      isCalendarOpen = false
      
      UIView.animate(withDuration: 0.5) {
        
        self.btnSelectionDate.transform = CGAffineTransform.identity
        self.calendarTopViewHeightConstraint[0].constant = 0.0
        self.calendarTopViewHeightConstraint[1].constant = 0.0
        self.calendarSuperViewHeightConstraint.constant = 0.0
        self.topHeightConstraint.constant = 0.0
        self.bottomHeightConstraint.constant = 0.0
        self.calendar.clipsToBounds = true
        self.view.layoutIfNeeded()
      }
      
    }else {
      
      isCalendarOpen = true
      
      UIView.animate(withDuration: 0.5) {
        self.btnSelectionDate.transform = self.btnSelectionDate.transform.rotated(by: -(.pi/2))
        self.btnSelectionDate.transform = self.btnSelectionDate.transform.rotated(by: -(.pi/2))
        self.calendarTopViewHeightConstraint[0].constant = 100.0
        self.calendarTopViewHeightConstraint[1].constant = -80.0
        self.calendarSuperViewHeightConstraint.constant = 350.0
        self.topHeightConstraint.constant = 10.0
        self.bottomHeightConstraint.constant = 10.0
        self.calendar.clipsToBounds = true
        self.view.layoutIfNeeded()
      }
    }
  }
  
  // MARK: - setDate on Header
  func setDateOnHeader(_ date: Date) {
    let strDate = date.convertDateToString(strDateFormate: DateFormats.birth.format())
    self.lblDate.text = strDate
  }
 
  // MARK:- Load data when select date or scroll Calendar
  func scrollOrSelectCalendarDate(_ date: Date, day: Int, month: Int, isOnDateSelect: Bool) {
    
    let strStartDate = self.dateFormatter4.string(from: date)
    
    var objCalendar = Calendar.current
    objCalendar.timeZone =  NSTimeZone(abbreviation: "UTC")! as TimeZone
    
    var components = DateComponents()
    components.day = day //-1
    components.month = month //1
    components.hour = 23
    components.minute = 59
    components.second = 59
    
    let endDate = objCalendar.date(byAdding: components, to: self.dateFormatter4.date(from: strStartDate)!)!
    
    let strEndDate = self.dateFormatter4.string(from: endDate)
    
    callGetAppointmentListAPI(strStartDate, endDate: strEndDate, onDateSelect: isOnDateSelect)

    if isOnDateSelect == true {
      animateCalendarTopBottom()
    }
  }
}


// MARK: - UITableViewDelegate, UITableViewDataSource
extension AppointmentVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if self.arrAppointmentList.count == 0 {
      return 1
    }
    
    return self.arrAppointmentList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    if self.arrAppointmentList.count == 0 {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.NoAppointmentCellID, for: indexPath) as? NoAppointmentCell else {
        return UITableViewCell()
      }
      
      return objCell
      
    }else {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.AppointmentCellID, for: indexPath) as? AppointmentCell else {
        return UITableViewCell()
      }
      
      if indexPath.row == self.arrAppointmentList.count - 1 {
        objCell.lineHeightConstraint.constant = 0.0
      }else {
        objCell.lineHeightConstraint.constant = 1.0
      }
      
      //objCell.layoutIfNeeded()
      objCell.configureCell(self.arrAppointmentList[indexPath.row])
      
      
      
      return objCell
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //return 20
    
    if self.arrAppointmentList.count == 0 {
      return 0
    }
    
    return 30
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if self.arrAppointmentList.count == 0 {
      let view = UIView()
      view.backgroundColor = UIColor.clear
      return view
    }
    
    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tblAppointment.bounds.size.width, height: 50.0))
    view.backgroundColor = UIColor.white
    
    let label = UILabel(frame: CGRect(x: 10, y: 10, width: view.frame.size.width - 20.0, height: 20.0))
    label.text = "Appointment"
    label.textColor = UIColor.black
    label.font = MainFont.bold.with(size: 17.0) //UIFont(name: "Roboto-Bold", size: 17.0)
    view.addSubview(label)
    return view
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView()
    view.backgroundColor = UIColor.clear
    return view
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0
  }
    
}

// MARK:- FSCalendarDelegate, FSCalendarDataSource
extension AppointmentVC: FSCalendarDelegate, FSCalendarDataSource {
 
  func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {

    print("did select date \(self.dateFormatter.string(from: date))")
   
    setDateOnHeader(date)
    
    let selectedDates = calendar.selectedDates.map({self.dateFormatter2.string(from: $0)})
    print("selected dates is \(selectedDates)")
    if monthPosition == .next || monthPosition == .previous {
        calendar.setCurrentPage(date, animated: true)
    }

 
    scrollOrSelectCalendarDate(date, day: 0, month: 0, isOnDateSelect: true)
    
  }
  
  /*func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
    self.calendarHeightConstraint.constant = bounds.height
    self.view.layoutIfNeeded()
  }*/
  
  func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
    
    Helper.DLog(message: "\(self.dateFormatter.string(from: calendar.currentPage))")
    
    setDateOnHeader(calendar.currentPage)
    
    scrollOrSelectCalendarDate(calendar.currentPage, day: -1, month: 1, isOnDateSelect: false)
  }
  
  func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
    let dateString = self.dateFormatter2.string(from: date)
    if self.datesWithEvent.contains(dateString) {
        return 1
    }
    return 0
  }
  
}
 
// MARK:- FSCalendarDelegateAppearance
extension AppointmentVC: FSCalendarDelegateAppearance {
  
  func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventSelectionColorsFor date: Date) -> [UIColor]? {
    return [Constants.Colors.colorPrimaryDark!]
    
  }
  
  func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
      let key = self.dateFormatter2.string(from: date)
      if self.datesWithEvent.contains(key) {
          //return [UIColor.magenta, appearance.eventDefaultColor, UIColor.black]
        return [Constants.Colors.colorPrimaryDark!] //[Constants.Colors.colorlightgray!]
      }
      return nil
  }
  
//  func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
//      /*let key = self.dateFormatter1.string(from: date)
//      if let color = self.fillSelectionColors[key] {
//        return UIColor.magenta
//      }*/
//
//
//      return UIColor.green
//      //return appearance.selectionColor
//  }
//
//  func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
//      /*let key = self.dateFormatter1.string(from: date)
//      if let color = self.fillDefaultColors[key] {
//          return color
//      }*/
//      return UIColor.brown
//      //return nil
//  }
//
//  func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
//      /*let key = self.dateFormatter1.string(from: date)
//      if let color = self.borderDefaultColors[key] {
//          return color
//      }*/
//      return UIColor.magenta
//      //return appearance.borderDefaultColor
//  }
//
//  func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
//      /*let key = self.dateFormatter1.string(from: date)
//      if let color = self.borderSelectionColors[key] {
//          return color
//      }*/
//      //return appearance.borderSelectionColor
//  }
  
  func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderRadiusFor date: Date) -> CGFloat {
      /*if [8, 17, 21, 25].contains((self.gregorian.component(.day, from: date))) {
          return 0.0
      }*/
      return 1.0
  }
  
}


extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()

        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }

    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
