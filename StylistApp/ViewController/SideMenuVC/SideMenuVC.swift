//
//  SideMenuVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 27/01/21.
//

import UIKit

class SideMenuVC: UIViewController {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var profileIcon: UIImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblEmail: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    initializeOnce()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - initializeOnce
  func initializeOnce() {
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    callProfileDetailsAPI { (isCompleted) in

      if isCompleted == true {
        self.updateUI()
      }
    }
  }
  
  // MARK: - Call Profile Details API
  func callProfileDetailsAPI(_ completion: @escaping(Bool) -> Void) {
    
    APIHandler.shared.getStylistDetails { (statuscode, dataResponse, error) in
      
      var stylistDetails: StylistDetails? = nil
      
      guard error == nil else {
        completion(false)
        return
      }
      
      if let response = dataResponse?.data {
        
        if let stylist = response.stylist {
          
          stylistDetails = stylist
          
//          let encoder = JSONEncoder()
//          if let encoded = try? encoder.encode(stylistDetails) {
//            let defaults = UserDefaults.standard
//            defaults.set(encoded, forKey: Constants.UserDefaults.StylistDetails)
//            self.updateUI()
//          }
          
          if GlobalFunction.addStylistDetails(stylistDetails) == true {
            
            completion(true)
          }else {
            completion(false)
            return
          }
          
        }
        
      }
      
    }
    
  }
  
  // MARK: updateUI
  func updateUI() {
    
    if let dicStylist = GlobalFunction.getStylistDetails() {
      
      if let fullName = dicStylist.fullName, fullName != "" {
        self.lblName.text = fullName
      }else {
        self.lblName.text = ""
      }
      
      if let email = dicStylist.email, email != "" {
        self.lblEmail.text = email
      }else {
        self.lblEmail.text = ""
      }
      
      if let thumbnail = dicStylist.profilePic {
        self.profileIcon.setImage(url: thumbnail, style: .squared, completion: nil)
      }
    }
  }
  
  // MARK:  - @IBActions
  @IBAction func actionLogout(_ sender: Any) {
    
    let alertVC = GlobalFunction.openCustomAlert()
    alertVC.commonAlert = .logout
    alertVC.customDelegate = self
    self.parent?.parent?.present(alertVC, animated: true, completion: nil)
    
  }
  
  @IBAction func actionOption(_ sender: Any) {
    
    let btn = sender as! UIButton
    
    if let sideMenuController = self.parent?.parent as? PGSideMenu {
      
      if let objPrivacyPolicyVC = Constants.kSideMenuVC.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as? PrivacyPolicyVC {
        
        if btn.tag == 0 {
          objPrivacyPolicyVC.strTitle = "Privacy Policy"
        }else if btn.tag == 1 {
          objPrivacyPolicyVC.strTitle = "Terms of Use"
        }else if btn.tag == 2 {
          objPrivacyPolicyVC.strTitle = "FAQs"
        }
        
        guard let url = URL(string: "https://google.com/") else {
            return
        }

        
        objPrivacyPolicyVC.disableZoom = true
        //objPrivacyPolicyVC.navigationWay = .push
        /*objPrivacyPolicyVC.toolbarItemTypes = [.reload, .activity]*/
        objPrivacyPolicyVC.url = url
        /*objPrivacyPolicyVC.pullToRefresh = true
        objPrivacyPolicyVC.headers = ["browser": "in-app browser"]
        //objPrivacyPolicyVC.tintColor = .cyan
        objPrivacyPolicyVC.cookies = [
            HTTPCookie(properties:
            [HTTPCookiePropertyKey.originURL: url.absoluteString,
             HTTPCookiePropertyKey.path: "/",
             HTTPCookiePropertyKey.name: "author",
             HTTPCookiePropertyKey.value: "Zheng-Xiang Ke"])!,
            HTTPCookie(properties:
            [HTTPCookiePropertyKey.originURL: url.absoluteString,
             HTTPCookiePropertyKey.path: "/",
             HTTPCookiePropertyKey.name: "GitHub",
             HTTPCookiePropertyKey.value: "kf99916"])!]*/
        
        if let navVC = sideMenuController.contentController as? UINavigationController  {
          navVC.pushViewController(objPrivacyPolicyVC, animated: true)
          sideMenuController.hideMenu()
        }
        
        /*if let tabBarVC = sideMenuController.contentController?.children[0] as? TabBarController {
          
          if let navVC = tabBarVC.selectedViewController as? UINavigationController {
            navVC.pushViewController(objNotificationVC, animated: true)
          }
        }*/
      }
      
      
    }
  }
}

// MARK: - CustomDelegate
extension SideMenuVC: CustomDelegate {
  
  func actionCustomAlertDialog(_ btn: UIButton) {
    if let sideMenuController = self.parent?.parent as? PGSideMenu {
      sideMenuController.hideMenu()
      GlobalFunction.logOut()
    }
  }
}
