//
//  EditProfileVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 17/02/21.
//

import UIKit
import libPhoneNumber_iOS
import JVFloatLabeledTextField
import QCropper

class EditProfileVC: UIViewController {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var imgUserProfile: UIImageView!
  @IBOutlet weak var txtFullName: FloatingTextField!
  @IBOutlet weak var txtViewAboutMe: JVFloatLabeledTextView!
  @IBOutlet weak var txtGender: FloatingTextField!
  @IBOutlet weak var txtBirthDate: FloatingTextField!
  @IBOutlet weak var txtMobileNo: FloatingTextField!
  @IBOutlet weak var txtEmail: FloatingTextField!
  @IBOutlet weak var progressHUD: NVActivityIndicatorView!
  @IBOutlet var heightConstraint: [NSLayoutConstraint]!
  @IBOutlet weak var bottomLineView: UIView!
  
  // MARK:- @vars
  var profileDetails: StylistDetails? = nil
  var arrGender: [String] = ["Male", "Female"]
  var birthDatePicker = UIDatePicker()
  var dateFormatter: DateFormatter!
  var birthDate: Date!
  var strMobile: String = ""
  var handler: GrowingTextViewHandler!
  var arrDocument: [UploadDocument] = [UploadDocument(title: "Profile", img: nil, data: nil, name: "profile_pic", fileName: "profilepic.jpeg")]
  var originalImage: UIImage?
  var cropperState: CropperState?
  let minimumlines = 10
  
  // MARk:- @let
  let pickerView = UIPickerView()
  
  
  // MARK:- View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    // Do any additional setup after loading the view.
  }

  // MARK: - setupView
  func setupView() {
    Helper.showLoading(self.progressHUD)
    pickerView.delegate = self
    pickerView.dataSource = self
    txtGender.inputView = pickerView
    birthDatePicker.datePickerMode = .date
    if #available(iOS 13.4, *) {
      birthDatePicker.preferredDatePickerStyle = .wheels
    } else {
      // Fallback on earlier versions
    }
    birthDatePicker.addTarget(self, action: #selector(self.onDateChange(_:)), for: .valueChanged)
    self.txtBirthDate.inputView = birthDatePicker
    dateFormatter = DateFormatter()
    dateFormatter.dateFormat = DateFormats.birth.format()
    birthDatePicker.maximumDate = Date()
    
    self.txtViewAboutMe.isScrollEnabled = false
    
    self.handler = GrowingTextViewHandler(textView: self.txtViewAboutMe, heightConstraint: self.heightConstraint[0])
    self.handler.minimumNumberOfLines = minimumlines
   
    self.callProfileDetailsAPI()
  }
  
  // MARK: - Start Date Change
  @objc func onDateChange(_ sender: UIDatePicker) {
    birthDate = sender.date
    self.txtBirthDate.text = dateFormatter.string(from: sender.date)
  }
  
  // MARK: - Call Profile Details API
  func callProfileDetailsAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      APIHandler.shared.getStylistDetails { (statuscode, dataResponse, error) in
        
        self.profileDetails = nil
        
        Helper.hideLoading(self.progressHUD)
        
        guard error == nil else {
          Helper.showToast(_strMessage: error)
          return
        }
        
        if let response = dataResponse?.data {
          
          if let stylist = response.stylist {
            
            self.profileDetails = stylist
            
            if GlobalFunction.addStylistDetails(self.profileDetails) == true {
              self.updateUI()
            }else {
              return
            }
            
          }
          
        }
        
      }
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
  // MARK: updateUI
  func updateUI() {
    
    if let dicStylist = GlobalFunction.getStylistDetails() {
      
      if let fullName = dicStylist.fullName, fullName != "" {
        self.txtFullName.text = fullName
      }else {
        self.txtFullName.text = ""
      }
      
      if let info = dicStylist.info, info != "" {
        self.txtViewAboutMe.text = info
        self.handler.setText(info, animated: false)
      }else {
        self.txtViewAboutMe.text = ""
        self.handler.setText(self.txtViewAboutMe.text, animated: false)
      }
      
      self.textViewDidChange(self.txtViewAboutMe)
      
      if let gender = dicStylist.gender, gender != "" {
        self.txtGender.text = gender.capitalized
        
        if self.txtGender.text == self.arrGender[0] {
          self.pickerView.selectRow(0, inComponent: 0, animated: true)
        }else {
          self.pickerView.selectRow(1, inComponent: 0, animated: true)
        }
        
      }else {
        self.txtGender.text = ""
      }
      
      if let birthdate = dicStylist.birthdate, birthdate != "" {
        self.txtBirthDate.text = "\(Date.convertDateUTCToLocal(strDate: birthdate , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormats.birth.format()
        if let text = self.txtBirthDate.text {
          if let date = formatter.date(from: text) {
            birthDate = date
            self.birthDatePicker.date = date
          }
        }
        
      }else {
        self.txtBirthDate.text = ""
        self.birthDatePicker.date = Date()
      }
      
      if let mobileNumber = dicStylist.mobileNumber, mobileNumber != "" {
        self.txtMobileNo.text = mobileNumber
      }else {
        self.txtMobileNo.text  = ""
      }
      
      if let email = dicStylist.email, email != "" {
        self.txtEmail.text = email
      }else {
        self.txtEmail.text = ""
      }
      
      if let thumbnail = dicStylist.profilePic {
        self.imgUserProfile.setImage(url: thumbnail, style: .squared, completion: nil)
      }
    
    }
  }
  
  
  // MARK:- @IBAction
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func actionSave(_ sender: Any) {
    
    self.view.endEditing(true)
    
    if checkEveryFieldIsFilled() == true {
      
      strMobile = ""
      
      let phoneUtil = NBPhoneNumberUtil()
      
      do {
        let phoneNumber: NBPhoneNumber = try phoneUtil.parse(self.txtMobileNo.text?.trim(), defaultRegion: UserDefaults.standard.value(forKey:Constants.CountryCode.countryCode) as? String)
        try phoneUtil.format(phoneNumber, numberFormat: .E164)
        
        if phoneUtil.getNumberType(phoneNumber) == .MOBILE || phoneUtil.getNumberType(phoneNumber) == .FIXED_LINE_OR_MOBILE   {
          
          let isNumberValid = phoneUtil.isValidNumber(phoneNumber)
          
          if isNumberValid == true {
            strMobile = "+\(phoneNumber.countryCode!)\(phoneNumber.nationalNumber!)"
            self.callEditProfileAPI()
          }else {
            Helper.showToast(_strMessage: ErrorMessage.invalidmobileregioncode.message())
            return
          }
        }else {
          Helper.showToast(_strMessage: ErrorMessage.invalidmobileregioncode.message())
          return
        }
        
      }
      catch let error as NSError {
        print(error)
        Helper.showToast(_strMessage: ErrorMessage.invalidmobileregioncode.rawValue)
        return
      }
      
    }
  
  }
  
  
  
  // MARK: - Call Edit Profile API
  func callEditProfileAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      self.view.isUserInteractionEnabled = false
      
      Helper.showLoading(self.progressHUD)
      
      //      if let image = self.imgUserProfile.image {
      //        let imgData = image.jpegData(compressionQuality: 0.6)
      //        self.arrDocument[0].data = imgData
      //      }
      
      let strFullName = self.txtFullName.text?.trim()
      let strGender = self.txtGender.text?.trim().lowercased()
      
      //DispatchQueue.global(qos: .default).async(execute: {
      
      var bodyParams: [String: Any]? = ["full_name": strFullName ?? "", "gender": strGender ?? "", "mobile_number":  self.strMobile]
      
      if self.txtBirthDate.text?.trim().count != 0 {
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormats.fetched.format()
        
        let strBirthdate = formatter.string(from: self.birthDate)
        bodyParams?["birthdate"] = strBirthdate
      }
      
      if self.txtEmail.text?.trim().count != 0 {
        bodyParams?["email"] = self.txtEmail.text?.trim()
      }
      
      if self.txtViewAboutMe.text.trim().count != 0 {
        bodyParams?["info"] = self.txtViewAboutMe.text.trim()
      }
      
      
      APIHandler.shared.updateStylistDetails(bodyParams: bodyParams) { (statuscode, error) in
        
        self.view.isUserInteractionEnabled = true
        
        DispatchQueue.main.async(execute: {
          Helper.hideLoading(self.progressHUD)
        })
        
        if statuscode == 204 {
          
          Helper.showToast(_strMessage: SuccessMessage.profileupdatesuccess.message())
          
          if let sidemenuVC = self.parent?.parent?.children[1].children[0] as? SideMenuVC {
            
            sidemenuVC.callProfileDetailsAPI { (isCompleted) in
              
              if isCompleted == true {
                
                if let profileVC = self.parent?.parent?.children[0].children[0].children[2].children[0] as? ProfileVC {
                  
                  if let aboutVC = profileVC.viewControllers[0] as? AboutVC {
                    
                    aboutVC.updateUI()
                    self.navigationController?.popViewController(animated: true)
                  }
                  
                }
              }
            }
            
            
          }
          
        }else {
          Helper.showToast(_strMessage: error)
          return
        }
        
      }
      
      //})
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
  // MARK: - @@IBActions
  @IBAction func actionChangePicture(_ sender: Any) {
    self.openAlertController()
  }
  
  // MARK: - Check textfiled blank or not
  func checkEveryFieldIsFilled() -> Bool {
    
    if self.txtFullName.text?.trim().count ==  0 {
      Helper.showToast(_strMessage: ErrorMessage.name.message())
      return false
    }else if (self.txtFullName.text?.trim().count)! < MinLength.personname.rawValue {
      Helper.showToast(_strMessage: ErrorMessage.nameminlength.message())
      return false
    }else if self.txtMobileNo.text?.trim().count ==  0 {
      Helper.showToast(_strMessage: ErrorMessage.mobile.message())
      return false
    }else if self.txtEmail.text?.trim().count != 0 {
      if Helper.isValidEmail((self.txtEmail.text?.trim())!) == false {
        Helper.showToast(_strMessage: ErrorMessage.invalidemail.message())
        return false
      }
    }
    
    return true
  }
  
  // MARK: - Open Alert Controller
  func openAlertController() {
    
    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
    
    let takePhotoAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
      (alert: UIAlertAction!) -> Void in
      
      Helper.cameraAuthorizationStatus({ (result) in
        
        if result {
          DispatchQueue.main.async {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
              let imagePicker = UIImagePickerController()
              imagePicker.delegate = self
              imagePicker.sourceType = .camera
              imagePicker.allowsEditing = true
              self.present(imagePicker, animated: true, completion: nil)
            }else {
              
              Helper.showToast(_strMessage: Constants.Toast.noCameraAvailable)
            }
          }
          
        }else {
          DispatchQueue.main.async {
            let alert = UIAlertController(title: Constants.Toast.cameraAccessTitle, message: Constants.Toast.cameraAccessMessage, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: Constants.Toast.enableCameraAccess, style: .default) { action in
              UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            })
            
            alertController.pruneNegativeWidthConstraints()
            self.present(alert, animated: true, completion: nil)
          }
        }
      })
    })
    
    takePhotoAction.setValue(UIColor.black, forKey: "titleTextColor")

    let photoLibraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
        (alert: UIAlertAction!) -> Void in
        
        Helper.photoLibraryAuthorizationStatus({ (result) in
            
            if result {
                DispatchQueue.main.async {
                    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                        let imagePicker = UIImagePickerController()
                        imagePicker.navigationBar.tintColor = Constants.Colors.colorPrimary
                        imagePicker.delegate = self
                        imagePicker.sourceType = .photoLibrary
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                        
                    }
                }
                
            }else {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: Constants.Toast.libraryAccessTitle, message: Constants.Toast.libraryAccessMessage, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: Constants.Toast.enableLibraryAccess, style: .default) { action in
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                    })
                    alertController.pruneNegativeWidthConstraints()
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })
        
    })
    
    photoLibraryAction.setValue(UIColor.black, forKey: "titleTextColor")
    
    let cancelAction = UIAlertAction(title: Constants.Toast.cancel, style: .cancel, handler: {
      (alert: UIAlertAction!) -> Void in
    })
    
    cancelAction.setValue(UIColor.black, forKey: "titleTextColor")
    
    alertController.addAction(takePhotoAction)
    alertController.addAction(photoLibraryAction)
    alertController.addAction(cancelAction)
    
    if #available(iOS 13.0, *) {
         alertController.overrideUserInterfaceStyle = .light
    } else {
      // Fallback on earlier versions
    }
    
    alertController.pruneNegativeWidthConstraints()
    self.present(alertController, animated: true, completion: nil)
  }
  
  // MARK: - Profile Picture Upload Success
  func uploadPicture(_ image: UIImage) {
    
    if Reachability.isConnectedToNetwork() {
      
      let imgData = image.jpegData(compressionQuality: 1.0)
      self.arrDocument[0].data = imgData
      
      APIHandler.shared.addProfilePic(self.arrDocument) { (statuscode, error) in
        
        if statuscode == 204 {
          
          self.imgUserProfile.image = image
          
          Helper.showToast(_strMessage: SuccessMessage.profilePicSuccess.message())
          
          if let sidemenuVC = self.parent?.parent?.children[1].children[0] as? SideMenuVC {
            
            sidemenuVC.callProfileDetailsAPI { (isCompleted) in
              
              if isCompleted == true {
                
                if let profileVC = self.parent?.parent?.children[0].children[0].children[2].children[0] as? ProfileVC {
                  
                  if let aboutVC = profileVC.viewControllers[0] as? AboutVC {
                    
                    aboutVC.updateUI()
                    
                  }
                  
                }
              }
            }
          }
          
        }else {
          Helper.showToast(_strMessage: error)
        }
      }
      
    }
    else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
    
    
  }
}


// MARK: - UITextFieldDelegate
extension EditProfileVC : UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
   
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
    
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
   
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if range.location == 0 && string == " " {
      return false
    }
    
    let currentString: NSString = textField.text! as NSString
    let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
    
    if textField.tag == 0 {
       return newString.length <= MaxLength.personname.rawValue
    }else if textField.tag == 3 {
      return newString.length <= MaxLength.mobile.rawValue
    }
    
    return true
  }
  
  
}


// MARK: - UITextViewDelegate
extension EditProfileVC: UITextViewDelegate {
  
  func textViewDidChange(_ textView: UITextView) {
    
    let numLines = Helper.numberOfLines(textView: textView)
    
    let sizeThatFitsTextView = textView.sizeThatFits(CGSize(width: textView.bounds.size.width, height: CGFloat(MAXFLOAT)))
    
    print(sizeThatFitsTextView)
    
    if numLines > minimumlines {
      textView.isScrollEnabled = true
      
    }else {
      
      if textView.text == "" {
        self.heightConstraint[0].constant = 49
        self.heightConstraint[1].constant = 50
      }else {
        self.heightConstraint[0].constant = sizeThatFitsTextView.height //+ 5.0
        self.heightConstraint[1].constant = self.heightConstraint[0].constant + 1.0
      }
    
      
    }
  }
  
  func textViewDidBeginEditing(_ textView: UITextView) {

    self.bottomLineView.backgroundColor = Constants.Colors.colorPrimary
      
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
   
    self.bottomLineView.backgroundColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1.0)
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    
    if text == "\n" {
      textView.resignFirstResponder()
      return true
    }
    
    if range.location == 0 && text == " " {
      return false
    }
 
    
    let newLength = textView.text.utf16.count + text.utf16.count - range.length
    
    if newLength == 0 {
      
      self.heightConstraint[0].constant = 49
      self.heightConstraint[1].constant = 50
    
      return true
    }
    
    else if newLength > 0 && newLength <= MaxLength.leaveReason.rawValue {
      return true
    }
    else {
      return false
    }
    
  }
  
}

// MARK:- UIPickerViewDelegate
extension EditProfileVC: UIPickerViewDelegate, UIPickerViewDataSource {
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return self.arrGender.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return self.arrGender[row]
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    self.txtGender.text = self.arrGender[row].capitalized
  }
  
}

// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    // Local variable inserted by Swift 4.2 migrator.
    let info = Helper.convertFromUIImagePickerControllerInfoKeyDictionary(info)
    let image = info[Helper.convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as! UIImage
    //
    originalImage = image
    
    let cropper = CropperViewController(originalImage: image)

    cropper.delegate = self

    picker.dismiss(animated: true) {
      self.present(cropper, animated: true, completion: nil)
    }
    
    /*if Reachability.isConnectedToNetwork() {
      
      let imgData = image.jpegData(compressionQuality: 0.6)
      self.arrDocument[0].data = imgData
      
      APIHandler.shared.addProfilePic(self.arrDocument) { (statuscode, error) in
        
        picker.dismiss(animated: true) {
          self.uploadPicture(image, statusCode: statuscode, error: error)
        }
        
        /*if statuscode == 204 {
          
          if let sidemenuVC = self.parent?.parent?.children[1].children[0] as? SideMenuVC {
            
            sidemenuVC.callProfileDetailsAPI { (isCompleted) in
              
              if isCompleted == true {
                
                if let profileVC = self.parent?.parent?.children[0].children[0].children[2].children[0] as? ProfileVC {
                  
                  if let aboutVC = profileVC.viewControllers[0] as? AboutVC {
                    
                    aboutVC.updateUI()
                    
                    picker.dismiss(animated: true) {
                     
                      self.imgUserProfile.image = image
                      Helper.DLog(message: "\(SuccessMessage.profilePicSuccess.message())")
                      
                      
                    }
                    
                    
                    
                  }
                  
                }
              }
            }
            
            
          }
        }else {
          Helper.showToast(_strMessage: error)
        }*/
        
      }
      
    }else {
      dismiss(animated: true, completion: nil)
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }*/
    
  }
}

// MARK:- CropperViewControllerDelegate
extension EditProfileVC: CropperViewControllerDelegate {
  func cropperDidConfirm(_ cropper: CropperViewController, state: CropperState?) {
    
    cropper.dismiss(animated: true, completion: nil)
    
    if let state = state, let image = cropper.originalImage.cropped(withCropperState: state) {
      
      cropperState = state
       
      Helper.DLog(message: "\(cropper.isCurrentlyInInitialState)")
      Helper.DLog(message: "\(image)")
      
      self.uploadPicture(image)
      
    }
  }
}
