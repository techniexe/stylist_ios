//
//  PortfolioVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 11/02/21.
//

import UIKit
import XLPagerTabStrip

class PortfolioVC: UIViewController, IndicatorInfoProvider {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var portfolioCollectionView: UICollectionView!
  @IBOutlet weak var noDataView: UIView!
  
  //MARK:- Scroll Delegate
  weak var innerTableViewScrollDelegate: InnerTableViewScrollDelegate?
  var arrPortFolioList: [PortfolioInfo] = []
  private var dragDirection: DragDirection = .Up
  private var oldContentOffset = CGPoint.zero
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    initializeOnce()
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    callGetPortFolioAPI()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.portfolioCollectionView.layoutIfNeeded()
  }
  
  // MARK: - IndicatorInfoProvider
  func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return "Portfolio"
  }
  
  
  // MARK:- Initialize Once
  func initializeOnce() {
    self.portfolioCollectionView.backgroundColor = UIColor.white
    self.portfolioCollectionView.register(UINib(nibName: Constants.CollectionViewCell.PortFolioCollectionCell, bundle: nil), forCellWithReuseIdentifier: Constants.CollectionViewCellIdentifier.PortFolioCollectionCellID)
    self.portfolioCollectionView.register(UINib(nibName: Constants.CollectionViewCell.NoPortFolioCell, bundle: nil), forCellWithReuseIdentifier: Constants.CollectionViewCellIdentifier.NoPortFolioCellID)
    Helper.showLoading(self.activityIndicator)
    addFooterView()
  }
  
  // MARK: - Add Circulr View On Footer Of Tableview
  func addFooterView() {
    
    portfolioCollectionView.ptr.footerHeight = 40
    //portfolioCollectionView.ptr.footerView = getProgressCircle()
    portfolioCollectionView.ptr.footerCallback = { [weak self] in
      
      if self?.arrPortFolioList.count != 0 {
        
        if let strCreatedAt = self?.arrPortFolioList.last?.createdAt {
          
          if let dateFromString = strCreatedAt.dateFromISO8601 {
           
            let beforeTime = dateFromString.iso8601
            
            DispatchQueue.global(qos: .default).async(execute: {
            
  
              APIHandler.shared.getStylistPortfolio(queryParams: ["before": beforeTime]) { [self] (statuscode, dataResponse, error) in
                
                if dataResponse != nil {
                  
                  if let infoArray = dataResponse?.portfolio {
                    
                    if infoArray.count > 0 {
                  
                      self?.portfolioCollectionView.ptr.isLoadingFooter = false
                      
                      var arrIndexPath: [IndexPath] = []
                      
                      for element in infoArray {
                        arrIndexPath.append(IndexPath(item: self?.arrPortFolioList.count ?? 0, section: 0))
                        self?.arrPortFolioList.append(element)
                        
                      }
                      
                      DispatchQueue.main.async {
                        self?.portfolioCollectionView.performBatchUpdates({
                          self?.portfolioCollectionView.insertItems(at: arrIndexPath)
                        }, completion: { (completed) in
                        })
                      }
                
                      
                    }else {
                      
                      self?.hideFooterView()
                    }
                    
                  }else {
                    self?.hideFooterView()
                  }
                  
                }else {
                  self?.hideFooterView()
                }
                
              }
              
              
            })
            
          }
          
        }else {
          self?.hideFooterView()
        }
        
      }else{
        self?.hideFooterView()
      }
      
    }
  
  }
  
  // MARK:- Hide FooterView
  func hideFooterView() {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
      self.portfolioCollectionView.ptr.isLoadingFooter = false
    }
  }
  
  // MARK:- Call Get PortFolio API
  func callGetPortFolioAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      APIHandler.shared.getStylistPortfolio(queryParams: nil) { (statuscode, response, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        guard error == nil else {
          self.updateUI()
          return
        }
        
        self.arrPortFolioList = []
        
        if let info = response?.portfolio {
          
          for element in info {
            self.arrPortFolioList.append(element)
          }
          
        }
        
        self.updateUI()
        
      }
      
    }else {
      Helper.hideLoading(self.activityIndicator)
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - Update UI
  func updateUI() {
    
    self.portfolioCollectionView.scrollIndicatorInsets = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 20.0, right: 0.0)
    self.portfolioCollectionView.delegate = self
    self.portfolioCollectionView.dataSource = self
    self.portfolioCollectionView.reloadData()
    
  }
  
}

// MARK: -  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension PortfolioVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
    if self.arrPortFolioList.count == 0 {
      return 1
    }else {
      return self.arrPortFolioList.count
    }
    
    
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    if self.arrPortFolioList.count == 0 {
      
      guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewCellIdentifier.NoPortFolioCellID, for: indexPath) as?  NoPortFolioCell else {
        return UICollectionViewCell()
      }
      
      return cell
      
    }else {
      
      guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewCellIdentifier.PortFolioCollectionCellID, for: indexPath) as?  PortFolioCollectionCell else {
        return UICollectionViewCell()
      }
      
      cell.configureCell(self.arrPortFolioList[indexPath.item])
      
      return cell
    }
    
  
    
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
  
    if self.arrPortFolioList.count == 0 {
      
      return CGSize(width:collectionView.frame.width, height: 120.0)
      
    }else {
      let width = ((collectionView.frame.width-64)/3)
      let height = ((collectionView.frame.width-64)/3)
      return CGSize(width:width, height: height)
    }
    
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets.init(top: 20, left: 20, bottom: 20, right: 20) //TLBR
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    if self.arrPortFolioList.count == 0 {
      return 0
    }else {
      return 12
    }
    
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    if self.arrPortFolioList.count == 0 {
      return 0
    }else {
      return 12
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    if self.arrPortFolioList.count == 0 {
      return
    }else {
      
      if let objDeletePortfolioVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "DeletePortfolioVC") as? DeletePortfolioVC {
        
        
        objDeletePortfolioVC.dicPortFolio = self.arrPortFolioList[indexPath.item]
        objDeletePortfolioVC.strProfilePic = nil
        if let navVC = self.parent?.parent?.parent?.parent as? UINavigationController {
          navVC.pushViewController(objDeletePortfolioVC, animated: true)
        }
        
        /*if let pgSideMenuVC = self.parent?.parent?.parent?.parent?.parent as? PGSideMenu {
         objDeletePortfolioVC.modalPresentationStyle = .overCurrentContext
         objDeletePortfolioVC.modalTransitionStyle = .coverVertical
         pgSideMenuVC.present(objDeletePortfolioVC, animated: true, completion: nil)
         }
         
         if let navVC = self.parent?.parent?.parent as? UINavigationController {
         navVC.pushViewController(objEditProfileVC, animated: true)
         }*/
      }
    }
  }
  
  
  
  
  /*func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
   UIView.animate(withDuration: 0.5) {
   if let cell = collectionView.cellForItem(at: indexPath) as? MediaListCell {
   cell.imageView.transform = .init(scaleX: 0.95, y: 0.95)
   cell.contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
   }
   }
   }
   
   func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
   UIView.animate(withDuration: 0.5) {
   if let cell = collectionView.cellForItem(at: indexPath) as? MediaListCell {
   cell.imageView.transform = .identity
   cell.contentView.backgroundColor = .clear
   
   }
   }
   }*/
}

extension PortfolioVC: UIScrollViewDelegate {
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
    let delta = scrollView.contentOffset.y - oldContentOffset.y
    
    let topViewCurrentHeightConst = innerTableViewScrollDelegate?.currentHeaderHeight
    
    if let topViewUnwrappedHeight = topViewCurrentHeightConst {
      
      /**
       *  Re-size (Shrink) the top view only when the conditions meet:-
       *  1. The current offset of the table view should be greater than the previous offset indicating an upward scroll.
       *  2. The top view's height should be within its minimum height.
       *  3. Optional - Collapse the header view only when the table view's edge is below the above view - This case will occur if you are using Step 2 of the next condition and have a refresh control in the table view.
       */
      
      if delta > 0,
         topViewUnwrappedHeight > topViewHeightConstraintRange.lowerBound,
         scrollView.contentOffset.y > 0 {
        
        dragDirection = .Up
        innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
        //print("contentOffset: \(scrollView.contentOffset.y)")
        scrollView.contentOffset.y -= delta
      }
      
      /**
       *  Re-size (Expand) the top view only when the conditions meet:-
       *  1. The current offset of the table view should be lesser than the previous offset indicating an downward scroll.
       *  2. Optional - The top view's height should be within its maximum height. Skipping this step will give a bouncy effect. Note that you need to write extra code in the outer view controller to bring back the view to the maximum possible height.
       *  3. Expand the header view only when the table view's edge is below the header view, else the table view should first scroll till it's offset is 0 and only then the header should expand.
       */
      
      if delta < 0,
         // topViewUnwrappedHeight < topViewHeightConstraintRange.upperBound,
         scrollView.contentOffset.y < 0 {
        
        dragDirection = .Down
        innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
        scrollView.contentOffset.y -= delta
      }
    }
    
    oldContentOffset = scrollView.contentOffset
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if scrollView.contentOffset.y <= 0 {
      
      innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }else {
      
      // UITableView only moves in one direction, y axis
      /* let currentOffset = scrollView.contentOffset.y
       let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
       
       //      Helper.DLog(message: "currentOffset:::\(currentOffset)")
       //      Helper.DLog(message: "contentSize:::\(scrollView.contentSize.height)")
       //      Helper.DLog(message: "frameSize:::\(scrollView.frame.height)")
       //      Helper.DLog(message: "maximumOffset:::\(maximumOffset)")
       
       // Change 10.0 to adjust the distance from bottom
       if maximumOffset - currentOffset <= 10.0 {
       self.reviewTblView.ptr.isLoadingFooter = true
       self.loadMoreWithPagination()
       }*/
      
    }
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if decelerate == false && scrollView.contentOffset.y <= 0 {
      
      innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }
  }
}
