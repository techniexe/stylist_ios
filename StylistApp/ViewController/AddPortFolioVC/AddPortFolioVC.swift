//
//  AddPortFolioVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 17/02/21.
//

import UIKit
import SwiftGifOrigin

class AddPortFolioVC: UIViewController {
  
  // MARK:- @IBOutlets
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var indicatorheightConstraint: NSLayoutConstraint!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var heightConstraint: NSLayoutConstraint!
  @IBOutlet weak var imgUpload: UIImageView!
  @IBOutlet weak var btnClose: UIButton!
  
  // MARK:- @vars
  var arrPhotoList: [UploadDocument] = []
  var arrImages: [Image] = []
  var gallery: GalleryController!
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView() {
    
    addTapGestureOnImage()
    self.collectionView.backgroundColor = UIColor.white
    self.collectionView.register(UINib(nibName: Constants.CollectionViewCell.AddPortFolioCollectionCell, bundle: nil), forCellWithReuseIdentifier: Constants.CollectionViewCellIdentifier.AddPortFolioCollectionCellID)
    updateUI()
  }
  
  // MARK:- Add Tap Gesture on Upload Icon
  func addTapGestureOnImage() {
    
    let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(uploadImageTouched))
    tapRecognizer.numberOfTapsRequired = 1
    tapRecognizer.numberOfTouchesRequired = 1
    self.imgUpload.addGestureRecognizer(tapRecognizer)
    self.imgUpload.isUserInteractionEnabled = true
    
  }
  
  // MARK: - @IBAction
  @IBAction func actionClose(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  
  // MARK: - Update UI
  func updateUI() {
  
    self.collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    self.collectionView.delegate = self
    self.collectionView.dataSource = self
    self.collectionView.reloadData()
    
    self.collectionView.layoutIfNeeded()
    heightConstraint.constant = self.collectionView.contentSize.height
    
  }
 
  // MARK:- Remove From CollectionView
  @objc func actionRemove(_ sender: UIButton) {
    
    self.arrImages.remove(at: sender.tag - 1)
    self.arrPhotoList.remove(at: sender.tag-1)
    //self.collectionView.deleteItems(at: [IndexPath(item: sender.tag, section: 0)])
    self.collectionView.reloadData()
    self.collectionView.layoutIfNeeded()
    self.heightConstraint.constant = self.collectionView.contentSize.height 
    
  }
  
  
  @objc func uploadImageTouched(_ recognizer: UITapGestureRecognizer) {
    Helper.DLog(message: "Touch Uploading Icon")
  }
  
  // MARK: - @IBActions
  @IBAction func actionUpload(_ sender: Any) {
    
    if self.arrPhotoList.count == 0 {
      
      Helper.showToast(_strMessage: ErrorMessage.addMedia.message())
      
      return
      
    }else {
      
      if Reachability.isConnectedToNetwork() {
       
        self.view.isUserInteractionEnabled = false
        self.imgUpload.loadGif(asset: "ic_upload")
        self.imgUpload.isHidden = false
        self.btnClose.isHidden = true
        
        /*view.layoutIfNeeded() // force any pending operations to finish

        UIView.animate(withDuration: 0.2, animations: { () -> Void in
          self.indicatorheightConstraint.constant = 40.0
          self.view.layoutIfNeeded()
        })

        Helper.showLoading(self.activityIndicator)*/
        
        APIHandler.shared.addPortFolio(self.arrPhotoList) { (statuscode, error) in
          
          self.view.isUserInteractionEnabled = true
          self.imgUpload.isHidden = true
          self.btnClose.isHidden = false
          
          /*self.view.layoutIfNeeded() // force any pending operations to finish

          UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.indicatorheightConstraint.constant = 0.0
            self.view.layoutIfNeeded()
          })
          
          Helper.hideLoading(self.activityIndicator)*/
          
          if statuscode == 204 {
            
            Helper.showToast(_strMessage: SuccessMessage.portfolioPicSuccess.message())
            self.navigationController?.popViewController(animated: true)
            
          }else {
            Helper.showToast(_strMessage: error)
          }
          
        }
        
      }else{
        Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
      }
    }
  }
  
}


// MARK: -  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension AddPortFolioVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
    if self.arrPhotoList.count == 0 {
     
      return 1
    }
    
    return self.arrPhotoList.count + 1
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewCellIdentifier.AddPortFolioCollectionCellID, for: indexPath) as?  AddPortFolioCollectionCell else {
      return UICollectionViewCell()
    }
    
    if indexPath.item == 0 {
      
      cell.imgView.image = UIImage(named: "ic_cam")
      cell.btnRemove.isHidden = true
      
    }else {
      cell.btnRemove.isHidden = false
      cell.imgView.image = self.arrPhotoList[indexPath.item - 1].img
    }
    
    cell.btnRemove.tag = indexPath.item
    
    cell.btnRemove.addTarget(self, action: #selector(self.actionRemove(_:)), for: .touchUpInside)
    
    return cell
    
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
  
    let width = ((collectionView.frame.width-44)/3)
    let height = ((collectionView.frame.width-44)/3)
    return CGSize(width:width, height: height)
    
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 20.0, left: 15.0, bottom: 0.0, right: 15.0)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 7
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 7
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    Helper.DLog(message: "\(indexPath.item)")
    
    if indexPath.item == 0 {
      
      if self.arrPhotoList.count == 5 {
        Helper.showToast(_strMessage: ErrorMessage.medialimit.message())
        return
      }
      
      gallery = GalleryController()
      gallery.delegate = self
      Config.tabsToShow = [.imageTab, .cameraTab]
    
      
      gallery.cart.images = self.arrImages
      present(gallery, animated: true, completion: nil)
      
      
    }else{
      return
    }
    
  }
}
 
// MARK:- GalleryControllerDelegate
extension AddPortFolioVC: GalleryControllerDelegate {
  
  func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
    controller.dismiss(animated: true, completion: nil)
    Helper.DLog(message: "\(images.count)")

    self.arrPhotoList = []
    self.arrImages = []
    
    for i in 0..<images.count {
      let objImage = images[i]
      self.arrImages.append(objImage)
    }
    
    Image.resolve(images: images, completion: { [weak self] resolvedImages in
      //SVProgressHUD.dismiss()
      //self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
      
      
      for i in 0..<resolvedImages.count {
        
       
        let imgData = resolvedImages[i]?.jpegData(compressionQuality: 1.0)
        self?.arrPhotoList.append(UploadDocument(title: "Title", img: resolvedImages[i], data: imgData, name: "", fileName: "portfolio.jpeg"))
        
      }
      
      self?.collectionView.reloadData()
      self?.collectionView.layoutIfNeeded()
      self?.heightConstraint.constant = self?.collectionView.contentSize.height ?? 0
    })
    
  }
  
  func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
    controller.dismiss(animated: true, completion: nil)
    Helper.DLog(message: "\(video)")
  }
  
  func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
   
//    LightboxConfig.DeleteButton.enabled = true
//    Helper.DLog(message: "\(images.count)")
//    Image.resolve(images: images, completion: { [weak self] resolvedImages in
//      //SVProgressHUD.dismiss()
//      self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
//    })
  }
  
  func galleryControllerDidCancel(_ controller: GalleryController) {
    controller.dismiss(animated: true, completion: nil)
    gallery = nil
  }
  
//  func lightboxControllerWillDismiss(_ controller: LightboxController) {
//    //controller.dismiss(animated: true, completion: nil)
//  }
//  
  // MARK: - Helper

  /*func showLightbox(images: [UIImage]) {
    guard images.count > 0 else {
      return
    }

    let lightboxImages = images.map({ LightboxImage(image: $0) })
    let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
    lightbox.dismissalDelegate = self

    gallery.present(lightbox, animated: true, completion: nil)
  }*/
}
