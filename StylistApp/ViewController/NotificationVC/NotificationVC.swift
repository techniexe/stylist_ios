//
//  NotificationVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 09/02/21.
//

import UIKit

class NotificationVC: UIViewController {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var noDataView: UIView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  
  // MARK: - @vars
  var arrNotificationList: [NotificationData] = []
  var currentIndexPath: IndexPath? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    setupView()
  }
  
  
  // MARK: - setupView
  func setupView() {
    //self.navigationController?.setNavigationBarHidden(true, animated: false)
    self.tblView.backgroundColor = .white
    self.tblView.estimatedRowHeight = 44
    self.tblView.rowHeight = UITableView.automaticDimension
    self.tblView.register(UINib(nibName: Constants.TableViewCell.NotificationCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.NotificationCellID)
    callNotificationListAPI()
    //self.tblView.reloadData()
  }
  
  
  //MARK:- Call Notification List API
  func callNotificationListAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.shared.getStylistNotifications(queryParams: nil) { (statuscode, response, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        self.arrNotificationList = []
        
        guard error == nil else {
          self.noDataView.isHidden = false
          return
        }
        
        
        if let dataResponse = response?.data {
          
          if let arrNotif = dataResponse.notifications {
           
            self.noDataView.isHidden = true
            
            for (index, element) in arrNotif.enumerated() {
              self.arrNotificationList.insert(element, at: index)
            }
            
            if self.arrNotificationList.count == 0 {
              self.noDataView.isHidden = false
            }else {
              self.noDataView.isHidden = true
            }
            
            DispatchQueue.main.async {
              self.tblView.reloadData()
            }
            
            
          }
          
        }else {
          self.noDataView.isHidden = false
        }
        
      }
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - @IBActions
  @IBAction func actionBack(_ sender: Any) {
    //self.dismiss(animated: true, completion: nil)
    self.navigationController?.popViewController(animated: true)
  }
  
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
 
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.arrNotificationList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.NotificationCellID, for: indexPath) as? NotificationCell else {
      return UITableViewCell()
    }
    
    /*if let _ = self.arrNotificationList[indexPath.row].seen_on {
      cell.contentView.backgroundColor = UIColor.white
    }else {
      cell.contentView.backgroundColor = Constants.Colors.opacityColor
    }
    */
    cell.contentView.backgroundColor = UIColor.white
    cell.lblMessage.tag = indexPath.row
    cell.configureCell(self.arrNotificationList[indexPath.row])
    cell.delegate = self
    return cell
    
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 44.0
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    Helper.DLog(message: "\(indexPath.row)")
  }
  
  /*func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 50
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return "User Review \(section)"
  }*/
  
}

//MARK:- NotificationCellDelegate
extension NotificationVC: NotificationCellDelegate {
  func didPressOnUserName(_ strUserName: String?, tag: Int) {
    Helper.DLog(message: "\((strUserName)!)- \(tag)")
  }
}


extension NotificationVC {
  
  //MARK:- Add Circulr View On Footer Of Tableview
  func addFooterView() {
    
    self.tblView.ptr.footerCallback = { [weak self] in
      
      if self?.arrNotificationList.count != 0 {
        
        if let strCreatedAt = self?.arrNotificationList.last?.created_at {
          
          if let dateFromString = strCreatedAt.dateFromISO8601 {
            
            let beforeTime = dateFromString.iso8601
            
            DispatchQueue.global(qos: .default).async(execute: { [self] in
              
              APIHandler.shared.getStylistNotifications(queryParams: ["before": beforeTime]) { (statuscode, response, error) in
                
                if let dataResponse = response?.data {
                  
                  if let list = dataResponse.notifications {
                    
                    if list.count > 0 {
                      
                      self?.tblView.ptr.isLoadingFooter = false
                      
                      var arrIndexPath: [IndexPath] = []
                      
                      for (index, element) in list.enumerated() {
                        arrIndexPath.insert(IndexPath(row: self?.arrNotificationList.count ?? 0, section: 0), at: index)
                        self?.arrNotificationList.insert(element, at: (self?.arrNotificationList.count)!)
                        
                      }
                      
                      DispatchQueue.main.async {
                        self?.tblView.performBatchUpdates({
                          self?.tblView.insertRows(at: arrIndexPath, with: .none)
                        }, completion: { (completed) in
                          
                        })
                      }
                      
                    }else {
                      self?.hideFooterView()
                    }
                    
                  }else {
                    self?.hideFooterView()
                  }
                  
                }else {
                  self?.hideFooterView()
                }
                
              }
              
             
              
            })
            
          }
          
        }else {
          self?.hideFooterView()
        }
        
        
        
      }else{
        self?.hideFooterView()
      }
      
    }
    
  }
  
  // MARK:- Hide FooterView
  func hideFooterView() {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
      self.tblView.ptr.isLoadingFooter = false
    }
  }
  
}
