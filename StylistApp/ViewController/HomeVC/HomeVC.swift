//
//  HomeVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 25/01/21.
//

import UIKit

class HomeVC: UIViewController {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var detailView: UIView!
  @IBOutlet weak var lblTotalAppointment: UILabel!
  @IBOutlet weak var lblCompleteAppointment: UILabel!
  @IBOutlet weak var lblCancelAppointment: UILabel!
  @IBOutlet weak var lblTotalLeave: UILabel!
  @IBOutlet weak var tblReview: UITableView!
  @IBOutlet weak var noReview: UIView!
  @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var tblViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var notificationView: CustomView!
  @IBOutlet weak var lblNotification: UILabel!
  
  // MARK: - @vars
  var dicDashBoardDetails: DashBoardDetails? = nil
  var arrReviewList: [ReviewDetails] = []
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    initializeOnce()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    getNotificationCount()
    callGetDashBoardDetailsAPI()
  }
  
  // MARK: - initializeOnce
  func initializeOnce() {
    
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    self.detailView.isHidden = true
    self.tblReview.backgroundColor = UIColor.white
    self.tblReview.register(UINib(nibName: Constants.TableViewCell.ReviewCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.ReviewCellID)
    self.tblReview.register(UINib(nibName: Constants.TableViewCell.NoReviewCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.NoReviewCellID)
    Helper.showLoading(self.activityIndicator)
  }
  
  // MARK: - @IBActions
  @IBAction func actionLeftMenu(_ sender: Any) {
    if let sideMenuController = self.parent?.parent?.parent?.parent as? PGSideMenu {
     
      if let sidemneuVC = sideMenuController.leftMenuController?.children[0] as? SideMenuVC {
        sidemneuVC.updateUI()
      }
      
      sideMenuController.toggleLeftMenu()
    }
  }
  
  @IBAction func actionNotification(_ sender: Any) {
    
    if let objNotificationVC = Constants.kHomeStoryBoard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC {
      
      if let navVC = self.parent?.parent?.parent as? UINavigationController {
        navVC.pushViewController(objNotificationVC, animated: true)
      }
      
      /*objNotificationVC.modalTransitionStyle = .coverVertical
      objNotificationVC.modalPresentationStyle = .fullScreen
      self.present(objNotificationVC, animated: true, completion: nil)*/
      
      /*if let parentNavVC = self.parent?.parent?.parent?.parent as? PGSideMenu{
        let navVC = UINavigationController(rootViewController: objNotificationVC)
        navVC.modalTransitionStyle = .coverVertical
        navVC.modalPresentationStyle = .fullScreen
        parentNavVC.present(navVC, animated: true, completion: nil)
      }*/

    }
    
  }
  
  @IBAction func actionHeader(_ sender: Any) {
    
  }
  
  // MARK: - GET DASHBOARD DETAILS
  func callGetDashBoardDetailsAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      APIHandler.shared.getDashBoardDetails { (statuscode, response, error) in
        
        guard error == nil else {
          Helper.hideLoading(self.activityIndicator)
          Helper.showToast(_strMessage: error)
          self.updateUI()
          return
        }
        
        if let dashBoardDetails = response?.data {
          self.dicDashBoardDetails = dashBoardDetails
        }
        
        self.callGetReviewAPI()
        
      }
      
    }else {
      Helper.hideLoading(self.activityIndicator)
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  
  // MARK: - GET REVIEW
  func callGetReviewAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      APIHandler.shared.getReviewList(queryParams: nil) { (statuscode, response, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        guard error == nil else {
          self.updateUI()
          return
        }
        
        self.arrReviewList = []
        
        if let arrReview = response?.data {
          
          for review in arrReview {
            self.arrReviewList.append(review)
          }
          
        }
        
        self.updateUI()
        
      }
      
    }else {
      Helper.hideLoading(self.activityIndicator)
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK: - Update UI
  func updateUI() {
    
    if let dashBoardDetails = self.dicDashBoardDetails {
      
      if let totalAppointment = dashBoardDetails.totalAppointment {
        
        self.lblTotalAppointment.text = Helper.getStringFromDouble(NSNumber(value: totalAppointment))
      }
      
      if let completedAppointment = dashBoardDetails.completedAppointment {
        
        self.lblCompleteAppointment.text = Helper.getStringFromDouble(NSNumber(value: completedAppointment))
      }
      
      if let cancelAppointment = dashBoardDetails.cancelledAppointment {
        
        self.lblCancelAppointment.text = Helper.getStringFromDouble(NSNumber(value: cancelAppointment))
      }
      
      if let totalLeaves = dashBoardDetails.totalLeaves {
        
        self.lblTotalLeave.text = Helper.getStringFromDouble(NSNumber(value: totalLeaves))
      }
      
    }
    
    guard self.arrReviewList.count > 0 else {
      self.detailView.isHidden = false
      //noReview.isHidden = false
      
      
      self.tblReview.delegate = self
      self.tblReview.dataSource = self
      self.tblReview.isHidden = false
      self.tblReview.reloadData()
      
      self.tblReview.layoutIfNeeded()
      tblViewHeightConstraint.constant = self.tblReview.contentSize.height
      return
    }
    
    self.tblReview.delegate = self
    self.tblReview.dataSource = self
    self.tblReview.isHidden = false
    self.tblReview.reloadData()
    self.detailView.isHidden = false
    self.tblReview.layoutIfNeeded()
    //viewHeightConstraint.constant = self.tblReview.contentSize.height
    tblViewHeightConstraint.constant = self.tblReview.contentSize.height

  }
}


// MARK: - UITableViewDelegate, UITableViewDataSource
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if self.arrReviewList.count == 0 {
      return 1
    }
    
    return self.arrReviewList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    if self.arrReviewList.count == 0 {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.NoReviewCellID, for: indexPath) as? NoReviewCell else {
        return UITableViewCell()
      }
      
      return objCell
      
    }else {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.ReviewCellID, for: indexPath) as? ReviewCell else {
        return UITableViewCell()
      }
      
//      objCell.lblName.text = "Paras Modi"
//      objCell.lblReviewText.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard."
//      objCell.lblCreatedDate.text = "2 days ago"
//      objCell.ratingView.rating = 5.0
      
      if indexPath.row == self.arrReviewList.count - 1 {
        objCell.bottomLine.isHidden = true
      }else {
        objCell.bottomLine.isHidden = false
      }
      
      
      objCell.configureCell(self.arrReviewList[indexPath.row])
      
      return objCell
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //return 20
    
    if self.arrReviewList.count == 0 {
      return 0
    }
    
    return 30
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if self.arrReviewList.count == 0 {
      let view = UIView()
      view.backgroundColor = UIColor.clear
      return view
    }
    
    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tblReview.bounds.size.width, height: 50.0))
    view.backgroundColor = UIColor.white
    
    let label = UILabel(frame: CGRect(x: 10, y: 10, width: view.frame.size.width - 20.0, height: 20.0))
    label.text = "User Review"
    label.textColor = UIColor.black
    label.font = MainFont.bold.with(size: 17.0)  //UIFont(name: "Roboto-Bold", size: 17.0)
    view.addSubview(label)
    return view
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView()
    view.backgroundColor = UIColor.clear
    return view
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0
  }
  

  
}

extension HomeVC {
  
  
  // MARK: - Get Notification Count
  func getNotificationCount() {
    
    GlobalFunction.checkLoggedInUser { (isLogin) in
      
      if isLogin == true {
        
        if Reachability.isConnectedToNetwork() {
          
        APIHandler.shared.getStylistNotificationCount { (statuscode, response, error) in
        
          if let dataResponse = response?.data {
          
            if let notificationCount = dataResponse.notification_count {
              
              if notificationCount == 0 {
                self.notificationView.isHidden = true
   
              }else {
                self.notificationView.isHidden = false
                self.lblNotification.text = "\(notificationCount)"

              }
              
            }else {
              self.notificationView.isHidden = true
            }
            
          }else {
            self.notificationView.isHidden = true
          }
          
        }
          
        }else {
          self.notificationView.isHidden = true
        }
      }else {
        self.notificationView.isHidden = true
      }
      
    }
    
  }
}
