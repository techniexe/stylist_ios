//
//  DeletePortfolioVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 19/02/21.
//

import UIKit
import SDWebImage

class DeletePortfolioVC: UIViewController {
  
  // MARk: - @IBOutlets
  @IBOutlet weak var zoomImgView: EEZoomableImageView!
  @IBOutlet weak var btnDelete: UIButton!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var progressView: MKMagneticProgress!
  
  // MARK:- @vars
  var dicPortFolio: PortfolioInfo? = nil
  var strProfilePic: String? = nil
  
  // MARK:- View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setup View
  func setupView() {
    
    if Reachability.isConnectedToNetwork() == true {
      
      if let portFolioInfo = dicPortFolio {
        
        if let url = portFolioInfo.url {
          self.downloadImage(url)
        }
        
        
        
      }else {
        
        if let strUrl = strProfilePic {
          self.downloadImage(strUrl)
        }
        
      }
      
     
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  func downloadImage (_ url: String) {
    
    self.progressView.isHidden = false
    
    SDWebImageManager.shared.loadImage(with: URL(string: (url)), options: [.highPriority], progress: { (receivedSize, expectedSize,url) in
      
      let uploadProgress: Double = Double(receivedSize) / Double(expectedSize)
      
      DispatchQueue.main.async {
        self.progressView.setProgress(progress: CGFloat(uploadProgress), animated: true)
        
      }
      
    }) { (image, data, error, cacheType, finished, url) in
      if finished == true {
        DispatchQueue.main.async {
          self.progressView.isHidden = true
          self.zoomImgView.image = image
          
          if let _ = self.strProfilePic {
            self.btnDelete.isHidden = true
          }else {
            self.btnDelete.isHidden = false
          }
          
         
        }
      }
    }
    
  }
  
  // MARK: - @IBActions
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func actionDelete(_ sender: Any) {
  
    let alertController = UIAlertController(title: nil, message: Constants.CommonAlert.deletePortFolio , preferredStyle: .alert)
    
    alertController.setMessage(font: MainFont.medium.with(size: 16.0), color: UIColor.black)
    
    let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: {
      (alert: UIAlertAction!) -> Void in
        self.deletePortFolio()
    })
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
      (alert: UIAlertAction!) -> Void in
      
    })
    
    
    //cancelAction.setValue(UIColor.black, forKey: "titleTextColor")
    
    alertController.addAction(deleteAction)
    alertController.addAction(cancelAction)
    
    if #available(iOS 13.0, *) {
      alertController.overrideUserInterfaceStyle = .light
    } else {
      // Fallback on earlier versions
    }
    
    alertController.pruneNegativeWidthConstraints()
    
    self.parent?.parent?.present(alertController, animated: true, completion: nil)
    
    
    /*let alertVC = GlobalFunction.openCustomAlert()
    alertVC.commonAlert = .deletePortFlio
    alertVC.customDelegate = self
    self.parent?.parent?.present(alertVC, animated: true, completion: nil)*/
  }
  
}

// MARK: - CustomDelegate
extension DeletePortfolioVC: CustomDelegate {
  
  func actionCustomAlertDialog(_ btn: UIButton) {
  
  }
  
}

extension DeletePortfolioVC {
  
  func deletePortFolio() {
    
    if Reachability.isConnectedToNetwork() {
      
      Helper.showLoading(self.activityIndicator)
      
      var arrList: [String] = []
      
      if let portFolioInfo = dicPortFolio {
        
        if let strPortFolioID = portFolioInfo._id {
          arrList.append(strPortFolioID)
        }
      }
      
      APIHandler.shared.deleteStylistPortFolio(bodyParams: ["portfolioIds" : arrList]) { (statuscode, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        if statuscode == 204 {
          
          Helper.showToast(_strMessage: SuccessMessage.portflioDeletedSuccess.message())
          self.navigationController?.popViewController(animated: true)
          
        }else {
          Helper.showToast(_strMessage: error)
        }
        
      }
      
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
}
