//
//  TabBarController.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 27/01/21.
//

import UIKit

class TabBarController: UITabBarController {

  let selectedColor   = Constants.Colors.colorPrimary ?? UIColor.red
  
  let unselectedColor = Constants.Colors.colorlightgray ?? UIColor.red
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      initializeOnce()
    }
  // MARK: - initializeOnce
  func initializeOnce() {
   
    if #available(iOS 13.0, *) {
      self.overrideUserInterfaceStyle = .light
    } else {
      // Fallback on earlier versions
    }
    
    //UITabBar.appearance().backgroundColor = UIColor.white
    
    self.delegate = self
    
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    
   /* UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 12.0)!, NSAttributedString.Key.foregroundColor: unselectedColor], for: .normal)
    UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 12.0)!, NSAttributedString.Key.foregroundColor: selectedColor], for: .selected)*/
    
    UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
    
    
    tabBar.layer.borderWidth = 1.0
    tabBar.layer.borderColor = Constants.Colors.colorPrimary?.cgColor
    tabBar.clipsToBounds = true
    self.setValue(tabBar, forKey: "tabBar")
    
    setTabBar()
  }

  // MARK: - Set Image On TabBar
  func setTabBar() {
    
    if let arrNavigationController = self.viewControllers as? [UINavigationController] {
      
      if let homeVC = arrNavigationController[0].viewControllers.first as? HomeVC {
        
        let customTabBarItem : UITabBarItem = UITabBarItem(title: "Dashboard", image: UIImage(named: "ic_dashboard")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "ic_dashboard_active")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        homeVC.tabBarItem = customTabBarItem
        
      }
      
      if let appointmentVC = arrNavigationController[1].viewControllers.first as? AppointmentVC {
        
        let customTabBarItem : UITabBarItem = UITabBarItem(title: "Appointment", image: UIImage(named: "ic_appointment")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "ic_appointment_active")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        
        appointmentVC.tabBarItem = customTabBarItem
        
      }
      
      if let profileVC = arrNavigationController[2].viewControllers.first as? ProfileVC {
        
        let customTabBarItem : UITabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "ic_myprofile")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "ic_myprofile_active")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        
        profileVC.tabBarItem = customTabBarItem
      }
      
      
    }
  }
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITabBarControllerDelegate
extension TabBarController: UITabBarControllerDelegate {

  
  override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    /*let indexOfTab = tabBar.items?.firstIndex(of: item)
    print("pressed tabBar: \(String(describing: indexOfTab))")*/
  }
  
  func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    
    Helper.DLog(message: "SelectedIndex:: \(tabBarController.selectedIndex)")
    
    /*if let navigationController = viewController as? UINavigationController {
      
      if let homeVC = navigationController.children[0] as? HomeVC {
        Helper.DLog(message: "HomeVC")
      }
      if let appointmentVC = viewController.children[0] as? AppointmentVC {
        Helper.DLog(message: "AppointmentVC")
      }
      if let profileVC = viewController.children[0] as? ProfileVC {
        Helper.DLog(message: "ProfileVC")
      }
      
    }
    */
      
  }
}
