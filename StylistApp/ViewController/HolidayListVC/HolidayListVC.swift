//
//  HolidayListVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 11/02/21.
//

import UIKit
import XLPagerTabStrip

class HolidayListVC: UIViewController, IndicatorInfoProvider {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var noDataView: UIView!
  
  //MARK:- Scroll Delegate
  weak var innerTableViewScrollDelegate: InnerTableViewScrollDelegate?
  // MARK: - @vars
  var arrHolidayList: [HolidayList] = []
  private var dragDirection: DragDirection = .Up
  private var oldContentOffset = CGPoint.zero
  var cardPopup: SBCardPopupViewController? = nil
  var strFullMonth: String = ""
  var strMonth: String = ""
  var year: Int = 0
  var queryParams: [String: Any]? = nil
  var isDateChange: Bool = false
  var selectedDate: Date!
  
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    initializeOnce()
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setCurrentMonth()
  }
  
  // MARK: - IndicatorInfoProvider
  func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return "Holidays"
  }
  
  // MARK: - initializeOnce
  func initializeOnce() {
    
    self.tblView.backgroundColor =  Constants.Colors.colorBackground
    self.tblView.register(UINib(nibName: Constants.TableViewCell.HolidayListCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.HolidayListCellID)
    self.tblView.register(UINib(nibName: Constants.TableViewCell.NoHolidayCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.NoHolidayCellID)
    self.tblView.register(UINib(nibName: Constants.TableViewCell.LeaveHeaderCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.LeaveHeaderCellID)
    Helper.showLoading(self.activityIndicator)
    addFooterView()
  }
  
  // MARK: - set Month and Year
  func setMonthAndYear(_ month: String, year: Int, isDateChange: Bool) {
   
    strFullMonth = month
    strMonth = strFullMonth.prefix(3).lowercased()
    self.year = year
    //selectedDate = date
    //strFullMonth = selectedDate.monthFull.capitalized
    //strMonth = selectedDate.monthMedium.lowercased()
    //year = Int(selectedDate.yearMedium) ?? 0
    self.isDateChange = isDateChange
    queryParams = ["month": strMonth, "year": year]
    getHolidayList(queryParams)
    
  }
  
  
  // MARK:- Call Get Stylist Leave List
  func callGetHolidayListAPI(_ queryParams: [String:Any]?, completion: @escaping(Int?, AppData<[HolidayList]>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
    
      APIHandler.shared.getHolidayList(queryParams: queryParams) { (statuscode, response, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        completion(statuscode,response,error)
      }
      
    }else {
      Helper.hideLoading(self.activityIndicator)
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  
  // MARK:- Get Leave List
  func getHolidayList(_ queryParams: [String:Any]?) {
    
    callGetHolidayListAPI(queryParams) { (statuscode, response, error) in
      
      guard error == nil else {
        self.updateUI()
        return
      }
      
      self.arrHolidayList = []
      
      if let arrHoliday = response?.data {
        
        for holiday in arrHoliday {
          self.arrHolidayList.append(holiday)
        }
        
      }
      
      self.updateUI()
    }
    
    
  }
  
  // MARK: - Update UI
  func updateUI() {
    
    //self.tblView.scrollIndicatorInsets = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 20.0, right: 0.0)
    //self.tblView.contentInset = UIEdgeInsets(top: 5.0, left: 0.0, bottom: 20.0, right: 0.0)
    self.tblView.rowHeight = UITableView.automaticDimension
    self.tblView.estimatedRowHeight = 44
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.allowsSelection = false
    self.tblView.reloadData()
    
  }
  
  // MARK;- Clear All
  @objc func actionClearAll(_ sender: UIButton) {
    
    setCurrentMonth()
  }
  
  // MARK:- Set Current Month and Year
  func setCurrentMonth() {
   
    strFullMonth = Date().monthFull.capitalized
    strMonth =  Date().monthMedium.lowercased()
    year = Int(Date().yearMedium) ?? 0
    
    strMonth = strFullMonth.prefix(3).lowercased()
    self.isDateChange = false
    queryParams = nil
    getHolidayList(nil)
    
    //setMonthAndYear(strFullMonth, year: year, isDateChange: false)
  }
  
  // MARK;- Date Change
  @objc func actionDateChange(_ sender: UIButton) {
    
    if let objApplyDateVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "ApplyDateVC") as? ApplyDateVC {
      
      objApplyDateVC.delegate = self
      objApplyDateVC.selectedMonth = strFullMonth
      objApplyDateVC.selectedYear = year
      
      self.cardPopup = nil
      self.cardPopup = SBCardPopupViewController(contentViewController: objApplyDateVC)
      self.cardPopup?.cornerRadius = 5
      self.cardPopup?.disableTapToDismiss = true
      self.cardPopup?.disableSwipeToDismiss = true
      if let pgSideMenuVC = self.parent?.parent?.parent?.parent?.parent as? PGSideMenu {
        self.cardPopup?.show(onViewController: pgSideMenuVC)
      }
      
    }
    
  }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension HolidayListVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if self.arrHolidayList.count == 0 {
     return 2
    }
    
    return self.arrHolidayList.count + 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
    return 1
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.section == 0 {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.LeaveHeaderCellID, for: indexPath) as? LeaveHeaderCell else {
        return UITableViewCell()
      }
      
      objCell.btnClearAll.addTarget(self, action: #selector(self.actionClearAll(_:)), for: .touchUpInside)
      
      objCell.btnDateChage.addTarget(self, action: #selector(self.actionDateChange(_:)), for: .touchUpInside)
      
      objCell.txtDate.text = "\(strFullMonth) - \(String(year))"
      
      if self.isDateChange == true {
        
        objCell.btnClearAll.isHidden = false
        
        objCell.btnDateChage.isHidden = true
        
      }else {
        
        objCell.btnClearAll.isHidden = true
        
        objCell.btnDateChage.isHidden = false
      }
      
      
      
      return objCell
      
    }else {
      
      if self.arrHolidayList.count == 0 {
        
        guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.NoHolidayCellID, for: indexPath) as? NoHolidayCell else {
          return UITableViewCell()
        }
        
        return objCell
        
      }else {
        
        guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.HolidayListCellID, for: indexPath) as? HolidayListCell else {
          return UITableViewCell()
        }
        
        objCell.configureCell(self.arrHolidayList[indexPath.section - 1])
        
        return objCell
        
      }
      
    }
    
  }
  
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let view = UIView()
    view.backgroundColor = UIColor.clear
    return view
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    
    if section == 0 {
      let view = UIView()
      view.backgroundColor = UIColor.clear
      return view
    }else {
      let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.tblView.bounds.size.width, height: 20.0))
      view.backgroundColor = Constants.Colors.colorBackground
      return view
    }
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    
    if section == 0 {
      return 0
    }else {
      return 20
    }
  }
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    return
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
    let delta = scrollView.contentOffset.y - oldContentOffset.y
    
    let topViewCurrentHeightConst = innerTableViewScrollDelegate?.currentHeaderHeight
    
    if let topViewUnwrappedHeight = topViewCurrentHeightConst {
      
      /**
       *  Re-size (Shrink) the top view only when the conditions meet:-
       *  1. The current offset of the table view should be greater than the previous offset indicating an upward scroll.
       *  2. The top view's height should be within its minimum height.
       *  3. Optional - Collapse the header view only when the table view's edge is below the above view - This case will occur if you are using Step 2 of the next condition and have a refresh control in the table view.
       */
      
      if delta > 0,
         topViewUnwrappedHeight > topViewHeightConstraintRange.lowerBound,
         scrollView.contentOffset.y > 0 {
        
        dragDirection = .Up
        innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
        //print("contentOffset: \(scrollView.contentOffset.y)")
        scrollView.contentOffset.y -= delta
      }
      
      /**
       *  Re-size (Expand) the top view only when the conditions meet:-
       *  1. The current offset of the table view should be lesser than the previous offset indicating an downward scroll.
       *  2. Optional - The top view's height should be within its maximum height. Skipping this step will give a bouncy effect. Note that you need to write extra code in the outer view controller to bring back the view to the maximum possible height.
       *  3. Expand the header view only when the table view's edge is below the header view, else the table view should first scroll till it's offset is 0 and only then the header should expand.
       */
      
      if delta < 0,
         // topViewUnwrappedHeight < topViewHeightConstraintRange.upperBound,
         scrollView.contentOffset.y < 0 {
        
        dragDirection = .Down
        innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
        scrollView.contentOffset.y -= delta
      }
    }
    
    oldContentOffset = scrollView.contentOffset
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if scrollView.contentOffset.y <= 0 {
      
      innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if decelerate == false && scrollView.contentOffset.y <= 0 {
      
      innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }
  }
}


extension HolidayListVC {
  
  // MARK: - Add Circulr View On Footer Of Tableview
  func addFooterView() {
   
    tblView.ptr.footerHeight = 40
    //tblView.ptr.footerView = getProgressCircle()
    
    self.tblView.ptr.footerCallback = { [weak self] in
      
      if self?.arrHolidayList.count != 0 {
        
        if let strCreatedAt = self?.arrHolidayList.last?.createdAt {
          
          if let dateFromString = strCreatedAt.dateFromISO8601 {
            
            let beforeTime = dateFromString.iso8601
            
            DispatchQueue.global(qos: .default).async(execute: {
        
              
              var qryparams = [String:Any]()
              
              qryparams["before"] = beforeTime
              
              if let _ = self?.queryParams {
                
                qryparams["month"] = self?.strMonth
                
                qryparams["year"] = self?.year
                
              }
              
              
              
              self?.callGetHolidayListAPI(qryparams, completion: { (statuscode, dataResponse, error) in
                
                if dataResponse != nil {
                  
                  if let dataArray = dataResponse?.data {
                    
                    if dataArray.count > 0 {
                      
                      self?.tblView.ptr.isLoadingFooter = false
                      
                      var arrIndex: [Int] = []
                     
                      var index: Int = 0
                      
                      index = (self?.arrHolidayList.count)!
                      
                      for element in dataArray {
                        index += 1
                        arrIndex.append(index)
                        self?.arrHolidayList.append(element)
                      }
                      
                      DispatchQueue.main.async {
                        self?.tblView.performBatchUpdates({
                          self?.tblView.insertSections(IndexSet(arrIndex), with: .none)
                        }, completion: { (completed) in
                          
                        })
                      }
                      
                
                    }else {
                      
                      self?.hideFooterView()
                    }
                    
                  }else {
                    self?.hideFooterView()
                  }
                  
                }else {
                  self?.hideFooterView()
                }
                
              })
              
              
            })
            
          }
          
        }
          
        
      }else{
        self?.hideFooterView()
      }

    }
    
  }
  
  // MARK:- Hide FooterView
  func hideFooterView() {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
      self.tblView.ptr.isLoadingFooter = false
    }
  }
}

// MARK: - CustomDelegate
extension HolidayListVC: CustomDelegate {
  
  func applyDate(_ month: String, year: Int) {
    
    strFullMonth = month
    self.year = year
  
    setMonthAndYear(month, year: year, isDateChange: true)
  }
  
  
}
