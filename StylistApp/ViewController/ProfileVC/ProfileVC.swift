//
//  ProfileVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 25/01/21.
//

import UIKit
import XLPagerTabStrip
import CoreLocation

var topViewInitialHeight : CGFloat = 200

let topViewFinalHeight : CGFloat = 0 //UIApplication.shared.statusBarFrame.size.height + 144 //navigation hieght

let topViewHeightConstraintRange = topViewFinalHeight..<topViewInitialHeight

class ProfileVC: ButtonBarPagerTabStripViewController {
  
  // MARK: - IBOutlets
  @IBOutlet weak var stickyHeaderView: UIView!
  @IBOutlet weak var profileIcon: UIImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblDesignation: UILabel!
  @IBOutlet weak var tabBarCollectionView: UICollectionView!
  @IBOutlet weak var horizontalLineView: UIView!
  @IBOutlet weak var bottomView: UIView!
  @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var btnPlus: UIButton!
 
  // MARK: - @vars
  var tapIndex: Int = 0
  
  override func viewDidLoad() {
    // Do any additional setup after loading the view.
   
    setSettingsStyle()
    changeCurrentIndexProgressive()
    super.viewDidLoad()
    initializeOnce()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    updateHeaderView()
  }
  
  // MARK: - initializeOnce
  func initializeOnce() {
    
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    self.containerView.bounces = false
    self.containerView.isScrollEnabled = false
    /*setupCollectionView()
    setupPagingViewController()
    populateBottomView()
    addPanGestureToTopViewAndCollectionView()*/
    
    let tapGestureProfilePic = UITapGestureRecognizer(target: self, action: #selector(self.actionProfilePic(_:)))
    self.profileIcon.isUserInteractionEnabled = true
    self.profileIcon.addGestureRecognizer(tapGestureProfilePic)
    
    let tapGestureLocation = UITapGestureRecognizer(target: self, action: #selector(self.actionSalonLocation(_:)))
    self.lblDesignation.isUserInteractionEnabled = true
    self.lblDesignation.addGestureRecognizer(tapGestureLocation)
  }
  
  //MARK:- Tap Gesture on Profile Picture
  @objc func actionProfilePic(_ tapGesture: UITapGestureRecognizer) {
    
    if let objDeletePortfolioVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "DeletePortfolioVC") as? DeletePortfolioVC {
      
      
      objDeletePortfolioVC.dicPortFolio = nil
      
      if let dicStylist = GlobalFunction.getStylistDetails() {
        objDeletePortfolioVC.strProfilePic = dicStylist.profilePic
      }
      
      if let navVC = self.parent?.parent?.parent as? UINavigationController {
        navVC.pushViewController(objDeletePortfolioVC, animated: true)
      }
      
      /*if let pgSideMenuVC = self.parent?.parent?.parent?.parent?.parent as? PGSideMenu {
       objDeletePortfolioVC.modalPresentationStyle = .overCurrentContext
       objDeletePortfolioVC.modalTransitionStyle = .coverVertical
       pgSideMenuVC.present(objDeletePortfolioVC, animated: true, completion: nil)
       }
       
       if let navVC = self.parent?.parent?.parent as? UINavigationController {
       navVC.pushViewController(objEditProfileVC, animated: true)
       }*/
    }
    
    
  }
  
  @objc func actionSalonLocation(_ tapGesture: UITapGestureRecognizer) {
    
    if let dicStylist = GlobalFunction.getStylistDetails() {
      
      if let branch = dicStylist.branch {
      
        if let location = branch.location {
          
          if let coordinates = location.coordinates {
            
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) { //comgooglemapsurl
              
              if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=\(coordinates[1]),\(coordinates[0])&daddr=&directionsmode=driving&zoom=14&views=traffic")!)
                
              } else {
                //UIApplication.shared.openURL(url)
              }
              
            }else {
              
              UIApplication.shared.openMapURL(url: "http://maps.apple.com/?ll=\(coordinates[1]),\(coordinates[0])")
            }
            
          }
          
        }
        
        
       
        
        
       
        
      }
      
    }
    
  }
  
  // MARK: - Set Style of Settings in CollectionView
  func setSettingsStyle() {
    
    // change selected bar color
    settings.style.buttonBarBackgroundColor = .white
    settings.style.buttonBarItemBackgroundColor = .white
    settings.style.buttonBarItemFont = MainFont.regular.with(size: 15.0)
    settings.style.selectedBarBackgroundColor =  Constants.Colors.colorPrimaryDark!
    settings.style.selectedBarHeight = 2.0
    settings.style.buttonBarMinimumLineSpacing = 0
    settings.style.buttonBarItemTitleColor = .darkGray
    settings.style.buttonBarItemsShouldFillAvailableWidth = true
    settings.style.buttonBarLeftContentInset = 0
    settings.style.buttonBarRightContentInset = 0
    settings.style.buttonBarMinimumInteritemSpacing = 0
    settings.style.buttonBarItemLeftRightMargin = 20

  }
  
  // MARK: Current Index Progressive
  func changeCurrentIndexProgressive() {

    changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
        guard changeCurrentIndex == true else { return }

        oldCell?.label.textColor = .black
        newCell?.label.textColor = Constants.Colors.colorPrimary

        /*if animated {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                newCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                oldCell?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            })
        }
        else {
            newCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            oldCell?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }*/
    }
  }
  
  // MARK: - PagerTabStripDataSource
  override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
    
    let aboutVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
    let portfolioVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "PortfolioVC") as! PortfolioVC
    let reviewsVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "ReviewsVC") as! ReviewsVC
    let applyLeaveListVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier:"ApplyLeaveListVC") as! ApplyLeaveListVC
    let holidayListVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "HolidayListVC") as! HolidayListVC
    
    portfolioVC.innerTableViewScrollDelegate = self
    aboutVC.innerTableViewScrollDelegate = self
    reviewsVC.innerTableViewScrollDelegate = self
    applyLeaveListVC.innerTableViewScrollDelegate = self
    holidayListVC.innerTableViewScrollDelegate = self
    
    return [aboutVC, portfolioVC, reviewsVC, applyLeaveListVC, holidayListVC]
  }
  
  // MARK: - Update Indicator
  override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
    
      super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
      if indexWasChanged && toIndex > -1 && toIndex < viewControllers.count {
        Helper.DLog(message: "fromIndex: \(fromIndex)")
        Helper.DLog(message: "toIndex: \(toIndex)")
          switch toIndex {
          case 0:
            tapIndex = 0
            self.btnPlus.isHidden = true
          case 1:
            tapIndex = 1
            self.btnPlus.isHidden = false
          case 2:
            tapIndex = 2
            self.btnPlus.isHidden = true
          case 3:
            tapIndex = 3
            self.btnPlus.isHidden = false
          case 4:
            tapIndex = 4
            self.btnPlus.isHidden = true
          default:
              print("")
          }
      }
      
  }

  
  /*
  //MARK: Setup CollectionView
  func setupCollectionView() {
    
    let layout = tabBarCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
    layout?.estimatedItemSize = CGSize(width: 100, height: 50)
    
    tabBarCollectionView.register(UINib(nibName: Constants.CollectionViewCell.TabBarCollectionViewCell, bundle: nil),
                                  forCellWithReuseIdentifier: Constants.CollectionViewCellIdentifier.TabBarCollectionViewCellID)
    tabBarCollectionView.dataSource = self
    tabBarCollectionView.delegate = self
    
    setupSelectedTabView()
  }
  
  // MARK: - Setup Selected TabView
  func setupSelectedTabView() {
    
    let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: 10))
    label.text = arrHeaderList[0]
    label.sizeToFit()
    var width = label.intrinsicContentSize.width
    width = width + 40
    Helper.DLog(message: "width::\(width)")
    selectedTabView.frame = CGRect(x: 0, y: 0, width: width, height: 2)
    selectedTabView.backgroundColor = Constants.Colors.colorPrimaryDark //UIColor(red:0.65, green:0.58, blue:0.94, alpha:1)
    horizontalLineView.addSubview(selectedTabView)
  }
  
  // MARK: - Setup PagingViewController
  func setupPagingViewController() {
    
    pageViewController = UIPageViewController(transitionStyle: .scroll,
                                              navigationOrientation: .horizontal,
                                              options: nil)
    pageViewController.dataSource = self
    pageViewController.delegate = self
  }
  
  // MARK: - Populated BottomView
  func populateBottomView() {
    
    for subTabCount in 0..<arrHeaderList.count {
      
      if subTabCount == 0 {
        
        if let aboutVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "AboutVC") as? AboutVC {
          
          aboutVC.innerTableViewScrollDelegate = self
          
          let displayName = arrHeaderList[subTabCount]
          let page = Page(with: displayName, _vc: aboutVC, _isSelect: true)
          pageCollection.pages.append(page)
        }
        
        
      }else if subTabCount == 1 {
        
        if let portfolioVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "PortfolioVC") as? PortfolioVC {
          
          portfolioVC.innerTableViewScrollDelegate = self
          
          let displayName = arrHeaderList[subTabCount]
          let page = Page(with: displayName, _vc: portfolioVC, _isSelect: false)
          pageCollection.pages.append(page)
        }
      }else if subTabCount == 2 {
        
        if let reviewsVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "ReviewsVC") as? ReviewsVC {
          
          reviewsVC.innerTableViewScrollDelegate = self
          
          let displayName = arrHeaderList[subTabCount]
          let page = Page(with: displayName, _vc: reviewsVC, _isSelect: false)
          pageCollection.pages.append(page)
        }
      }else if subTabCount == 3 {
        
        if let applyLeaveListVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "ApplyLeaveListVC") as? ApplyLeaveListVC {
          
          applyLeaveListVC.innerTableViewScrollDelegate = self
          
          let displayName = arrHeaderList[subTabCount]
          let page = Page(with: displayName, _vc: applyLeaveListVC, _isSelect: false)
          pageCollection.pages.append(page)
        }
      }else if subTabCount == 4 {
        
        if let holidayListVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "HolidayListVC") as? HolidayListVC {
          
          holidayListVC.innerTableViewScrollDelegate = self
          
          let displayName = arrHeaderList[subTabCount]
          let page = Page(with: displayName, _vc: holidayListVC, _isSelect: false)
          pageCollection.pages.append(page)
        }
      }
      
      
    }
    
    let initialPage = 0
    
    pageViewController.setViewControllers([pageCollection.pages[initialPage].vc],
                                          direction: .forward,
                                          animated: true,
                                          completion: nil)
    
    
    addChild(pageViewController)
    pageViewController.willMove(toParent: self)
    bottomView.addSubview(pageViewController.view)
    
    pinPagingViewControllerToBottomView()
  }
  
  // MARK: - Pin PagingViewController
  func pinPagingViewControllerToBottomView() {
    
    bottomView.translatesAutoresizingMaskIntoConstraints = false
    pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
    
    pageViewController.view.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor).isActive = true
    pageViewController.view.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor).isActive = true
    pageViewController.view.topAnchor.constraint(equalTo: bottomView.topAnchor).isActive = true
    pageViewController.view.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor).isActive = true
  }
  
  // MARK: - Add PanGesture To TopView and CollectionView
  func addPanGestureToTopViewAndCollectionView() {
    
    let topViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(topViewMoved))
    
    stickyHeaderView.isUserInteractionEnabled = true
    stickyHeaderView.addGestureRecognizer(topViewPanGesture)
    
    /* Adding pan gesture to collection view is overriding the collection view scroll.
     
     let collViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(topViewMoved))
     
     tabBarCollectionView.isUserInteractionEnabled = true
     tabBarCollectionView.addGestureRecognizer(collViewPanGesture)
     
     */
  }
  
  
  var dragInitialY: CGFloat = 0
  var dragPreviousY: CGFloat = 0
  var dragDirection: DragDirection = .Up
  
  // MARK: - Top View Moved
  @objc func topViewMoved(_ gesture: UIPanGestureRecognizer) {
    
    var dragYDiff : CGFloat
    
    switch gesture.state {
    
    case .began:
      
      dragInitialY = gesture.location(in: self.stickyHeaderView).y
      dragPreviousY = dragInitialY
      
    case .changed:
      
      let dragCurrentY = gesture.location(in: self.stickyHeaderView).y
      dragYDiff = dragPreviousY - dragCurrentY
      dragPreviousY = dragCurrentY
      dragDirection = dragYDiff < 0 ? .Down : .Up
      innerTableViewDidScroll(withDistance: dragYDiff)
      
    case .ended:
      
      innerTableViewScrollEnded(withScrollDirection: dragDirection)
      
    default: return
      
    }
  }
  
  //MARK:- UI Laying Out Methods
  func setBottomPagingView(toPageWithAtIndex index: Int, andNavigationDirection navigationDirection: UIPageViewController.NavigationDirection) {
    
    pageViewController.setViewControllers([pageCollection.pages[index].vc],
                                          direction: navigationDirection,
                                          animated: true,
                                          completion: nil)
  }

  
  // MARK: - Scroll SelectedTabView
  func scrollSelectedTabView(toIndexPath indexPath: IndexPath, shouldAnimate: Bool = true) {
    
    UIView.animate(withDuration: 0.3) {
      
      if let cell = self.tabBarCollectionView.cellForItem(at: indexPath) as? TabBarCollectionViewCell {
        cell.tabNameLabel.textColor = Constants.Colors.colorPrimaryDark
        self.selectedTabView.frame.size.width = cell.frame.width
        self.selectedTabView.frame.origin.x = cell.frame.origin.x
      }
    }
  }
  */
  // MARK: update HeaderView
  func updateHeaderView() {
    
    if let dicStylist = GlobalFunction.getStylistDetails() {
      
      if let fullName = dicStylist.fullName, fullName != "" {
        self.lblName.text = fullName
      }else {
        self.lblName.text = ""
      }
      
      let attStrFinal = NSMutableAttributedString(string: "")
      
      let imgMapAttachment = NSTextAttachment()
      imgMapAttachment.image = UIImage(named: "ic_map")
      imgMapAttachment.bounds = CGRect(x: 0, y: -1, width: 11, height: 12)
      
      let attStrMapImage = NSAttributedString(attachment: imgMapAttachment)
      
      attStrFinal.append(attStrMapImage)
      
      if let branch = dicStylist.branch {
        
        if let name = branch.name {
          attStrFinal.append(NSAttributedString(string: " \(name)  |  "))
        }
        
      }
      
      if let designation = dicStylist.designation {
        attStrFinal.append(NSAttributedString(string: designation))
      }
      
      self.lblDesignation.attributedText = attStrFinal
      
      
      if let thumbnail = dicStylist.profilePic {
        self.profileIcon.setImage(url: thumbnail, style: .squared, completion: nil)
      }
    }
  }
  
  
  // MARK: - @IBActions
  @IBAction func actionLeftMenu(_ sender: Any) {
    if let sideMenuController = self.parent?.parent?.parent?.parent as? PGSideMenu {
      
      if let sidemneuVC = sideMenuController.leftMenuController?.children[0] as? SideMenuVC {
        
        sidemneuVC.updateUI()
      }
      
      sideMenuController.toggleLeftMenu()
    }
  }
  
  @IBAction func actionOption(_ sender: Any) {
    
    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
    
    let editProfileAction = UIAlertAction(title: "Edit Profile", style: .default, handler: {
      (alert: UIAlertAction!) -> Void in
      self.pushToEditProfileVC()
    })
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
      (alert: UIAlertAction!) -> Void in
      
    })
           
    editProfileAction.setValue(UIColor.black, forKey: "titleTextColor")
    //cancelAction.setValue(UIColor.black, forKey: "titleTextColor")
    
    alertController.addAction(editProfileAction)
    alertController.addAction(cancelAction)
    
    if #available(iOS 13.0, *) {
      alertController.overrideUserInterfaceStyle = .light
    } else {
      // Fallback on earlier versions
    }
    
    alertController.pruneNegativeWidthConstraints()
    self.present(alertController, animated: true, completion: nil)
  }
  
  @IBAction func actionPlus(_ sender: Any) {
    
    switch tapIndex {
    case 1:
      pushToAddPortFolioVC()
    case 3:
      pushToApplyLeaveVC()
    default:
      Helper.DLog(message: "")
    }
  }
  
  // MARK: - Push Add PortFolio screen
  func pushToAddPortFolioVC() {
   
    if let objAddPortFolioVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "AddPortFolioVC") as? AddPortFolioVC {
      
      if let navVC = self.parent?.parent?.parent as? UINavigationController {
       
        navVC.pushViewController(objAddPortFolioVC, animated: true)
        
      }
      
      /*if let pgSideMenuVC = self.parent?.parent?.parent?.parent as? PGSideMenu {
        objAddPortFolioVC.modalPresentationStyle = .overCurrentContext
        objAddPortFolioVC.modalTransitionStyle = .coverVertical
        pgSideMenuVC.present(objAddPortFolioVC, animated: true, completion: nil)
      }*/
    }
    
  }
  
  // MARK: - Push Apply Leave screen
  func pushToApplyLeaveVC() {
   
    if let applyListVc = self.viewControllers[tapIndex] as? ApplyLeaveListVC {
      applyListVc.pushToApplyLeaveVC(nil)
    }
    
    /*if let objApplyLeaveVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "ApplyLeaveVC") as? ApplyLeaveVC {
      
      objApplyLeaveVC.customDeleagte = self
      objApplyLeaveVC.leaveOperation = .Add
      
      /*if let pgSideMenuVC = self.parent?.parent?.parent?.parent as? PGSideMenu {
        objApplyLeaveVC.modalPresentationStyle = .overCurrentContext
        objApplyLeaveVC.modalTransitionStyle = .coverVertical
        pgSideMenuVC.present(objApplyLeaveVC, animated: true, completion: nil)
      }*/
      
      self.cardPopup = nil
      self.cardPopup = SBCardPopupViewController(contentViewController: objApplyLeaveVC)
      self.cardPopup?.cornerRadius = 0
      self.cardPopup?.disableTapToDismiss = true
      self.cardPopup?.disableSwipeToDismiss = true
      if let pgSideMenuVC = self.parent?.parent?.parent?.parent as? PGSideMenu {
        self.cardPopup?.show(onViewController: pgSideMenuVC)
      }
    }*/
    
  }
  
  
  
  
  // MARK: - Push Edit Profile screen
  func pushToEditProfileVC() {
   
    if let objEditProfileVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC {
      
      if let navVC = self.parent?.parent?.parent as? UINavigationController {
        navVC.pushViewController(objEditProfileVC, animated: true)
      }
      
    }
    
  }
}

/*
//MARK:- UICollectionViewDataSource
extension ProfileVC: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
    return pageCollection.pages.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    if let tabCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewCellIdentifier.TabBarCollectionViewCellID, for: indexPath) as? TabBarCollectionViewCell {
      
      if pageCollection.pages[indexPath.item].isSelect == true {
        tabCell.tabNameLabel.textColor = Constants.Colors.colorPrimaryDark
      }else {
        tabCell.tabNameLabel.textColor = UIColor.black
      }
      
      tabCell.tabNameLabel.text = pageCollection.pages[indexPath.item].name
      return tabCell
    }
    
    return UICollectionViewCell()
  }
}

//MARK:- Collection View Delegate

extension ProfileVC: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    if indexPath.item == pageCollection.selectedPageIndex {
      
      return
    }
    
    var direction: UIPageViewController.NavigationDirection
    
    if indexPath.item > pageCollection.selectedPageIndex {
      
      direction = .forward
      
    } else {
      
      direction = .reverse
    }
  
    
    pageCollection.pages[pageCollection.selectedPageIndex].isSelect = false
    
    tabBarCollectionView.reloadItems(at: [IndexPath(item: pageCollection.selectedPageIndex, section: 0)])
    
    pageCollection.selectedPageIndex = indexPath.item
    
    pageCollection.pages[pageCollection.selectedPageIndex].isSelect = true
    
    tabBarCollectionView.scrollToItem(at: indexPath,
                                      at: .centeredHorizontally,
                                      animated: true)
    
    scrollSelectedTabView(toIndexPath: indexPath)
    
    setBottomPagingView(toPageWithAtIndex: indexPath.item, andNavigationDirection: direction)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    
    return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    //return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
  }
}


//MARK:- UIPageViewControllerDataSource
extension ProfileVC: UIPageViewControllerDataSource {
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    
    if let currentViewControllerIndex = pageCollection.pages.firstIndex(where: { $0.vc == viewController }) {
      
      if (1..<pageCollection.pages.count).contains(currentViewControllerIndex) {
        
        // go to previous page in array
        
        return pageCollection.pages[currentViewControllerIndex - 1].vc
      }
    }
    return nil
  }
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    
    if let currentViewControllerIndex = pageCollection.pages.firstIndex(where: { $0.vc == viewController }) {
      
      if (0..<(pageCollection.pages.count - 1)).contains(currentViewControllerIndex) {
        
        // go to next page in array
        
        return pageCollection.pages[currentViewControllerIndex + 1].vc
      }
    }
    return nil
  }
}

//MARK:- Delegate Method to tell Inner View Controller movement inside Page View Controller
//Capture it and change the selection bar position in collection View
//MARK:- UIPageViewControllerDelegate
extension ProfileVC: UIPageViewControllerDelegate {
  
  func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    
    guard completed else { return }
    
    guard let currentVC = pageViewController.viewControllers?.first else { return }
    
    guard let currentVCIndex = pageCollection.pages.firstIndex(where: { $0.vc == currentVC }) else { return }
    
    let indexPathAtCollectionView = IndexPath(item: currentVCIndex, section: 0)
    
    scrollSelectedTabView(toIndexPath: indexPathAtCollectionView)
    tabBarCollectionView.scrollToItem(at: indexPathAtCollectionView,
                                      at: .centeredHorizontally,
                                      animated: true)
  }
}*/

//MARK:- Sticky Header Effect
//MARK:- InnerTableViewScrollDelegate
extension ProfileVC: InnerTableViewScrollDelegate {
  
  var currentHeaderHeight: CGFloat {
    
    return headerViewHeightConstraint.constant
  }
  
  
  func innerTableViewDidScroll(withDistance scrollDistance: CGFloat, withScrollDirection scrollDirection: DragDirection) {
    
    headerViewHeightConstraint.constant -= scrollDistance
    
    /* Don't restrict the downward scroll.*/
    if headerViewHeightConstraint.constant > topViewInitialHeight {
      headerViewHeightConstraint.constant = topViewInitialHeight
    }
    
    /*if headerViewHeightConstraint.constant < topViewFinalHeight {
     
     headerViewHeightConstraint.constant = topViewFinalHeight
     }*/
  }
  
  func innerTableViewScrollEnded(withScrollDirection scrollDirection: DragDirection) {
    
    let topViewHeight = headerViewHeightConstraint.constant
    
    //*  Scroll is not restricted.
    // *  So this check might cause the view to get stuck in the header height is greater than initial height.
    
    if topViewHeight >= topViewInitialHeight || topViewHeight <= topViewFinalHeight { return }
    
    
    if topViewHeight <= topViewFinalHeight + 20 {
      
      scrollToFinalView()
      
    } else if topViewHeight <= topViewInitialHeight - 20 {
      
      switch scrollDirection {
      
      case .Down: scrollToInitialView()
      case .Up: scrollToFinalView()
        
      }
      
    } else {
      
      scrollToInitialView()
    }
  }
  
  func scrollToInitialView() {
    
    let topViewCurrentHeight = stickyHeaderView.frame.height
    
    let distanceToBeMoved = abs(topViewCurrentHeight - topViewInitialHeight)
    
    var time = distanceToBeMoved / 500
    
    if time < 0.25 {
      
      time = 0.25
    }
    
    headerViewHeightConstraint.constant = topViewInitialHeight
    
    UIView.animate(withDuration: TimeInterval(time), animations: {
      
      self.view.layoutIfNeeded()
    })
  }
  
  func scrollToFinalView() {
    
    let topViewCurrentHeight = stickyHeaderView.frame.height
    
    let distanceToBeMoved = abs(topViewCurrentHeight - topViewFinalHeight)
    
    var time = distanceToBeMoved / 500
    
    if time < 0.25 {
      
      time = 0.25
    }
    
    headerViewHeightConstraint.constant = topViewFinalHeight
    
    UIView.animate(withDuration: TimeInterval(time), animations: {
      
      self.view.layoutIfNeeded()
    })
  }
}

// MARK: - CustomDelegate
/*extension ProfileVC: CustomDelegate {
  
  func addStylistLeave(isAdd: Bool) {
    
    if isAdd == true {
      if let applyListVc = self.viewControllers[tapIndex] as? ApplyLeaveListVC {
        applyListVc.getLeaveAfterAddLeave()
      }
    }
    
    
    
  }
  
}*/
