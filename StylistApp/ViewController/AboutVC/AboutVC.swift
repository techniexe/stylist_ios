//
//  AboutVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 10/02/21.
//

import UIKit
import XLPagerTabStrip

protocol InnerTableViewScrollDelegate: class {
  
  var currentHeaderHeight: CGFloat { get }
  func innerTableViewDidScroll(withDistance scrollDistance: CGFloat, withScrollDirection scrollDirection: DragDirection)
  func innerTableViewScrollEnded(withScrollDirection scrollDirection: DragDirection)
}

class AboutVC: UIViewController, IndicatorInfoProvider {
  
  // MARK:- @IBOutlets
  @IBOutlet weak var aboutmeView: UIView!
  @IBOutlet weak var lblAboutme: UILabel!
  @IBOutlet weak var lblBirthDate: UILabel!
  @IBOutlet weak var lblGender: UILabel!
  @IBOutlet weak var lblMobileNo: UILabel!
  @IBOutlet weak var lblEmail: UILabel!
  @IBOutlet weak var salonTypeView: UIView!
  @IBOutlet weak var salonTypeLineView: UIView!
  @IBOutlet weak var lblSalonName: UILabel!
  @IBOutlet weak var lblSalonType: UILabel!
  @IBOutlet weak var lblSalonPone: UILabel!
  @IBOutlet weak var lblSalonEmail: UILabel!
  @IBOutlet weak var aboutSalonView: UIView!
  @IBOutlet weak var lblAboutSalon: UILabel!
  @IBOutlet weak var tblView: UITableView!
  
  //MARK:- Scroll Delegate
  weak var innerTableViewScrollDelegate: InnerTableViewScrollDelegate?
  private var dragDirection: DragDirection = .Up
  private var oldContentOffset = CGPoint.zero
  
  override func viewDidLoad() {
    super.viewDidLoad()
    initializeOnce()
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.lblAboutme.textAlignment = .justified
    self.lblAboutSalon.textAlignment = .justified
    updateUI()
  }
  
  // MARK: - IndicatorInfoProvider
  func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return "About"
  }
  
  // MARK: - initializeOnce
  func initializeOnce() {
  
    //self.tblView.backgroundColor = UIColor.white
    //self.tblView.estimatedRowHeight = 618.0
    //self.tblView.rowHeight = UITableView.automaticDimension
    //self.tblView.register(UINib(nibName: Constants.TableViewCell.AboutCellTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.AboutCellTableViewCellID)
    //Helper.showLoading(self.activityIndicator)
    //addFooterView()
    
    let tapGestureSalonPhone = UITapGestureRecognizer(target: self, action: #selector(self.actionSalonMobNo(_:)))
    self.lblSalonPone.isUserInteractionEnabled = true
    self.lblSalonPone.addGestureRecognizer(tapGestureSalonPhone)
    
  }
  
  //MARK:- Tap Gesture on Salon Contact Number
  @objc func actionSalonMobNo(_ tapGesture: UITapGestureRecognizer) {
    
    if let dicStylist = GlobalFunction.getStylistDetails() {
     
      if let branch = dicStylist.branch {
      
        if let contactNumber = branch.contactNumber, contactNumber != "" {
          Helper.tapOnMobileNumber(contactNumber)
        }
      }
      
    }
    
    
  }
  
  // MARK: updateUI
  func updateUI() {
    
   /* self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.reloadData()*/
    
    
    
    if let dicStylist = GlobalFunction.getStylistDetails() {
      
      if let info = dicStylist.info, info != "" {
        self.lblAboutme.text = info
        self.aboutmeView.isHidden = false
      }else {
        self.lblAboutme.text = ""
        self.aboutmeView.isHidden = true
      }
      
      if let birthdate = dicStylist.birthdate, birthdate != "" {
        self.lblBirthDate.text = "\(Date.convertDateUTCToLocal(strDate: birthdate , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
      }else { self.lblBirthDate.text = "-"}
      
      if let gender = dicStylist.gender, gender != "" {
        self.lblGender.text = gender.capitalized
      }else {
        self.lblGender.text  = "-"
      }
      
      if let mobileNumber = dicStylist.mobileNumber, mobileNumber != "" {
        self.lblMobileNo.text = mobileNumber
      }else {
        self.lblMobileNo.text  = "-"
      }
      
      if let email = dicStylist.email, email != "" {
        self.lblEmail.text = email
      }else {
        self.lblEmail.text  = "-"
      }
      
      
      if let branch = dicStylist.branch {
       
        self.lblSalonName.text = branch.name ?? "-"
        
        if let serveGender = branch.serveGender, serveGender != "" {
          
          if serveGender == "male" {
            self.lblSalonType.text = serveGender.capitalized
          }else if serveGender == "female" {
            self.lblSalonType.text = serveGender.capitalized
          }else {
            self.lblSalonType.text = "Unisex"
          }
          
          
        }else {
          self.lblSalonType.text  = "-"
        }
        
        
        if let contactNumber = branch.contactNumber, contactNumber != "" {
          self.lblSalonPone.text = contactNumber
        }else {
          self.lblSalonPone.text  = "-"
        }
        
        
        
        if let about = branch.about, about != "" {
          self.lblAboutSalon.text = about
          self.aboutSalonView.isHidden = false
        }else {
          self.aboutSalonView.isHidden = true
          self.lblAboutSalon.text  = ""
        }
        
      }
      
      
      if let vendor = dicStylist.vendor {
        
        if let email = vendor.email, email != "" {
          self.lblSalonEmail.text = email
        }else {
          self.lblSalonEmail.text  = "-"
        }
        
      }else {
        self.lblSalonEmail.text  = "-"
      }
      
    }
  }
  
}


/*
// MARK: - UITableViewDelegate, UITableViewDataSource
extension AboutVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 1
  
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.AboutCellTableViewCellID, for: indexPath) as? AboutCellTableViewCell else {
      return UITableViewCell()
    }
    
    return objCell
    
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let view = UIView()
    view.backgroundColor = UIColor.clear
    return view
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView()
    view.backgroundColor = UIColor.clear
    return view
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0
  }
  
  
  
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
      let delta = scrollView.contentOffset.y - oldContentOffset.y
      
      let topViewCurrentHeightConst = innerTableViewScrollDelegate?.currentHeaderHeight
      
      if let topViewUnwrappedHeight = topViewCurrentHeightConst {
          
          /**
           *  Re-size (Shrink) the top view only when the conditions meet:-
           *  1. The current offset of the table view should be greater than the previous offset indicating an upward scroll.
           *  2. The top view's height should be within its minimum height.
           *  3. Optional - Collapse the header view only when the table view's edge is below the above view - This case will occur if you are using Step 2 of the next condition and have a refresh control in the table view.
           */
          
          if delta > 0,
              topViewUnwrappedHeight > topViewHeightConstraintRange.lowerBound,
              scrollView.contentOffset.y > 0 {
              
              dragDirection = .Up
              innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta)
              //print("contentOffset: \(scrollView.contentOffset.y)")
              scrollView.contentOffset.y -= delta
          }
          
          /**
           *  Re-size (Expand) the top view only when the conditions meet:-
           *  1. The current offset of the table view should be lesser than the previous offset indicating an downward scroll.
           *  2. Optional - The top view's height should be within its maximum height. Skipping this step will give a bouncy effect. Note that you need to write extra code in the outer view controller to bring back the view to the maximum possible height.
           *  3. Expand the header view only when the table view's edge is below the header view, else the table view should first scroll till it's offset is 0 and only then the header should expand.
           */
          
          if delta < 0,
              // topViewUnwrappedHeight < topViewHeightConstraintRange.upperBound,
              scrollView.contentOffset.y < 0 {
              
              dragDirection = .Down
              innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta)
              scrollView.contentOffset.y -= delta
          }
      }
      
      oldContentOffset = scrollView.contentOffset
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if scrollView.contentOffset.y <= 0 {
        
        innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }else {
     
      // UITableView only moves in one direction, y axis
     /* let currentOffset = scrollView.contentOffset.y
      let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
      
//      Helper.DLog(message: "currentOffset:::\(currentOffset)")
//      Helper.DLog(message: "contentSize:::\(scrollView.contentSize.height)")
//      Helper.DLog(message: "frameSize:::\(scrollView.frame.height)")
//      Helper.DLog(message: "maximumOffset:::\(maximumOffset)")
      
      // Change 10.0 to adjust the distance from bottom
      if maximumOffset - currentOffset <= 10.0 {
        self.reviewTblView.ptr.isLoadingFooter = true
        self.loadMoreWithPagination()
      }*/
      
    }
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if decelerate == false && scrollView.contentOffset.y <= 0 {
        
        innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }
  }
}
*/


// MARK:- UIScrollViewDelegate
extension AboutVC: UIScrollViewDelegate {
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
      let delta = scrollView.contentOffset.y - oldContentOffset.y
      
      let topViewCurrentHeightConst = innerTableViewScrollDelegate?.currentHeaderHeight
      
      if let topViewUnwrappedHeight = topViewCurrentHeightConst {
          
          /**
           *  Re-size (Shrink) the top view only when the conditions meet:-
           *  1. The current offset of the table view should be greater than the previous offset indicating an upward scroll.
           *  2. The top view's height should be within its minimum height.
           *  3. Optional - Collapse the header view only when the table view's edge is below the above view - This case will occur if you are using Step 2 of the next condition and have a refresh control in the table view.
           */
          
          if delta > 0,
              topViewUnwrappedHeight > topViewHeightConstraintRange.lowerBound,
              scrollView.contentOffset.y > 0 {
              
              dragDirection = .Up
              innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
              //print("contentOffset: \(scrollView.contentOffset.y)")
              scrollView.contentOffset.y -= delta
          }
          
          /**
           *  Re-size (Expand) the top view only when the conditions meet:-
           *  1. The current offset of the table view should be lesser than the previous offset indicating an downward scroll.
           *  2. Optional - The top view's height should be within its maximum height. Skipping this step will give a bouncy effect. Note that you need to write extra code in the outer view controller to bring back the view to the maximum possible height.
           *  3. Expand the header view only when the table view's edge is below the header view, else the table view should first scroll till it's offset is 0 and only then the header should expand.
           */
          
          if delta < 0,
              // topViewUnwrappedHeight < topViewHeightConstraintRange.upperBound,
              scrollView.contentOffset.y < 0 {
              
              dragDirection = .Down
            innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
              scrollView.contentOffset.y -= delta
          }
      }
      
      oldContentOffset = scrollView.contentOffset
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if scrollView.contentOffset.y <= 0 {
        
        innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }else {
     
      // UITableView only moves in one direction, y axis
     /* let currentOffset = scrollView.contentOffset.y
      let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
      
//      Helper.DLog(message: "currentOffset:::\(currentOffset)")
//      Helper.DLog(message: "contentSize:::\(scrollView.contentSize.height)")
//      Helper.DLog(message: "frameSize:::\(scrollView.frame.height)")
//      Helper.DLog(message: "maximumOffset:::\(maximumOffset)")
      
      // Change 10.0 to adjust the distance from bottom
      if maximumOffset - currentOffset <= 10.0 {
        self.reviewTblView.ptr.isLoadingFooter = true
        self.loadMoreWithPagination()
      }*/
      
    }
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if decelerate == false && scrollView.contentOffset.y <= 0 {
        
        innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }
  }
}
