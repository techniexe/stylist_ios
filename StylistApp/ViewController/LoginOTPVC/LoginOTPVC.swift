//
//  LoginOTPVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 22/01/21.
//

import UIKit

class LoginOTPVC: UIViewController {
  
  // MARK:  - @IBOutlets
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
 
  @IBOutlet weak var lblTitle: UILabel!
  
  @IBOutlet weak var lblTimer: UILabel!
  
  @IBOutlet weak var btnResend: UIButton!
  
  @IBOutlet weak var btnVerify: UIButton!
  
  @IBOutlet weak var otpTextFieldView: OTPFieldView!

  // MARK: - @vars
  var strMobileNumber: String = ""
  var timer: Timer? = nil
  var seconds: Int = 60
  var strCode: String = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    initializeOnce()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.lblTimer.isHidden = false
    setTimerText(seconds: seconds)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.invalidateTimer()
  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let textFiled = self.otpTextFieldView.viewWithTag(1) as? UITextField
    textFiled?.becomeFirstResponder()
    setupTimer()
  }
  
  // MARK: - initializeOnce
  func initializeOnce() {
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    self.btnVerify.isEnabled = false
    self.btnVerify.isUserInteractionEnabled = false
    self.btnVerify.alpha = 0.5
    self.lblTitle.textAlignment = .justified
    if self.strMobileNumber.count > 10 {
      
      let strMobile: String = self.strMobileNumber
      
      let newString = strMobile.dropFirst(self.strMobileNumber.count - 10)
      
      self.lblTitle.text = Helper.setMobileOTPText(String(newString))
      
    }else {
      
      self.lblTitle.text = ""
    }
    
    
    setupOtpView()
  }
  
  // MARK: - Setup OTP View
  func setupOtpView(){
    self.otpTextFieldView.fieldsCount = 6
    self.otpTextFieldView.fieldBorderWidth = 2
    self.otpTextFieldView.defaultBorderColor = Constants.Colors.colorlightgray ?? UIColor.black
    self.otpTextFieldView.filledBorderColor = Constants.Colors.colorPrimary ?? UIColor.blue
    self.otpTextFieldView.cursorColor = UIColor.blue
    self.otpTextFieldView.displayType = .underlinedBottom
    self.otpTextFieldView.fieldSize = 40
    self.otpTextFieldView.layoutIfNeeded()
    self.otpTextFieldView.separatorSpace = ((self.otpTextFieldView.bounds.width - (self.otpTextFieldView.fieldSize * CGFloat(self.otpTextFieldView.fieldsCount))) / CGFloat((self.otpTextFieldView.fieldsCount - 1)))
    self.otpTextFieldView.shouldAllowIntermediateEditing = false
    self.otpTextFieldView.delegate = self
    self.otpTextFieldView.initializeUI()
  }
  
  //MARK:- Set Timer Text
  func setTimerText(seconds: Int) {
  
    Helper.secondsToHoursMinutesSeconds(seconds: seconds) { hours, minutes, seconds in
      
      let minutes = Helper.getStringFrom(seconds: minutes)
      let seconds = Helper.getStringFrom(seconds: seconds)
      self.lblTimer.text = "\(minutes):\(seconds)"
    }
    
  }
  
  
  // MARK: - setup Timer
  func setupTimer() {
    
    if timer == nil {
      
      timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
        if self.seconds > 0 {
          self.setTimerText(seconds: self.seconds)
          self.seconds -= 1
        } else {
          self.btnResend.isHidden = false
          self.lblTimer.isHidden = true
          Timer.invalidate()
          self.invalidateTimer()
        }
      }
      
    }
    
  }
  
  // MARK: - Invalid Timer
  func invalidateTimer() {
    self.timer?.invalidate()
    self.timer = nil
  }
  
  // MARK:  - @IBActions
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func actionResendCode(_ sender: Any) {
    
    seconds = 60
    setTimerText(seconds: seconds)
    
    let otpTextFields = self.otpTextFieldView.subviews as! [OTPTextField]
    
    for aTextField in otpTextFields {
      self.otpTextFieldView.deleteText(in: aTextField)
    }
    
    callResendOTPAPI()
  }
  
  @IBAction func actionVerify(_ sender: Any) {
    callVerifyMobileNumberAPI()
  }
  
  // MARK: - Call Verify Mobile Number API
  func callVerifyMobileNumberAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      self.disableUserInteraction()
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.shared.verifyMobileForGetToken(strMobileNumber, strCode: self.strCode) { (statuscode, customToken, error) in
        
        self.enableUserInteraction()
       
        Helper.hideLoading(self.activityIndicator)
        
        guard error == nil else {
          
          if error == "" {
            Helper.showToast(_strMessage: ErrorMessage.invalidcode.rawValue)
          }else {
            Helper.showToast(_strMessage: error)
          }
          return
        }
        
        if let data = customToken?.data {
          
          if let customToken = data.customToken, customToken != "" {
            
            UserDefaults.standard.set(customToken, forKey: Constants.UserDefaults.customToken)
            UserDefaults.standard.set(true, forKey: Constants.UserDefaults.alreadyLogIn)
            UserDefaults.standard.synchronize()
            Helper.showToast(_strMessage: SuccessMessage.loginsuccess.message())
            self.invalidateTimer()
            let otpTextFields = self.otpTextFieldView.subviews as! [OTPTextField]
            
            for aTextField in otpTextFields {
              self.otpTextFieldView.deleteText(in: aTextField)
            }
            GlobalFunction.setRootViewController()
          }
          
        }
        
      }
      
    }
    
    else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
    
  }
  
  // MARK: - Call GET OTP API
  func callResendOTPAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      self.disableUserInteraction()
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.shared.loginWithMobile(strMobileNumber, loginType: .mobile, bodyParams: nil, queryParams: ["method" : "sms"]) { (statuscode, error) in
        
        self.enableUserInteraction()
        
        Helper.hideLoading(self.activityIndicator)
        
        if statuscode == 204 {
          
          Helper.showToast(_strMessage: SuccessMessage.codesuccess.message())
          self.btnResend.isHidden = true
          self.lblTimer.isHidden = false
          self.setupTimer()
          
        }else {
          Helper.showToast(_strMessage: error)
        }
        
      }
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
  }
  
}

// MARK: - UITextFieldDelegate
extension LoginOTPVC: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
    return true
  }
  
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    guard let text = textField.text else { return true }
    
    let newLength = text.count + string.count - range.length
    
    return newLength <= 1
  }
  
}

// MARK: - OTPFieldViewDelegate
extension LoginOTPVC: OTPFieldViewDelegate {
  func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
    print("index:: \(index)")
    return true
  }
  
  func enteredOTP(otp: String) {
    self.strCode = otp
    callVerifyMobileNumberAPI()
  }
  
  func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
    print("hasEnteredAllOTP")
    return true
  }

  
}
