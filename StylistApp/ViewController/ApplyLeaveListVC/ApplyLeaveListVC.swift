//
//  ApplyLeaveListVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 11/02/21.
//

import UIKit
import XLPagerTabStrip

class ApplyLeaveListVC: UIViewController, IndicatorInfoProvider {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var noDataView: UIView!
  
  
  //MARK:- Scroll Delegate
  weak var innerTableViewScrollDelegate: InnerTableViewScrollDelegate?
  // MARK: - @vars
  var arrLeaveList: [LeaveList] = []
  private var dragDirection: DragDirection = .Up
  private var oldContentOffset = CGPoint.zero
  var deleteLeaveIndexPath: IndexPath? = nil
  var lastSelectedIndexPath: IndexPath? = nil
  var cardPopup: SBCardPopupViewController? = nil
  var strFullMonth: String = ""
  var strMonth: String = ""
  var year: Int = 0
  var queryParams: [String: Any]? = nil
  var isDateChange: Bool = false
  var selectedDate: Date!
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    initializeOnce()
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setCurrentMonth()
  }
  
  // MARK: - IndicatorInfoProvider
  func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return "Leaves"
  }

  
  // MARK: - initializeOnce
  func initializeOnce() {
    
    self.tblView.backgroundColor = Constants.Colors.colorBackground
    self.tblView.register(UINib(nibName: Constants.TableViewCell.LeaveListCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.LeaveListCellID)
    self.tblView.register(UINib(nibName: Constants.TableViewCell.NoLeaveCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.NoLeaveCellID)
    self.tblView.register(UINib(nibName: Constants.TableViewCell.LeaveHeaderCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.LeaveHeaderCellID)
    Helper.showLoading(self.activityIndicator)
    addFooterView()
  }
  
  // MARK: - set Month and Year
  func setMonthAndYear(_ month: String, year: Int, isDateChange: Bool) {
   
    strFullMonth = month
    strMonth = strFullMonth.prefix(3).lowercased()
    self.year = year
    self.isDateChange = isDateChange
    queryParams = ["month": strMonth, "year": year]
    getLeaveList(queryParams)
  }
  
  // MARK:- Call Get Stylist Leave List
  func callGetLeaveListAPI(_ queryParams: [String:Any]?, completion: @escaping(Int?, AppData<[LeaveList]>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      APIHandler.shared.getStylistLeaveList(queryParams: queryParams) { (statuscode, response, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        completion(statuscode,response,error)
      }
      
    }else {
      Helper.hideLoading(self.activityIndicator)
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  // MARK:- Get Leave List
  func getLeaveList(_ queryParams: [String:Any]?) {
    
    callGetLeaveListAPI(queryParams) { (statuscode, response, error) in
      
      guard error == nil else {
        self.updateUI()
        return
      }
      
      self.arrLeaveList = []
      
      if let arrLeave = response?.data {
        
        for leave in arrLeave {
          self.arrLeaveList.append(leave)
        }
        
      }
      
      self.updateUI()
    }
    
    
  }
  
  // MARK:- Get Leave List After Add Leave
  func getLeaveAfterAddLeave() {
    
    callGetLeaveListAPI(queryParams) { (statuscode, response, error) in
      
      guard error == nil else {
        self.updateUI()
        return
      }
      
      self.arrLeaveList = []
      
      if let arrLeave = response?.data {
        
        for leave in arrLeave {
          self.arrLeaveList.append(leave)
        }
        
      }
      
      if self.cardPopup != nil {
        self.cardPopup?.close()
        self.cardPopup = nil
      }
      
      self.updateUI()
    }
    
    
  }
  
  
  
  // MARK: - Update UI
  func updateUI() {
    
    //self.tblView.scrollIndicatorInsets = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 20.0, right: 0.0)
    //self.tblView.contentInset = UIEdgeInsets(top: 5.0, left: 0.0, bottom: 20.0, right: 0.0)
    self.tblView.rowHeight = UITableView.automaticDimension
    self.tblView.estimatedRowHeight = 44
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.allowsSelection = false
    self.tblView.reloadData()
    
  }
  
  // MARK;- Clear All
  @objc func actionClearAll(_ sender: UIButton) {
    
    setCurrentMonth()
  }
  
  // MARK:- Set Current Month and Year
  func setCurrentMonth() {
   
    strFullMonth = Date().monthFull.capitalized
    strMonth =  Date().monthMedium.lowercased()
    year = Int(Date().yearMedium) ?? 0
    
    strMonth = strFullMonth.prefix(3).lowercased()
    self.isDateChange = false
    queryParams = nil
    getLeaveList(nil)
  }
  
  // MARK;- Date Change
  @objc func actionDateChange(_ sender: UIButton) {
    
    if let objApplyDateVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "ApplyDateVC") as? ApplyDateVC {
      
      objApplyDateVC.delegate = self
      objApplyDateVC.selectedMonth = strFullMonth
      objApplyDateVC.selectedYear = year
      
      self.cardPopup = nil
      self.cardPopup = SBCardPopupViewController(contentViewController: objApplyDateVC)
      self.cardPopup?.cornerRadius = 5
      self.cardPopup?.disableTapToDismiss = true
      self.cardPopup?.disableSwipeToDismiss = true
      if let pgSideMenuVC = self.parent?.parent?.parent?.parent?.parent as? PGSideMenu {
        self.cardPopup?.show(onViewController: pgSideMenuVC)
      }
      
    }
    
  }
  
}



// MARK: - UITableViewDelegate, UITableViewDataSource
extension ApplyLeaveListVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
  
    if self.arrLeaveList.count == 0 {
     return 2
    }
    
    return self.arrLeaveList.count + 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    if indexPath.section == 0 {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.LeaveHeaderCellID, for: indexPath) as? LeaveHeaderCell else {
        return UITableViewCell()
      }
      
      objCell.btnClearAll.addTarget(self, action: #selector(self.actionClearAll(_:)), for: .touchUpInside)
      
      objCell.btnDateChage.addTarget(self, action: #selector(self.actionDateChange(_:)), for: .touchUpInside)
      
      objCell.txtDate.text = "\(strFullMonth) - \(String(year))"
      
      if self.isDateChange == true {
        
        objCell.btnClearAll.isHidden = false
        
        objCell.btnDateChage.isHidden = true
        
      }else {
        
        objCell.btnClearAll.isHidden = true
        
        objCell.btnDateChage.isHidden = false
      }
      
      
      
      return objCell
      
    }else {
      
      if self.arrLeaveList.count == 0 {
        
        guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.NoLeaveCellID, for: indexPath) as? NoLeaveCell else {
          return UITableViewCell()
        }
        
        return objCell
        
      }else {
        
        guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.LeaveListCellID, for: indexPath) as? LeaveListCell else {
          return UITableViewCell()
        }
        
        
        //      objCell.lblName.text = "Paras Modi"
        //      objCell.lblReviewText.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard."
        //      objCell.lblCreatedDate.text = "2 days ago"
        //      objCell.ratingView.rating = 5.0
        
        objCell.configureCell(self.arrLeaveList[indexPath.section-1])
        
        return objCell
        
      }
      
    }
    
  }
  
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let view = UIView()
    view.backgroundColor = UIColor.clear
    return view
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    
    if section == 0 {
      let view = UIView()
      view.backgroundColor = UIColor.clear
      return view
    }else {
      let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.tblView.bounds.size.width, height: 20.0))
      view.backgroundColor = Constants.Colors.colorBackground
      return view
    }
    
    
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    
    if section == 0 {
      return 0
    }else {
      return 20
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    return
  }

  @available(iOS 11.0, *)
  func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    
    let deleteAction = UIContextualAction(style: .destructive, title: nil) { _, _, complete in
      self.confirmDelete(indexPath: indexPath)
      complete(true)
    }
    
    deleteAction.image = UIImage(named: "ic_delete")?.sd_tintedImage(with: UIColor.white)
    deleteAction.backgroundColor = Constants.Colors.colorPrimary
    
    let editAction = UIContextualAction(style: .destructive, title: nil) { _, _, complete in
      self.pushToApplyLeaveVC(indexPath)
      complete(true)
    }
    
    editAction.image = UIImage(named: "ic_pencil")?.sd_tintedImage(with: UIColor.white)
    editAction.backgroundColor = Constants.Colors.colorPrimaryDark
    
    let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
    configuration.performsFirstActionWithFullSwipe = false
    return configuration
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    
    if indexPath.section == 0 {
      return false
    }else {
      
      if self.arrLeaveList.count == 0 {
        return false
      }else {
        if self.arrLeaveList[indexPath.section-1].leaveStatus == "UNAPPROVED" {
          return true
        }
        return false
      }
      
    }
    
    
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
      let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { _, _ in
          //self.Items.remove(at: indexPath.row)
          //self.tableView.deleteRows(at: [indexPath], with: .automatic)
      }
      deleteAction.backgroundColor = .red
      return [deleteAction]
  }
  
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
    let delta = scrollView.contentOffset.y - oldContentOffset.y
    
    let topViewCurrentHeightConst = innerTableViewScrollDelegate?.currentHeaderHeight
    
    if let topViewUnwrappedHeight = topViewCurrentHeightConst {
      
      /**
       *  Re-size (Shrink) the top view only when the conditions meet:-
       *  1. The current offset of the table view should be greater than the previous offset indicating an upward scroll.
       *  2. The top view's height should be within its minimum height.
       *  3. Optional - Collapse the header view only when the table view's edge is below the above view - This case will occur if you are using Step 2 of the next condition and have a refresh control in the table view.
       */
      
      if delta > 0,
         topViewUnwrappedHeight > topViewHeightConstraintRange.lowerBound,
         scrollView.contentOffset.y > 0 {
        
        dragDirection = .Up
        innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
        //print("contentOffset: \(scrollView.contentOffset.y)")
        scrollView.contentOffset.y -= delta
      }
      
      /**
       *  Re-size (Expand) the top view only when the conditions meet:-
       *  1. The current offset of the table view should be lesser than the previous offset indicating an downward scroll.
       *  2. Optional - The top view's height should be within its maximum height. Skipping this step will give a bouncy effect. Note that you need to write extra code in the outer view controller to bring back the view to the maximum possible height.
       *  3. Expand the header view only when the table view's edge is below the header view, else the table view should first scroll till it's offset is 0 and only then the header should expand.
       */
      
      if delta < 0,
         // topViewUnwrappedHeight < topViewHeightConstraintRange.upperBound,
         scrollView.contentOffset.y < 0 {
        
        dragDirection = .Down
        innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
        scrollView.contentOffset.y -= delta
      }
    }
    
    oldContentOffset = scrollView.contentOffset
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if scrollView.contentOffset.y <= 0 {
      
      innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if decelerate == false && scrollView.contentOffset.y <= 0 {
      
      innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }
  }
}


extension ApplyLeaveListVC {
  
  // MARK: - Push To apply Leave ViewController
  func pushToApplyLeaveVC(_ indexPath: IndexPath?) {
    
    if let objApplyLeaveVC = Constants.kProfileStoryBoard.instantiateViewController(withIdentifier: "ApplyLeaveVC") as? ApplyLeaveVC {
      
      lastSelectedIndexPath = indexPath
      objApplyLeaveVC.delegate = self
      
      if let indexPath = indexPath {
        
        objApplyLeaveVC.leaveOperation = .Edit
        objApplyLeaveVC.dicStylistLeave = self.arrLeaveList[indexPath.section-1]
      
      }else {
        objApplyLeaveVC.leaveOperation = .Add
        objApplyLeaveVC.dicStylistLeave = nil
      }
      
      
     
      /*if let pgSideMenuVC = self.parent?.parent?.parent?.parent?.parent as? PGSideMenu {
        /*objApplyLeaveVC.modalPresentationStyle = .overCurrentContext
        objApplyLeaveVC.modalTransitionStyle = .coverVertical
        pgSideMenuVC.present(objApplyLeaveVC, animated: true, completion: nil)*/
      }*/
      
      self.cardPopup = nil
      self.cardPopup = SBCardPopupViewController(contentViewController: objApplyLeaveVC)
      self.cardPopup?.cornerRadius = 5
      self.cardPopup?.disableTapToDismiss = true
      self.cardPopup?.disableSwipeToDismiss = true
      if let pgSideMenuVC = self.parent?.parent?.parent?.parent?.parent as? PGSideMenu {
        self.cardPopup?.show(onViewController: pgSideMenuVC)
      }
    }
    
  }
  
  
  // MARK: - Add Circulr View On Footer Of Tableview
  func addFooterView() {
   
    tblView.ptr.footerHeight = 40
    //tblView.ptr.footerView = getProgressCircle()
    
    self.tblView.ptr.footerCallback = { [weak self] in
      
      if self?.arrLeaveList.count != 0 {
        
        if let strCreatedAt = self?.arrLeaveList.last?.createdAt {
          
          if let dateFromString = strCreatedAt.dateFromISO8601 {
            
            let beforeTime = dateFromString.iso8601
            
            DispatchQueue.global(qos: .default).async(execute: { [self] in
        
              var qryparams = [String:Any]()
              
              qryparams["before"] = beforeTime
              
              if let _ = self?.queryParams {
                
                qryparams["month"] = self?.strMonth
                
                qryparams["year"] = self?.year
                
              }
              
              
              self?.callGetLeaveListAPI(qryparams, completion: { (statuscode, dataResponse, error) in
                
                if dataResponse != nil {
                  
                  if let dataArray = dataResponse?.data {
                    
                    if dataArray.count > 0 {
                      
                      self?.tblView.ptr.isLoadingFooter = false
                      
                      var arrIndex: [Int] = []
                      
                      var index: Int = 0
                      
                      index = (self?.arrLeaveList.count)!
                      
                      for element in dataArray {
                        index += 1
                        arrIndex.append(index)
                        self?.arrLeaveList.append(element)
                        
                      }
                      
                      
                      DispatchQueue.main.async {
                        self?.tblView.performBatchUpdates({
                          self?.tblView.insertSections(IndexSet(arrIndex), with: .none)
                        }, completion: { (completed) in
                          
                        })
                      }
                      
                
                    }else {
                      
                      self?.hideFooterView()
                    }
                    
                  }else {
                    self?.hideFooterView()
                  }
                  
                }else {
                  self?.hideFooterView()
                }
                
              })
              
              
            })
            
          }
          
        }
          
        
      }else{
        self?.hideFooterView()
      }

    }
    
  }
  
  // MARK:- Hide FooterView
  func hideFooterView() {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
      self.tblView.ptr.isLoadingFooter = false
    }
  }
}


extension ApplyLeaveListVC {
  
  // Delete Confirmation and Handling
  func confirmDelete(indexPath: IndexPath) {
   
    deleteLeaveIndexPath = indexPath
    
    let alert = UIAlertController(title: nil, message: Constants.CommonAlert.deleteLeave, preferredStyle: .alert)
    
    alert.setMessage(font: MainFont.medium.with(size: 16.0), color: UIColor.black)
    
    let DeleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: handleDeletePlanet)
    let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: cancelDeletePlanet)
    
    alert.addAction(DeleteAction)
    alert.addAction(CancelAction)
    
    if #available(iOS 13.0, *) {
      alert.overrideUserInterfaceStyle = .light
    } else {
      // Fallback on earlier versions
    }
    
    alert.pruneNegativeWidthConstraints()
    
    self.parent?.parent?.present(alert, animated: true, completion: nil)
  }
  
  func handleDeletePlanet(alertAction: UIAlertAction) -> Void {
  
    if let indexPath = deleteLeaveIndexPath {
     
      if Reachability.isConnectedToNetwork() {
        
        APIHandler.shared.requestOTP { (statuscode, response, error) in
          
          guard error == nil else {
            Helper.showToast(_strMessage: error)
            return
          }
          
          if let strCode = response?.code {
            
            if let leaveID = self.arrLeaveList[indexPath.section - 1]._id {
              
              APIHandler.shared.cancelLeave(strLeaveID: leaveID, bodyParams: ["authentication_code": strCode]) { (statuscode, error) in
                
                if statuscode == 204 {
                
                  Helper.showToast(_strMessage: SuccessMessage.leaveCancelSuccess.message())
                  
                  if let section = self.deleteLeaveIndexPath?.section  {
                    self.arrLeaveList.remove(at: section - 1)
                    let indexSet = IndexSet(integer: indexPath.section)
                    self.tblView.deleteSections(indexSet, with: .automatic)
                  }
                  
                  
                  //self.getLeaveList()
                
                }else {
                
                  Helper.showToast(_strMessage: error)
                
                }
                
              }
              
            }
            
            
          }
          
        }
        
      }else {
        Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
      }
      
      
      
    }
  }
      
  func cancelDeletePlanet(alertAction: UIAlertAction) -> Void {
    deleteLeaveIndexPath = nil
  }
}


// MARK: - ApplyLeaveDelegate
extension ApplyLeaveListVC: ApplyLeaveDelegate {
  
  
  func addStylistLeave(_ dicLeave: LeaveList?, leaveOperation: StylistLeaveOperation) {
    
    
    if leaveOperation == .Add {
      
      if let leave = dicLeave {
       
        self.arrLeaveList.insert(leave, at: 0)
        self.tblView.reloadData()
        self.tblView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: false)
        
      }
      
      self.getLeaveAfterAddLeave()
      
    }else {
      
      if let leave = dicLeave {
        
        if let section = self.lastSelectedIndexPath?.section {
          
          self.arrLeaveList[section - 1]  = leave
          
          self.tblView.reloadRows(at: [IndexPath(row: 0, section: section)], with: UITableView.RowAnimation.automatic)
        }
        
      }
      
    }
    
  }
  
}


// MARK:- CustomDelegate
extension ApplyLeaveListVC: CustomDelegate {
  
  func applyDate(_ month: String, year: Int) {
    
    strFullMonth = month
    self.year = year
    setMonthAndYear(month, year: year, isDateChange: true)
    
  }
}

