//
//  ApplyLeaveVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 17/02/21.
//

import UIKit


protocol ApplyLeaveDelegate {
  
  func addStylistLeave(_ dicLeave: LeaveList?, leaveOperation: StylistLeaveOperation)
}

class ApplyLeaveVC: UIViewController {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var txtStartDate: FloatingTextField!
  @IBOutlet weak var txtEndDate: FloatingTextField!
  @IBOutlet weak var indicatorheightConstraint: NSLayoutConstraint!
  @IBOutlet weak var txtViewDescription: UITextView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
 
  // MARK: - @vars
  var startDatePicker: UIDatePicker!
  var endDatePicker: UIDatePicker!
  var dateFormatter: DateFormatter!
  var startDate: Date!
  var endDate: Date!
  var delegate: ApplyLeaveDelegate? = nil
  var leaveOperation : StylistLeaveOperation = .Add
  var dicStylistLeave: LeaveList? = nil
  
  // MARK: - let
  let placeholder = "Write a Leave For Reason"
  let placeholderTextColor = UIColor(red: 126.0/255.0, green: 126.0/255.0, blue: 127.0/255.0, alpha: 1.0)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView() {
    
    self.view.backgroundColor = UIColor(white: 0.4, alpha: 0.8)
    //self.view.backgroundColor = UIColor(white: 0.1, alpha: 0.5)
    self.txtViewDescription.backgroundColor = .white
    self.txtViewDescription.textColor = placeholderTextColor
    
    self.txtStartDate.addDoneOnKeyboardWithTarget(self, action: #selector(self.startDateDoneButtonClicked(_:)))
    self.txtEndDate.addDoneOnKeyboardWithTarget(self, action: #selector(self.endDateDoneButtonClicked(_:)))
    
    
    startDatePicker = UIDatePicker()
    endDatePicker = UIDatePicker()
    startDatePicker.datePickerMode = .date
    endDatePicker.datePickerMode = .date
    
    if #available(iOS 13.4, *) {
      startDatePicker.preferredDatePickerStyle = .wheels
      endDatePicker.preferredDatePickerStyle = .wheels
    } else {
      // Fallback on earlier versions
    }
   
    startDatePicker.addTarget(self, action: #selector(self.startDateChange(_:)), for: .valueChanged)
    endDatePicker.addTarget(self, action: #selector(self.endDateChange(_:)), for: .valueChanged)
    
    self.txtStartDate.inputView = startDatePicker
    self.txtEndDate.inputView = endDatePicker
    
    dateFormatter = DateFormatter()
    dateFormatter.dateFormat = DateFormats.birth.format()
    
    
    if leaveOperation == .Edit {
      
      self.lblTitle.text = "Update Leave"
      
      if let dicLeave = dicStylistLeave {
        
        if let reason = dicLeave.reason, reason != "" {
          self.txtViewDescription.text = reason
          self.txtViewDescription.textColor = UIColor.black
        }
        
        if let startTime = dicLeave.startTime, startTime != "" {
          
          self.txtStartDate.text = "\(Date.convertDateUTCToLocal(strDate: startTime , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
          
          let formatter = DateFormatter()
          formatter.dateFormat = DateFormats.birth.format()
          
          if let text = self.txtStartDate.text {
            if let date = formatter.date(from: text) {
              self.startDatePicker.date = date
              startDate = self.startDatePicker.date
              startDatePicker.minimumDate = Date()
            }
          }
          
          
        }
        
        if let endTime = dicLeave.endTime, endTime != "" {
          self.txtEndDate.text = "\(Date.convertDateUTCToLocal(strDate: endTime , oldFormate: DateFormats.iso.format(), newFormate: DateFormats.birth.format()))"
          
          let formatter = DateFormatter()
          formatter.dateFormat = DateFormats.birth.format()
          
          if let text = self.txtEndDate.text {
            if let date = formatter.date(from: text) {
              self.endDatePicker.date = date
              endDate = self.endDatePicker.date
              endDatePicker.minimumDate = Date()
            }
          }
        }
        
      }
     
    }else {
      
      self.lblTitle.text = "Apply Leave"
      
      startDatePicker.minimumDate = Date()
      endDatePicker.minimumDate = Date()
      
      startDate = startDatePicker.minimumDate
      endDate = endDatePicker.minimumDate
    }
    
    
    
  }
  
  // MARK: - Start Date Change
  @objc func startDateChange(_ sender: UIDatePicker) {
    startDate = sender.date
    endDatePicker.minimumDate = startDate
    self.txtStartDate.text = dateFormatter.string(from: sender.date)
  }
  
  // MARK: - End Date Change
  @objc func endDateChange(_ sender: UIDatePicker) {
    endDate = sender.date
    self.txtEndDate.text = dateFormatter.string(from: sender.date)
  }
  
  // MARK: - Start Date Done Click on KeyBoard
  @objc func startDateDoneButtonClicked(_ sender: Any) {
    startDate = startDatePicker.date
    endDatePicker.minimumDate = startDate
    self.txtStartDate.text = dateFormatter.string(from: startDatePicker.date)
    self.view.endEditing(true)
  }
  
  // MARK: - End Date Done Click on KeyBoard
  @objc func endDateDoneButtonClicked(_ sender: Any) {
    endDate = endDatePicker.date
    self.txtEndDate.text = dateFormatter.string(from: endDatePicker.date)
    self.view.endEditing(true)
  }
  
  // MARK: - @IBActions
  @IBAction func actionClose(_ sender: Any) {
  
    if let popupVC = self.parent as? SBCardPopupViewController {
      popupVC.close()
    }
  }
  
  @IBAction func actionSubmit(_ sender: Any) {
    
    if self.txtViewDescription.textColor == placeholderTextColor || self.txtViewDescription.text.trim().count == 0 {
      
      Helper.showToast(_strMessage: ErrorMessage.enterLeaveReason.message())
      return
      
    }else if self.txtStartDate.text?.trim().count == 0 {
     
      Helper.showToast(_strMessage: ErrorMessage.enterStartDate.message())
      return
      
    }else if self.txtEndDate.text?.trim().count == 0 {
      
      Helper.showToast(_strMessage: ErrorMessage.enterEndDate.message())
      return
      
    }else if endDate.compare(startDate) == .orderedAscending {
      
      Helper.showToast(_strMessage: ErrorMessage.greaterEndDate.message())
      return
    
    } else {
      
      if Reachability.isConnectedToNetwork() {
        
        self.view.isUserInteractionEnabled = false
        
        view.layoutIfNeeded() // force any pending operations to finish

        UIView.animate(withDuration: 0.2, animations: { () -> Void in
          self.indicatorheightConstraint.constant = 40.0
          self.view.layoutIfNeeded()
        })

        Helper.showLoading(self.activityIndicator)
        
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormats.fetched.format()
        
        let startTime = formatter.string(from: startDate)
        let endTime = formatter.string(from: endDate)
        
        if leaveOperation == .Add {
         
          APIHandler.shared.addLeave(bodyParams: ["reason": self.txtViewDescription.text.trim(), "start_time": startTime, "end_time": endTime]) { (statusCode, dataResponse, error) in
            
            self.view.isUserInteractionEnabled = true
      
            self.view.layoutIfNeeded() // force any pending operations to finish

            UIView.animate(withDuration: 0.2, animations: { () -> Void in
              self.indicatorheightConstraint.constant = 0.0
              self.view.layoutIfNeeded()
            })
            
            Helper.hideLoading(self.activityIndicator)
            
            guard error == nil else {
              Helper.showToast(_strMessage: error)
              return
            }
            
            if let response = dataResponse {
              
              
              if let responseData = response.data {
                
                
                let dicLeave = LeaveList(_id: responseData._id, leaveStatus: responseData.leaveStatus, reason: responseData.reason
                                         , startTime: responseData.startTime, endTime: responseData.endTime, createdAt: responseData.createdAt, updatedAt: responseData.updatedAt, branch: nil)
                
                Helper.showToast(_strMessage: SuccessMessage.leaveAppliedSuccess.message())
                self.delegate?.addStylistLeave(dicLeave, leaveOperation: self.leaveOperation)
                
                if let popupVC = self.parent as? SBCardPopupViewController {
                  popupVC.close()
                }
                
                
              }
              
              
            }else {
              Helper.showToast(_strMessage: error)
            }
          }
          
          /*APIHandler.shared.addLeave(bodyParams: ["reason": self.txtViewDescription.text.trim(), "start_time": startTime, "end_time": endTime]) { (statusCode, error) in
            
            self.view.isUserInteractionEnabled = true
      
            self.view.layoutIfNeeded() // force any pending operations to finish

            UIView.animate(withDuration: 0.2, animations: { () -> Void in
              self.indicatorheightConstraint.constant = 0.0
              self.view.layoutIfNeeded()
            })
            
            Helper.hideLoading(self.activityIndicator)
            
            if statusCode == 200 {
              Helper.showToast(_strMessage: SuccessMessage.leaveAppliedSuccess.message())
              self.delegate?.addStylistLeave(nil, leaveOperation: .Add)
            }else {
              Helper.showToast(_strMessage: error)
            }
            
          }*/
          
        }else {
          
          if let strLeaveId = dicStylistLeave?._id, strLeaveId != "" {
            
            APIHandler.shared.editLeave(strLeaveID: strLeaveId, bodyParams: ["reason": self.txtViewDescription.text.trim(), "start_time": startTime, "end_time": endTime]) { (statusCode, dataResponse, error) in
              
              self.view.isUserInteractionEnabled = true
              
              self.view.layoutIfNeeded() // force any pending operations to finish
              
              UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.indicatorheightConstraint.constant = 0.0
                self.view.layoutIfNeeded()
              })
              
              Helper.hideLoading(self.activityIndicator)
              
              guard error == nil else {
                Helper.showToast(_strMessage: error)
                return
              }
              
              if let response = dataResponse {
                
                
                if let responseData = response.data {
                  
                  
                  let dicLeave = LeaveList(_id: responseData._id, leaveStatus: responseData.leaveStatus, reason: responseData.reason
                                           , startTime: responseData.startTime, endTime: responseData.endTime, createdAt: responseData.createdAt, updatedAt: responseData.updatedAt, branch: nil)
                  
                  Helper.showToast(_strMessage: SuccessMessage.leaveEditedSuccess.message())
                  self.delegate?.addStylistLeave(dicLeave, leaveOperation: self.leaveOperation)
                  
                  if let popupVC = self.parent as? SBCardPopupViewController {
                    popupVC.close()
                  }
                  
                  
                }
                
                
              }else {
                Helper.showToast(_strMessage: error)
              }
              
              
              /*if statusCode == 200 {
                Helper.showToast(_strMessage: SuccessMessage.leaveEditedSuccess.message())
                self.customDeleagte?.addStylistLeave?()
              }else {
                Helper.showToast(_strMessage: error)
              }*/
              
            }
            
            /*APIHandler.shared.editLeave(strLeaveID: strLeaveId, bodyParams: ["reason": self.txtViewDescription.text.trim(), "start_time": startTime, "end_time": endTime]) { (statusCode, error) in
              
              self.view.isUserInteractionEnabled = true
              
              self.view.layoutIfNeeded() // force any pending operations to finish
              
              UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.indicatorheightConstraint.constant = 0.0
                self.view.layoutIfNeeded()
              })
              
              Helper.hideLoading(self.activityIndicator)
              
              if statusCode == 204 {
                Helper.showToast(_strMessage: SuccessMessage.leaveEditedSuccess.message())
                self.customDeleagte?.addStylistLeave?()
              }else {
                Helper.showToast(_strMessage: error)
              }
            }*/
            
          }
          
      
        }
        
      
        
      }else {
        Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
      }
      
    }
    
  }

  
}


// MARK: - UITextViewDelegate
extension ApplyLeaveVC: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    
    if textView.textColor == placeholderTextColor {
      textView.text = nil
      textView.textColor = UIColor.black
    }else {
      textView.textColor = UIColor.black
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    
    if textView.text == "" {
      textView.textColor = placeholderTextColor
      textView.text = placeholder
    }else {
      textView.textColor = UIColor.black
    }
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
      
      if text == "\n" {
        textView.resignFirstResponder()
        return false
      }
    
      let newLength = textView.text.utf16.count + text.utf16.count - range.length
      
      if newLength == 0 {
          return true
      }
    
    if newLength > 0 && newLength <= MaxLength.leaveReason.getMaxLength() {
          
          return true
      }
      else {
          return false
      }
      
  }
  
}
