//
//  LoginVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 22/01/21.
//

import UIKit
import libPhoneNumber_iOS

class LoginVC: UIViewController {
  
  // MARK:  - @IBOutlets
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
 
  @IBOutlet weak var txtMobile: FloatingTextField!
  
  @IBOutlet weak var btnSignIn: UIButton!
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    initializeOnce()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - initializeOnce
  func initializeOnce() {
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    txtMobile.addDoneOnKeyboardWithTarget(self, action: #selector(doneButtonClicked))
  }
  
  /* Check textfiled blank or not */
  func checkEveryFieldIsFilled() -> Bool {
    
    if Helper.isMobileNumberEmpty(aStrMobileNumber:(self.txtMobile.text?.trim())!) {
      Helper.showToast(_strMessage: ErrorMessage.mobile.rawValue)
      return false
      
    }
    
    
    return true
  }
  
  // MARK: - Done Click on KeyBoard
  @objc func doneButtonClicked(_ sender: Any) {
    signIn()
  }
  
  // MARK:  - @IBActions
  @IBAction func actionSignIn(_ sender: Any) {
    signIn()
  }
  
  // MARK: - signIn
  func signIn() {
  
    self.view.endEditing(true)
    
    if checkEveryFieldIsFilled() {
      
      LocalNetworkAuthorization.init().requestAuthorization { (completed) in
      
        let phoneUtil = NBPhoneNumberUtil()
        
        do {
          let phoneNumber: NBPhoneNumber = try phoneUtil.parse(self.txtMobile.text?.trim(), defaultRegion: "IN")
          try phoneUtil.format(phoneNumber, numberFormat: .E164)
          
          if phoneUtil.getNumberType(phoneNumber) == .MOBILE || phoneUtil.getNumberType(phoneNumber) == .FIXED_LINE_OR_MOBILE   {
            
            
            let isNumberValid = phoneUtil.isValidNumber(phoneNumber)
            
            if isNumberValid == true {
              let mobileNumber = "+\(phoneNumber.countryCode!)\(phoneNumber.nationalNumber!)"
              self.callloginWithMobileAPI(mobileNumber, loginType: .mobile, queryParams: ["method" : "sms"])
            }else {
              Helper.showToast(_strMessage: ErrorMessage.invalidmobileregioncode.rawValue)
              return
            }
          }else {
            Helper.showToast(_strMessage: ErrorMessage.invalidmobileregioncode.rawValue)
            return
          }
          
        }
        catch let error as NSError {
          print(error)
          Helper.showToast(_strMessage: ErrorMessage.invalidemailmobile.rawValue)
          return
        }
        
      }
     
    }
    
  }
  
 
  
  // MARK: - Call Login API
  func callloginWithMobileAPI(_ strPath: String, loginType: LogInType, queryParams: [String: Any]?) {
    
    if Reachability.isConnectedToNetwork() {
      
      self.view.isUserInteractionEnabled = false
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.shared.loginWithMobile(strPath, loginType: .mobile, bodyParams: nil, queryParams: queryParams) { (statuscode, error) in
        
        self.view.isUserInteractionEnabled = true
       
        Helper.hideLoading(self.activityIndicator)
        
        if statuscode == 204 {
          
          Helper.showToast(_strMessage: SuccessMessage.codesuccess.message())
          if let loginOTPVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier: "LoginOTPVC") as? LoginOTPVC{
            loginOTPVC.strMobileNumber = strPath
            self.navigationController?.pushViewController(loginOTPVC, animated: true)
          }
          
        }else {
          Helper.showToast(_strMessage: error)
        }
        
      }
      
    }else {
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
    /*if let loginOTPVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier: "LoginOTPVC") as? LoginOTPVC {
      loginOTPVC.strMobileNumber = strPath
      self.navigationController?.pushViewController(loginOTPVC, animated: true)
    }*/
  }
  
}


// MARK: - UITextFieldDelegate
extension LoginVC : UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
   
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
    
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
   
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if range.location == 0 && string == " " {
      return false
    }
    
    let currentString: NSString = textField.text! as NSString
    let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
    return newString.length <= MaxLength.mobile.rawValue
    
  }
  
  
}


import Foundation
import Network

@available(iOS 14.0, *)
public class LocalNetworkAuthorization: NSObject {
    private var browser: NWBrowser?
    private var netService: NetService?
    private var completion: ((Bool) -> Void)?
    
    public func requestAuthorization(completion: @escaping (Bool) -> Void) {
        self.completion = completion
        
        // Create parameters, and allow browsing over peer-to-peer link.
        let parameters = NWParameters()
        parameters.includePeerToPeer = true
        
        // Browse for a custom service type.
        let browser = NWBrowser(for: .bonjour(type: "_bonjour._tcp", domain: nil), using: parameters)
        self.browser = browser
        browser.stateUpdateHandler = { newState in
            switch newState {
            case .failed(let error):
                print(error.localizedDescription)
            case .ready, .cancelled:
                break
            case let .waiting(error):
                print("Local network permission has been denied: \(error)")
                self.reset()
                self.completion?(false)
            default:
                break
            }
        }
        
        self.netService = NetService(domain: "local.", type:"_lnp._tcp.", name: "LocalNetworkPrivacy", port: 8083)
        self.netService?.delegate = self
        
        self.browser?.start(queue: .main)
        self.netService?.publish()
    }
    
    private func reset() {
        self.browser?.cancel()
        self.browser = nil
        self.netService?.stop()
        self.netService = nil
    }
}

@available(iOS 14.0, *)
extension LocalNetworkAuthorization : NetServiceDelegate {
    public func netServiceDidPublish(_ sender: NetService) {
        self.reset()
        print("Local network permission has been granted")
        completion?(true)
    }
}
