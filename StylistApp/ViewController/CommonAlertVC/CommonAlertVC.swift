//
//  CommonAlertVC.swift
//  ConmateBuyerApp
//
//  Created by Techniexe Infolabs on 09/11/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class CommonAlertVC: UIViewController {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var viewContainer: UIView!
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet var btnAlert: [CustomButton]!
  var customDelegate : CustomDelegate?
  var commonAlert: CommonAlert = .logout
  var itemName:  String = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    setText()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewContainer.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 4)
    
  }
  
  // MARK: - Set Text
  func setText() {
    switch commonAlert {
    case .logout:
      self.lblTitle.text = Constants.CommonAlert.logout
    case .deletePortFlio:
      self.lblTitle.text = Constants.CommonAlert.deletePortFolio
    }
  }
  
  @IBAction func actionYesOrNo(_ sender: Any) {
    
    if let btn = sender as? UIButton {

      self.dismissViewController()
      
      switch btn.tag {
      case 0:
        self.customDelegate?.actionCustomAlertDialog?(btn)
      case 1:
        Helper.DLog(message: "\(btn.tag)")
      default:
        Helper.DLog(message: "\(btn.tag)")
      }
      
     
    }
    
  }
}
