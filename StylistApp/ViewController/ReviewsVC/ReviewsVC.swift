//
//  ReviewsVC.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 11/02/21.
//

import UIKit
import XLPagerTabStrip


class ReviewsVC: UIViewController, IndicatorInfoProvider {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var reviewTblView: UITableView!
  @IBOutlet weak var noDataView: UIView!
  
  
  //MARK:- Scroll Delegate
  weak var innerTableViewScrollDelegate: InnerTableViewScrollDelegate?
  // MARK: - @vars
  var arrReviewList: [ReviewDetails] = []
  private var dragDirection: DragDirection = .Up
  private var oldContentOffset = CGPoint.zero
  var footerView: CustomTableFooterView? = nil
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    initializeOnce()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    callGetReviewAPI()
  }
  
  // MARK: - IndicatorInfoProvider
  func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return "Reviews"
  }
 
  // MARK: - initializeOnce
  func initializeOnce() {
  
    self.reviewTblView.backgroundColor = UIColor.white
    self.reviewTblView.register(UINib(nibName: Constants.TableViewCell.ReviewCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.ReviewCellID)
    self.reviewTblView.register(UINib(nibName: Constants.TableViewCell.NoReviewCell, bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.NoReviewCellID)
    Helper.showLoading(self.activityIndicator)
    addFooterView()
  }
  
  // MARK: - Add Circulr View On Footer Of Tableview
  func addFooterView() {
    reviewTblView.ptr.footerHeight = 40
    //reviewTblView.ptr.footerView = getProgressCircle()
    
    self.reviewTblView.ptr.footerCallback = { [weak self] in
      
      if self?.arrReviewList.count != 0 {
        
        if let strCreatedAt = self?.arrReviewList.last?.createdAt {
          
          if let dateFromString = strCreatedAt.dateFromISO8601 {
            
            let beforeTime = dateFromString.iso8601
            
            DispatchQueue.global(qos: .default).async(execute: {
            
              APIHandler.shared.getReviewList(queryParams: ["before": beforeTime]) { [self] (statuscode, dataResponse, error) in
                
                if dataResponse != nil {
                  
                  if let dataArray = dataResponse?.data {
                    
                    if dataArray.count > 0 {
                      
                      self?.reviewTblView.ptr.isLoadingFooter = false
                      
                      var arrIndexPath: [IndexPath] = []
                      
                      for element in dataArray {
                        arrIndexPath.append(IndexPath(row: self?.arrReviewList.count ?? 0, section: 0))
                        self?.arrReviewList.append(element)
                        
                      }
                      
                      DispatchQueue.main.async {
                        self?.reviewTblView.performBatchUpdates({
                          self?.reviewTblView.insertRows(at: arrIndexPath, with: UITableView.RowAnimation.none)
                        }, completion: { (completed) in
                          
                        })
                      }
                      
                
                    }else {
                      
                      self?.hideFooterView()
                    }
                    
                  }else {
                    self?.hideFooterView()
                  }
                  
                }else {
                  self?.hideFooterView()
                }
                
              }
              
              
            })
            
          }
          
        }
          
        
      }else{
        self?.hideFooterView()
      }

    }
    
  }
  
  // MARK:- Hide FooterView
  func hideFooterView() {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
      self.reviewTblView.ptr.isLoadingFooter = false
    }
  }
  
  // MARK: - GET REVIEW
  func callGetReviewAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      APIHandler.shared.getReviewList(queryParams: nil) { (statuscode, response, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        guard error == nil else {
          self.updateUI()
          return
        }
        
        self.arrReviewList = []
        
        if let arrReview = response?.data {
          
          for review in arrReview {
            self.arrReviewList.append(review)
          }
          
        }
        
        self.updateUI()
        
      }
      
    }else {
      Helper.hideLoading(self.activityIndicator)
      Helper.showToast(_strMessage: ErrorMessage.noNetwork.message())
    }
    
  }
  
  
  // MARK: - Update UI
  func updateUI() {
    
    //self.reviewTblView.scrollIndicatorInsets = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 20.0, right: 0.0)
    //self.reviewTblView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 20.0, right: 0.0)
    self.reviewTblView.delegate = self
    self.reviewTblView.dataSource = self
    self.reviewTblView.reloadData()
    
  }
}



// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReviewsVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if self.arrReviewList.count == 0 {
      return 1
    }
    
    return self.arrReviewList.count
  
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    if self.arrReviewList.count == 0 {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.NoReviewCellID, for: indexPath) as? NoReviewCell else {
        return UITableViewCell()
      }
      
      objCell.imgView.isHidden = true
      objCell.mainView.backgroundColor = .white
      
      return objCell
      
    }else {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.ReviewCellID, for: indexPath) as? ReviewCell else {
        return UITableViewCell()
      }
      
      objCell.layoutconstraint[0].constant = 20.0
      objCell.layoutconstraint[1].constant = 20.0
      
      if indexPath.row == self.arrReviewList.count - 1 {
        //objCell.bottomLine.isHidden = true
      }else {
        //objCell.bottomLine.isHidden = false
      }
      
      
      
      
//      objCell.lblName.text = "Paras Modi"
//      objCell.lblReviewText.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard."
//      objCell.lblCreatedDate.text = "2 days ago"
//      objCell.ratingView.rating = 5.0

      objCell.configureCell(self.arrReviewList[indexPath.row])
      
      return objCell
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let view = UIView()
    view.backgroundColor = UIColor.clear
    return view
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView()
    view.backgroundColor = UIColor.clear
    return view
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0
  }
  
  
  
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
      let delta = scrollView.contentOffset.y - oldContentOffset.y
      
      let topViewCurrentHeightConst = innerTableViewScrollDelegate?.currentHeaderHeight
      
      if let topViewUnwrappedHeight = topViewCurrentHeightConst {
          
          /**
           *  Re-size (Shrink) the top view only when the conditions meet:-
           *  1. The current offset of the table view should be greater than the previous offset indicating an upward scroll.
           *  2. The top view's height should be within its minimum height.
           *  3. Optional - Collapse the header view only when the table view's edge is below the above view - This case will occur if you are using Step 2 of the next condition and have a refresh control in the table view.
           */
          
          if delta > 0,
              topViewUnwrappedHeight > topViewHeightConstraintRange.lowerBound,
              scrollView.contentOffset.y > 0 {
              
              dragDirection = .Up
              innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
              //print("contentOffset: \(scrollView.contentOffset.y)")
              scrollView.contentOffset.y -= delta
          }
          
          /**
           *  Re-size (Expand) the top view only when the conditions meet:-
           *  1. The current offset of the table view should be lesser than the previous offset indicating an downward scroll.
           *  2. Optional - The top view's height should be within its maximum height. Skipping this step will give a bouncy effect. Note that you need to write extra code in the outer view controller to bring back the view to the maximum possible height.
           *  3. Expand the header view only when the table view's edge is below the header view, else the table view should first scroll till it's offset is 0 and only then the header should expand.
           */
          
          if delta < 0,
              // topViewUnwrappedHeight < topViewHeightConstraintRange.upperBound,
              scrollView.contentOffset.y < 0 {
              
              dragDirection = .Down
              innerTableViewScrollDelegate?.innerTableViewDidScroll(withDistance: delta, withScrollDirection: dragDirection)
              scrollView.contentOffset.y -= delta
          }
      }
      
      oldContentOffset = scrollView.contentOffset
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if scrollView.contentOffset.y <= 0 {
        
        innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }else {
     
      // UITableView only moves in one direction, y axis
     /* let currentOffset = scrollView.contentOffset.y
      let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
      
//      Helper.DLog(message: "currentOffset:::\(currentOffset)")
//      Helper.DLog(message: "contentSize:::\(scrollView.contentSize.height)")
//      Helper.DLog(message: "frameSize:::\(scrollView.frame.height)")
//      Helper.DLog(message: "maximumOffset:::\(maximumOffset)")
      
      // Change 10.0 to adjust the distance from bottom
      if maximumOffset - currentOffset <= 10.0 {
        self.reviewTblView.ptr.isLoadingFooter = true
        self.loadMoreWithPagination()
      }*/
      
    }
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    
    //You should not bring the view down until the table view has scrolled down to it's top most cell.
    
    if decelerate == false && scrollView.contentOffset.y <= 0 {
        
        innerTableViewScrollDelegate?.innerTableViewScrollEnded(withScrollDirection: dragDirection)
    }
  }
}


extension ReviewsVC {
  
  
  // MARK: - Load More with Pagination
  func loadMoreWithPagination(_ completion: @escaping(Int, [ReviewDetails]) -> Void) {
    
    if self.arrReviewList.count != 0 {
      
      if let strCreatedAt = self.arrReviewList.last?.createdAt {
        
        if let dateFromString = strCreatedAt.dateFromISO8601 {
          
          let beforeTime = dateFromString.iso8601
          
          DispatchQueue.global(qos: .default).async(execute: {
          
            APIHandler.shared.getReviewList(queryParams: ["before": beforeTime]) { (statuscode, dataResponse, error) in
              
              if dataResponse != nil {
                
                if let dataArray = dataResponse?.data {
                  
                  if dataArray.count > 0 {
                    
                    completion(dataArray.count, dataArray)
              
                  }else {
                    
                    completion(dataArray.count, self.arrReviewList)
                  }
                  
                }else {
                  completion(0, self.arrReviewList)
                }
                
              }else {
                completion(0, self.arrReviewList)
              }
              
            }
            
            
          })
          
        }
        
      }
        
      
    }else{
      completion(0, self.arrReviewList)
    }
    
    
  }
  
}
