
import UIKit

/* URL Schemes */
let googleMapURLScheme  = "comgooglemaps://"
let appleMapURLScheme   = "http://maps.apple.com/"

/* Other */
let defaultZoomLevel    = 14

/* Enum MapType */
@objc public enum MapType: Int {
    
    case `default` = 0  // Default: Ask user for selection of map
    case googleMap
    case appleMap
}

/* Enum MapView */
@objc public enum MapViewMode: Int {
    
    case standard = 0  // Default
    case satellite
    case transit
    
    /// get query string based on map type
    public func queryString(for mapType: MapType) -> String {
        switch self {
        case .standard:
            return (mapType == .googleMap) ? "views=roadmap" : "t=m"
        case .satellite:
            return (mapType == .googleMap) ? "views=satellite" : "t=k"
        case .transit:
            return (mapType == .googleMap) ? "views=transit" : "t=r"
        }
    }
}

/* NSObject Extension */
extension NSObject {
    
    /// checks if google map is installed or can open google map
    func canOpenGoogleMap() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: googleMapURLScheme)!)
    }
    
    /// open map url
    func openMapURL(url: String) {
        if let mapURL = URL(string: url.encodedURLString()) {
            if UIApplication.shared.canOpenURL(mapURL){
                UIApplication.shared.open(mapURL, options: [:], completionHandler: nil)
            }
        }
    }
}

/* String URL encoding */
extension String {
    func encodedURLString() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? self
    }
}

@objc open class MapService: NSObject {
    
    // MARK: - Properties
    @objc public var address: String = ""

    /// MapType: - default is '.default' which asks user to select map if 'Google Maps' is installed.
    @objc public var mapType: MapType = .default
    
    /// MapView mode: - default is Standard
    @objc public var mapViewMode: MapViewMode = .standard
    
    /// zoom level for map: - default is 14
    @objc public var zoomLevel: Int = defaultZoomLevel
    
    // MARK: - Initialize
    @objc public init(address: String) {
        self.address = address
    }
    
    
    // MARK:- Public
    /// open specified address or lat,long in map
    @objc public func openInMap() {
        
        // check if google map is not installed then open in apple map
        if !self.canOpenGoogleMap() {
            showPlaceInAppleMap()
            return
        }
        showPlaceInGoogleMap()
    }
}

// MARK:- Open in Maps
extension MapService {
    
    /// show place in google map
    private func showPlaceInGoogleMap() {
        var urlString = "\(googleMapURLScheme)?"
        
        // address
        if !address.isEmpty {
            urlString.append("q=\(address)&")
        }
        
        // add mapview mode and zoom level
        urlString.append("\(mapViewMode.queryString(for: .googleMap))&zoom=\(zoomLevel)")
        
        // open
        self.openMapURL(url: urlString)
    }
    
    /// show place in apple map
    private func showPlaceInAppleMap() {
        var urlString = "\(appleMapURLScheme)?"
        
        // address
        if !address.isEmpty{
            urlString.append("q=\(address)&")
        }
        
        // add mapview mode and zoom level
        urlString.append("\(mapViewMode.queryString(for: .appleMap))&z=\(zoomLevel)")
        // open
        self.openMapURL(url: urlString)
    }
}
