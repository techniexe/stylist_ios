//
//  AppDelegate.swift
//  StylistApp
//
//  Created by Techniexe Infolabs on 22/01/21.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    setCountryCode()
    setupKeyboard()
    GlobalFunction.setRootViewController()
    return true
  }

  func setCountryCode() {
    UserDefaults.standard.set(NSLocale.autoupdatingCurrent.regionCode, forKey:Constants.CountryCode.countryCode)
    UserDefaults.standard.synchronize()
  }
  
  // MARK: - Setup KeyBorad
  func setupKeyboard() {
    IQKeyboardManager.shared.enable = true
    IQKeyboardManager.shared.enableAutoToolbar = true
    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
  }
}

